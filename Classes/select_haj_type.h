//
//  select_haj_type.h
//  mutnav2
//
//  Created by rashad on 8/13/14.
//
//

#import <UIKit/UIKit.h>
#import "haj_mofrad.h"

@interface select_haj_type : UIViewController
@property (strong, nonatomic) IBOutlet UIView *haj_header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) UILabel *title_lable;
@property (strong, nonatomic) UILabel *title_lable2;
@property (strong, nonatomic) haj_mofrad *haj_mofrad_view;


@end
