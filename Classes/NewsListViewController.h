//
//  NewsListViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 5/12/15.
//
//

#import <UIKit/UIKit.h>
#import "Global.h"

#define kMyTag 0

@interface NewsListViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (strong, nonatomic) IBOutlet UITableView *mainTable;
@property(nonatomic,retain)NSMutableArray *menuearr;
@property(nonatomic,retain)NSMutableArray *urlarr;
@property (nonatomic,retain) UIView *news_view;
@property (retain, nonatomic) IBOutlet UIWebView *myWeb;
@property (nonatomic,retain) UIImageView *botimg;



@end
