//
//  detailsviewcontroller.m
//  mutnav2
//
//  Created by amr on 1/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "detailsviewcontroller.h"


@implementation detailsviewcontroller
@synthesize txtdetails;
@synthesize sdic,myid,plist,segments;
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	
	filemgr = [NSFileManager defaultManager];
	//plist=[[NSString alloc] initWithFormat:filepath,plist];
	
	NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
		
		//menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
		
		NSData *databuffer;
		databuffer = [filemgr contentsAtPath: path ];
	//	NSLog(@"vvvvv%s",databuffer);
		txtdetails.text=[[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
		//NSString* aStr = [[NSString alloc] initWithData:databuffer encoding:NSASCIIStringEncoding];
		
	}
	
}
-(IBAction)changefile:(id) sender{
	
	//NSLog(@"%d",[sender selectedSegmentIndex]);
	
	//plist=;
	[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@_%d",[sdic objectForKey:@"targetname"],[sender selectedSegmentIndex]]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
	//[segments setHidden:YES];
	
	myid=[sdic objectForKey:@"id"];
	
	self.title=[sdic objectForKey:@"name"];
	
	if([[sdic objectForKey:@"targetpage"] intValue]==2)
	{
		
		int filescount=[[sdic objectForKey:@"targetfilesno"] intValue];
		
		[segments setHidden:NO];
		[segments removeAllSegments];
		for(int a=0;a<filescount;a++)
		{
			NSString *segtitle=[[NSString alloc] initWithFormat:@"%d",a+1];
			
			[segments insertSegmentWithTitle:segtitle atIndex:a animated:YES];
		}
		[segments setSelectedSegmentIndex:0];
		
		//plist=;
		[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@_0",[sdic objectForKey:@"targetname"]]];
		
		
	}
	else {
		//[txtdetails set];
		//plist=;
		//txtdetails.se
		//[self set];
		txtdetails.frame=CGRectMake(20, 6, 280, 371);
		[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@%@",plist,myid]];
		for(id key in sdic)
			NSLog(@"key is %@ and value is %@",key,[sdic objectForKey:key]);
	}

	//NSLog(plistname);
	//NSString *mypath=[[NSBundle mainBundle] pathForResource:plist ofType:@"plist"];
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
	[segments release];
	[myid release];
	[plist release];
	[sdic release];
	[txtdetails release];
}


@end
