//
//  TownsViewController.m
//  GadawalV1
//
//  Created by waleed on 9/2/13.
//
//
#import "TownsViewController.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#import "oldImg.h"
#import "mutnav2AppDelegate.h"
@interface TownsViewController ()

@end

@implementation TownsViewController
@synthesize myData,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
   
    
    myData = [[SKMenu alloc] initWithFile:@"mutawefdatabase.db"];
   // self.view.backgroundColor=[UIColor whiteColor];
    self.view.frame = CGRectMake(0, 0, 320,440*highf);
    self.title = @"";
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"حدد مدينتك";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];
    
    if (type==1) {
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 5, 50, 30*highf);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
       /* UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];*/
         [self.view addSubview:backButton];
        
    }
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(260, 0, 55, 40);
    [saveBtn setImage:[UIImage imageNamed:@"save.png"] forState:UIControlStateNormal];
    [saveBtn setShowsTouchWhenHighlighted:TRUE];
    [saveBtn addTarget:self action:@selector(saveData:) forControlEvents:UIControlEventTouchDown];
  //  [self.view addSubview:saveBtn];
    
    _selectedTown=0;
    _mcc =[self getMCC];
    NSLog(@"%@",_mcc);
    [self loadXML];
    NSLog(@"%i",[_towns count]);
    _townTable = [[UITableView alloc] initWithFrame:CGRectMake(0,40,320,420*highf) style:UITableViewStyleGrouped];
    _townTable.delegate = self;
    _townTable.dataSource = self;
    _townTable.autoresizesSubviews = YES;
    _townTable.backgroundColor=[UIColor whiteColor];
    _townTable.rowHeight=45.0*highf;
    _townTable.alpha=1;
    _townTable.separatorColor = [UIColor blackColor];
    _townTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [self.view addSubview:_townTable];
    if ([_towns count]==0) {
        if (type==1) {
            UILabel *txtLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 300, 60)];
            txtLab.text = @"عفوًا لا يتوفر اتصال بالإنترنت";
            txtLab.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:txtLab];
            
        }
        else {
            mutnav2AppDelegate *delegate = (mutnav2AppDelegate*)([UIApplication sharedApplication].delegate);
            delegate.window.rootViewController = delegate.tabBarController;
        }
       // mutnav2AppDelegate *delegate = (mutnav2AppDelegate*)([UIApplication sharedApplication].delegate);
        //delegate.window.rootViewController = delegate.tabBarController;
    }
    
}
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) viewWillAppear:(BOOL)animated
{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
 

    [super viewWillAppear:animated];
    }

-(NSString*)getMCC{
    // Setup the Network Info and create a CTCarrier object
	CTTelephonyNetworkInfo *networkInfo = [[[CTTelephonyNetworkInfo alloc] init] autorelease];
	CTCarrier *carrier = [networkInfo subscriberCellularProvider];
	// Get mobile network code
    
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil) {NSLog(@"Mobile Country Code (MCC): %@", mcc);}
    else {mcc = @"602";}
   
    
	return  mcc;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser{
    NSLog(@"File found and parsing started");
    
}

-(void)loadXML{
  //  Reachability *curReach = [[Reachability reachabilityForInternetConnection] retain];
   /* Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];

    [curReach startNotifier];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
        
        if (type==1) {
            UILabel *txtLab= [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 300, 60)];
            txtLab.text = @"عفوًا لا يتوفر اتصال بالإنترنت";
            txtLab.textAlignment=NSTextAlignmentCenter;
            [self.view addSubview:txtLab];
        
        }
        else {
            [[self.view superview] removeFromSuperview];
            mutnav2AppDelegate *delegate = (mutnav2AppDelegate*)([UIApplication sharedApplication].delegate);
            delegate.window.rootViewController = delegate.tabBarController;
        }
       

    }
    else{*/
        NSString *tmp = [self getMCC];
        NSString *_splashurl =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v2/api/countrycities.aspx?mcc=%@",tmp] ;
        NSURL *url = [NSURL URLWithString:_splashurl];
        //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        NSLog(@"uuuu===%@",url);
        
          NSURLRequest* chRequest = [NSURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval:6];
        NSError* theError;
        NSData* response = [NSURLConnection sendSynchronousRequest:chRequest returningResponse:nil error:&theError];
        NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithData:response];
        
         xmlparser.delegate=(id)self;
        NSLog(@"%@",xmlparser);
        //rssParser *parser = [[rssParser alloc] initXMLParser];
        
        //[xmlparser setDelegate:parser];
        
        BOOL success = [xmlparser parse];
        
        if(success){
            NSLog(@"No Errors");
        }
        else{
            NSLog(@"Error Error Error!!!");
        }
        
        
  //  }
    
   
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"cities"]) {
        //Initialize the array.
        _towns = [[NSMutableArray alloc] init];
    }
    else if([elementName isEqualToString:@"city"]) {
        //Initialize the book.
        _town=[[NSMutableDictionary alloc] init];
        
        //Extract the attribute here.
        //   NSString *t=  [attributeDict objectForKey:@"content"] ;
        //NSLog(@"T=====t%@",t);
        /// [_ad setObject:t forKey:@"url"];
        // NSLog(@"Reading id value :%i", book.bookID);
    }
    NSLog(@"Processing Element: ");
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if(!_ElementValue)
        _ElementValue = [[NSMutableString alloc] initWithString:string];
    else
        [_ElementValue appendString:string];
    
    NSLog(@"Processing Value: %@", _ElementValue);
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if([elementName isEqualToString:@"cities"]){
        return;
    }
    
    if([elementName isEqualToString:@"city"]) {
        [_towns addObject:_town];
        [_town release];
        _town = nil;
    }
    else
        [_town setValue:_ElementValue forKey:elementName];
    
    [_ElementValue release];
    _ElementValue = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
        return [_towns count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    
    NSMutableString * theMutableString = [[_towns objectAtIndex:indexPath.row]objectForKey:@"name"];
    NSLog(@"%@",theMutableString);
    
    [theMutableString replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:(NSRange){0,[theMutableString length]}];

    
  //  cell.textLabel.text = [[_towns objectAtIndex:indexPath.row]objectForKey:@"name"];
      cell.textLabel.text =theMutableString;
    
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
    //cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.alpha=1;
    
    cell.selectionStyle =UITableViewCellSelectionStyleGray;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    for(int iterator=0;iterator<_towns.count;iterator++){
        UITableViewCell *eachCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:iterator inSection:0]];
        [eachCell setSelected:NO animated:YES];
        [eachCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];
    [newCell setSelected:YES animated:YES];
    [newCell setAccessoryType:UITableViewCellAccessoryCheckmark];

    
    _selectedTown = [[[_towns objectAtIndex:indexPath.row]objectForKey:@"id"]integerValue];
    NSLog(@"%d",_selectedTown);
    [self saveData];
    if (type==1) {
        [self popViewControllerWithAnimation];
    }
    else{
        
    mutnav2AppDelegate *delegate = (mutnav2AppDelegate*)([UIApplication sharedApplication].delegate);
    delegate.window.rootViewController = delegate.tabBarController;
    }
   /* NSArray *patharray=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSAllDomainsMask, YES);
    //get path of plist (firsttime)
    NSString *mypath1=[[patharray objectAtIndex:0] stringByAppendingPathComponent:@"firsttime.plist"];
    
    
    NSString *mypath=[[NSBundle mainBundle] pathForResource:@"firsttime" ofType:@"plist"];
	//NSMutableArray *menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
    
    //store bool value if (firsttime) is exist
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:mypath];
    //check if (firsttime.plist) is exist
    if(fileExist){//plist already exits in doc dir so save content in it
        
        NSMutableArray *data = [NSMutableArray arrayWithContentsOfFile:mypath];
        //update your array here
        NSString *comment = @"1";
        [data replaceObjectAtIndex:0 withObject:comment];
        [self saveData];
        //write file here
        [data writeToFile:mypath atomically:YES];
        mutnav2AppDelegate *delegate = (mutnav2AppDelegate*)([UIApplication sharedApplication].delegate);
        delegate.window.rootViewController = delegate.tabBarController;
        //[self.view removeFromSuperview];
    }*/
    
    [self.view removeFromSuperview];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) saveData{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filepath= [[paths objectAtIndex:0]stringByAppendingPathComponent:@"mutawefdatabase.db"];
    if (sqlite3_open([filepath UTF8String], &db)!=SQLITE_OK) {
        sqlite3_close(db);
        NSAssert(0,@"Database failed to open");
    }
    NSString *sql=[NSString stringWithFormat:@"INSERT INTO GeneralInformations ('placeID') VALUES ('%d')",_selectedTown];
    char *err;
    if (sqlite3_exec(db,[sql UTF8String], NULL, NULL, &err) !=SQLITE_OK) {
        NSAssert(0, @"Could not update table");
        NSLog(@"Done");
    }
    else{
        NSLog(@"table updated");
    }
    
   /* if ([myData countFoTable:@"GeneralInformations" withField:@"adsType" withValue:@"1"]>0) {
        [myData deleteFromTable:@"ads" ByID:adsType];
    }
    NSString *txt =[NSString stringWithFormat:@"'%i','%@','%@'",adsType,thridContent,_thirdURL];
    NSLog(@"%@",txt);
    [myData insertRecordIntoTableNamed:@"ads" withField:@"adsType,imgPath,adsURL" withValue:txt];
*/
}



@end
