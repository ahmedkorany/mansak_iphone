//
//  TweetCell.h
//  mutnav2
//
//  Created by waleed on 5/28/13.
//
//

#import <UIKit/UIKit.h>

NSString * const TweetCellId = @"TweetCell";

@interface TweetCell : UITableViewCell
@property (strong, nonatomic)NSDictionary *tweetata;
@end
