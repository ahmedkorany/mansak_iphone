//
//  EMutawefViewController.m
//  mutnav2
//
//  Created by Waleed Nour on 2/19/12.
//  Copyright (c) 2012 Massar Software. All rights reserved.
//

#import "EMutawefViewController.h"
#import <QuartzCore/QuartzCore.h>

@implementation EMutawefViewController

@synthesize bgImage;
@synthesize elecmutawef,imgindex,mytext,arrdoaa,arrmanasek,segments,lbltitle,from;
@synthesize sdic;
@synthesize leftButton,rightButton;
@synthesize douBtn,manBtn,pushBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NIbg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,400*highf);
    [self.view addSubview:bgImage];
    imgindex=0;
    elecmutawef = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"t0.png"]];
    elecmutawef.frame=CGRectMake(0, 0, 320, 360*highf);
    
    lbltitle = [[UILabel alloc]initWithFrame:CGRectMake(90, 300*highf, 150, 37*highf)];
    lbltitle.backgroundColor = [UIColor clearColor];
    lbltitle.text=[sdic objectForKey:@"title"];
    lbltitle.textAlignment = NSTextAlignmentCenter;
    lbltitle.autoresizesSubviews = YES;
    lbltitle.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    lbltitle.textColor = [UIColor blackColor];
    
    NSString *mypath=[[NSBundle mainBundle] pathForResource:@"elec" ofType:@"plist"];
	arrmanasek=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
	sdic=[arrmanasek objectAtIndex:0];
	
    mytext = [[UITextView alloc] initWithFrame:CGRectMake(16, 58*highf, 288,258*highf)];
    mytext.delegate =self;
    mytext.autoresizesSubviews = YES;
    mytext.backgroundColor = [UIColor colorWithRed:0.616 green:0.733 blue:0.294 alpha:1.0];
    mytext.alpha = 1;
    mytext.font = [UIFont boldSystemFontOfSize:16];    
    mytext.editable = NO;
    mytext.textAlignment = NSTextAlignmentRight;
   // [mytext.layer setBackgroundColor: [[UIColor whiteColor] CGColor]];
    [mytext.layer setBorderColor: [[UIColor colorWithRed:0.314 green:0.243 blue:0.227 alpha:1.0] CGColor]];
    [mytext.layer setBorderWidth: 3.0];
    [mytext.layer setCornerRadius:10.0f];
    [mytext.layer setMasksToBounds:YES];
  /*  segments = [[UISegmentedControl alloc] initWithFrame:CGRectMake(56, 150, 207, 40)];
    segments.backgroundColor = [UIColor clearColor];
    
    [segments insertSegmentWithTitle:@"المناسك" atIndex:1 animated:YES];
    [segments insertSegmentWithTitle:@"الدعاء" atIndex:0 animated:YES];
    [segments setSelectedSegmentIndex:1];
    [segments addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventValueChanged];
	*/
    
    douBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    douBtn.frame = CGRectMake(25, 15*highf, 120, 50*highf);
    [douBtn setImage:[UIImage imageNamed:@"doua.png"] forState:UIControlStateNormal];
    douBtn.backgroundColor = [UIColor clearColor];
    douBtn.tag=0;
    [douBtn addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventTouchUpInside];

    manBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    manBtn.frame = CGRectMake(155, 15*highf, 120, 50*highf);
    [manBtn setImage:[UIImage imageNamed:@"mansk.png"] forState:UIControlStateNormal];
    manBtn.backgroundColor = [UIColor clearColor];
     manBtn.tag=1;
    [manBtn addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventTouchUpInside];
    
    
    leftButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    leftButton.frame = CGRectMake(25, 316, 60, 30);
    [leftButton setTitle:@"<<" forState:UIControlStateNormal];
    leftButton.tintColor = [UIColor blackColor];
    leftButton.backgroundColor = [UIColor clearColor];
    [leftButton addTarget:self action:@selector(previous_tawaf:) forControlEvents:UIControlEventTouchUpInside];
    rightButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    rightButton.frame = CGRectMake(244, 316, 60, 30);
    [rightButton setTitle:@">>" forState:UIControlStateNormal];
    rightButton.tintColor = [UIColor blackColor];
    rightButton.backgroundColor = [UIColor clearColor];
    [rightButton addTarget:self action:@selector(next_tawaf:) forControlEvents:UIControlEventTouchUpInside];
 
        
	//arrmanasek=[NSMutableArray];
	if([from isEqualToString:@"0"])
	{
		elecmutawef.hidden=YES;
		mytext.hidden=NO;
	//	segments.hidden=NO;
		//mytext.frame=CGRectMake(20, 75, 280, 250);
	//	segments.frame=CGRectMake(20, 30, 280, 40);
        [self.view addSubview:douBtn];
        [self.view addSubview:manBtn];
        self.title = @"تطويف";
        [self.view addSubview:leftButton];
        [self.view addSubview:rightButton];
        [self.view addSubview:lbltitle];
        
	}
	else if([from isEqualToString:@"1"])
	{
		elecmutawef.frame=CGRectMake(0, 20, 324, 320);
		mytext.hidden=YES;
	//	segments.hidden=YES;
		elecmutawef.hidden=NO;
         self.title = @"العداد";
        pushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pushBtn.frame = CGRectMake(50, 40, 224, 240);
        pushBtn.backgroundColor = [UIColor clearColor];
        [pushBtn addTarget:self action:@selector(next_tawaf:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:pushBtn];

		
	}
	else {
		elecmutawef.frame=CGRectMake(73, 10, 162, 140);
        
		elecmutawef.hidden=NO;
		mytext.hidden=NO;
        mytext.frame=CGRectMake(16, 180, 280,130);
	//	segments.hidden=NO;
         self.title = @"الجميع";
        [douBtn setFrame:CGRectMake(25, 140, 120, 40)];
        [manBtn setFrame:CGRectMake(155, 140, 120, 40)];
        
        
        [self.view addSubview:douBtn];
        [self.view addSubview:manBtn];
        [self.view addSubview:leftButton];
        [self.view addSubview:rightButton];
        [self.view addSubview:lbltitle];
        
	}
    
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor greenColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text =  self.title;
    [titleView sizeToFit];
    NSString *filename=[sdic objectForKey:@"manasek"];
	[self addfiletotextview:filename];
    
    [self.view addSubview:elecmutawef];
    [self.view addSubview:mytext];
 //   [self.view addSubview:segments];
    
     
      /*
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self
                                 action: @selector(next_tawaf:)];
    
    tap.numberOfTapsRequired = 1;
    [tap setDelegate:self];
    [tap release];*/
    
    UIPanGestureRecognizer *panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleGesture:)];
    [panGest setMaximumNumberOfTouches:1];
    [self.view addGestureRecognizer:panGest];
    [panGest release];
    // Do any additional setup after loading the view from its nib.
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
       // UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 50, 30);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self setFrom:nil];
    [self setArrdoaa:nil];
    [self setArrmanasek:nil];
    [self setBgImage:nil];
    [self setElecmutawef:nil];
    [self setLbltitle:nil];
    [self setMytext:nil];
    [self setSdic:nil];
    [self setSegments:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(IBAction) next_tawaf:(id)sender{
	if(imgindex<[arrmanasek count]-1)
        imgindex++;
	if ([from isEqualToString:@"1"] && imgindex>=7) {
		imgindex=7;
	}

	if(![from isEqualToString:@"0"] &&( imgindex<=7 ||(imgindex>=11 && imgindex<=17)))
	{
        
	//	NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
        	NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
		elecmutawef.image=[UIImage imageNamed:newimg];
		elecmutawef.hidden=NO;
		
	}
	else {
		elecmutawef.hidden=YES;
	}
    
	lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
	//NSString *filename=[sdic objectForKey:@"manasek"];
	[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
//	segments.selectedSegmentIndex=1;
	
	
}
-(IBAction) previous_tawaf:(id)sender{
	if(imgindex>0){
        imgindex--;
		if(![from isEqualToString:@"0"] &&( imgindex<=7||(imgindex>=11 && imgindex<=17))){
     //       NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
                   NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
            elecmutawef.image=[UIImage imageNamed:newimg];
			elecmutawef.hidden=NO;
		}
		else {
			elecmutawef.hidden=YES;
		}
        
	}
	lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
	[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
//	segments.selectedSegmentIndex=1;
	
}

-(IBAction)changefile:(id) sender{
	int i = [sender tag];
    
	//NSLog(@"%d",randomint);
	if(i==0){
		NSInteger randomint=arc4random()%14+1;
		//NSString *randomfile=[[NSString alloc] initWithFormat:@"doaa%d",randomint];
        NSString *randomfile=[NSString stringWithFormat:@"doaa%d",randomint];
        [self addfiletotextview:randomfile];
      //  NSLog(@"%d",[sender selectedSegmentIndex]);
	}
	else {
		[self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
	}
    
	//plist=;
	//[self addfiletotextview:[[NSString alloc] initWithFormat:@"%@_%d",[sdic objectForKey:@"targetname"],[sender selectedSegmentIndex]]];
}
-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	
	filemgr = [NSFileManager defaultManager];
	//plist=[[NSString alloc] initWithFormat:filepath,plist];
	
	NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
    NSLog(@"%@",filename);
    
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
		
		//menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
		
		//NSData *databuffer;
	//	databuffer = [filemgr contentsAtPath: path ];
        NSError *error;
		//NSLog(@"vvvvv%s",databuffer);
	//	mytext.text=[[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        mytext.text=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
		//NSString* aStr = [[NSString alloc] initWithData:databuffer encoding:NSASCIIStringEncoding];
		
	}
	
}

- (BOOL)gestureRecognizer:(UITapGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    NSLog(@"TEST BOOL");
    if ([touch.view isKindOfClass:[UISegmentedControl class]]) {
        // we touched a button, slider, or other UIControl
        return NO; // ignore the touch
    }
    return YES;
    //return ([self.segments pointInside:[touch locationInView:self.segments] withEvent:nil]);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)handleGesture:(UIPanGestureRecognizer *)gestureRecognizer{
    
    if (gestureRecognizer.state ==UIGestureRecognizerStateEnded ) {
        CGPoint velocity = [gestureRecognizer velocityInView:self.view];
        if(velocity.x > 0)
        {
            if(imgindex>0){
                imgindex--;
                if(![from isEqualToString:@"0"] &&( imgindex<=7||(imgindex>=11 && imgindex<=17))){
                    //       NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
                    NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
                    elecmutawef.image=[UIImage imageNamed:newimg];
                    elecmutawef.hidden=NO;
                }
                else {
                    elecmutawef.hidden=YES;
                }
            }
            lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
            [self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
       //     segments.selectedSegmentIndex=1;
                       NSLog(@"%f", velocity.x);
            NSLog(@"gesture went right");
        }
        else
        {
            if(imgindex<[arrmanasek count]-1)
                imgindex++;
            if ([from isEqualToString:@"1"] && imgindex>=7) {
                imgindex=7;
            }
            if(![from isEqualToString:@"0"] &&( imgindex<=7 ||(imgindex>=11 && imgindex<=17)))
            {
                //	NSString *newimg=[[NSString alloc] initWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
                NSString *newimg=[NSString stringWithFormat:@"t%d.png",imgindex>10?imgindex-10:imgindex];
                elecmutawef.image=[UIImage imageNamed:newimg];
                elecmutawef.hidden=NO;
            }
            else {
                elecmutawef.hidden=YES;
            }
            lbltitle.text=[[arrmanasek objectAtIndex:imgindex] objectForKey:@"title"];
            //NSString *filename=[sdic objectForKey:@"manasek"];
            [self addfiletotextview:[[arrmanasek objectAtIndex:imgindex] objectForKey:@"manasek"]];
          //  segments.selectedSegmentIndex=1;
            NSLog(@"%f", velocity.x);
            NSLog(@"gesture went left");
        }
    }
}

- (void)dealloc {
    [super dealloc];
   /* [bgImage release];
	[elecmutawef release];
	[mytext release];
	[arrmanasek release];
	[arrdoaa release];
	[segments release];
	[lbltitle release];
	[sdic release];
	[from release];*/
}

@end
