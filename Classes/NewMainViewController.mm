//
//  NewMainViewController.m
//  mutnav2
//
//  Created by waleed on 3/31/13.
//
//
#import <QuartzCore/QuartzCore.h>
#import "NewMainViewController.h"
#import "Reachability.h"
#import "RXMLElement.h"
#import "webviewcontroller.h"
#import "Global.h"
#import "AppDelegate.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
@interface NewMainViewController ()

@end

@implementation NewMainViewController

@synthesize prayerTimes;
@synthesize backImage;
@synthesize tawef,saai;
@synthesize prayTimes;
@synthesize mobView,viewId;
@synthesize floorView,floorId,crowd,crowdId;
@synthesize prayImage,rightBtn,leftBtn,leftLab,centLab,rightLab,prayIndex,praysLab,praysname,labId;
@synthesize menuearr,mainTable,urlarr, myWeb,leftbut,centbut,rightbut,sa3yView,headerView,moblabs,ka3baView,jamaratView,mobViews,curView,prevTag,ka3baimg;
@synthesize crowdColors,crowdImgs,crowdNames,liveImage;
@synthesize refresh_btn;
@synthesize refresh_lbl_view;
@synthesize refresh_view;
@synthesize citylabel,timeLabel,first_title,timer,labText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        /*  if ([self isViewLoaded]) {
         self.view = nil;
         [self viewDidLoad];
         }*/
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

 // Implement loadView to create a view hierarchy programmatically, without using a nib.

 - (void)loadView{
   [super loadView];

}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
/*
#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager*)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined: {
            NSLog(@"User still thinking..");
        } break;
        case kCLAuthorizationStatusDenied: {
            NSLog(@"User hates you");
        } break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways: {
            [_locationManager startUpdatingLocation]; //Will update location immediately
        } break;
        default:
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
}

*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*_locationManager = [[CLLocationManager alloc]init];
    _locationManager.delegate = self;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse) {
        // Will open an confirm dialog to get user's approval
        [_locationManager requestWhenInUseAuthorization];
        //[_locationManager requestAlwaysAuthorization];
    } else {
        [_locationManager startUpdatingLocation]; //Will update location immediately
    }
    */
    
    self.view.clipsToBounds=YES;
    //self.view.frame=CGRectMake(0, 15, 320, 465*highf);
    
    _myData = [[SKMenu alloc] initWithFile:@"mutawefdatabase.db"];
    
    NSLog(@"%@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    
    

    
    self.view.backgroundColor=[UIColor whiteColor];
    screen_size=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screen_size.size.width,screen_size.size.height);
    
    backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NewBG.jpg"]];
    backImage.frame = CGRectMake(0, 0, 320,screen_size.size.height);
    
    [backImage setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
    
    [self.view addSubview:backImage];
    curView = 0;
    prevTag = -1;
    viewId = 0;
    
    
    
    tawef = [[NSMutableArray alloc] init];
    saai = [[NSMutableArray alloc] init];
    
    praysLab = [[NSMutableArray alloc]init];
    moblabs = [[NSMutableArray alloc]init];
    mobViews = [[NSMutableArray alloc]init];
    crowdImgs = [[NSMutableArray alloc]initWithObjects:@"fl2.png",@"fl3.png",@"fl4.png",@"fl5.png",@"fl1.png",@"fl6.png", nil];
    
    crowdColors = [[NSArray alloc] initWithObjects:
                   [UIColor colorWithRed:0.937 green:0.851 blue:0.008 alpha:1] ,
                   [UIColor colorWithRed:0.686 green:0.929 blue:0.055 alpha:1] ,
                   [UIColor colorWithRed:0.075 green:0.91 blue:0.435 alpha:1] ,
                   [UIColor colorWithRed:0.102 green:0.886 blue:0.91 alpha:1] ,
                   [UIColor colorWithRed:0.984 green:0.741 blue:0.106 alpha:1] ,
                   [UIColor colorWithRed:0.114 green:0.639 blue:0.812 alpha:1] , nil];
    crowdNames= [[NSMutableArray alloc] initWithObjects:@"veryCrowd",@"crowd",@"medium",@"empty",@"close",@"light",nil];
    
        UIPanGestureRecognizer *panRecognizer = [[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)] autorelease];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [panRecognizer setDelegate:self];
    [self.view addGestureRecognizer:panRecognizer];
    
    
    //  NSString *selectedLang=[[NSLocale preferredLanguages]objectAtIndex:0];
    
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0*highf, 320, 150*highf)];
    headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerView];
    UIImageView *img = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"header.png"]];
    img.frame = CGRectMake(0, 0*highf, 320, 133*highf);
    
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,  130*highf)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    
    
    // [self.headerView addSubview:img];
    [self.headerView addSubview:header_image];
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40,header_image.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
      [self.headerView addSubview:second_image];
    
    first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, header_image.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    first_title.textColor=[UIColor blackColor];
    first_title.textAlignment=NSTextAlignmentCenter;
  //  first_title.text=@"لجنة الحج المركزية";
      [self.headerView addSubview:first_title];
   /* UIImageView *liveimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainLive.png"]];
    liveimg.frame = CGRectMake(40,90*highf, 240, 50);
    [self.headerView addSubview:liveimg];
    [liveimg release];
    [img release];*/
    
    
    citylabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20*highf, 90, 16*highf)];
    citylabel.backgroundColor = [UIColor clearColor];
    citylabel.alpha = 1;
    
    citylabel.textColor = [UIColor whiteColor];
    citylabel.textAlignment = NSTextAlignmentCenter;
    citylabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:15];
    /* if ([selectedLang isEqualToString:@"ar"]) {
     citylabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:15];
     }else{
     citylabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:10];
     
     }*/
    [self.headerView addSubview:citylabel];
   
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 31*highf, 90, 16*highf)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.alpha = 1;
    // timeLabel.text = [formatter stringFromDate:date];
    timeLabel.textColor = [UIColor whiteColor];
    timeLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:13];
    /* if ([selectedLang isEqualToString:@"ar"]) {
     timeLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:13];
     }else
     {
     timeLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:10];
     
     }*/
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [self.headerView addSubview:timeLabel];
    [self drawTimePlayers];
    
    
    [self drawMedArea];
    [self drawMob];
    
    /*
     UIView *bathView = [[UIView alloc]initWithFrame:CGRectMake(0, 335*highf, self.view.frame.size.width, 100*highf)];
     bathView.backgroundColor =[UIColor colorWithRed:0.792 green:0.78 blue:0.745 alpha:1];
     
     UIButton *madBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     madBtn.frame = CGRectMake(75,105*highf ,75, 35);
     [madBtn setTitle:@"مباشر الحرم المدني" forState:UIControlStateNormal];
     [madBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
     madBtn.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:10.0];
     //[madBtn setImage:[UIImage imageNamed:@"madany.jpg"] forState:UIControlStateNormal];
     madBtn.tag = 2;
     [madBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
     
     [self.view addSubview:madBtn];
     
     
     UIButton *makBtn = [UIButton buttonWithType:UIButtonTypeCustom];
     // makBtn.frame = CGRectMake(165, 345*highf ,125, 80*highf);
     makBtn.frame = CGRectMake(170, 105*highf ,75, 35);
     [makBtn setTitle:@"مباشر الحرم المكي" forState:UIControlStateNormal];
     [makBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
     makBtn.titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:10.0];
     // [makBtn setImage:[UIImage imageNamed:@"makkey.jpg"] forState:UIControlStateNormal];
     makBtn.tag = 1;
     [makBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
     [self.view addSubview:makBtn];
     
     */
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(20, 342*highf, 280, 150*highf) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1.0;
    mainTable.rowHeight=30.0;
    mainTable.separatorColor = [UIColor clearColor];
    
    [self.view addSubview:mainTable];
    
    _botimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"upper.png"]];
    _botimg.frame = CGRectMake(0, 424*highf, 320, 56*highf);
    // [botimg setAutoresizingMask: UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleTopMargin];
    [self.view addSubview:_botimg];
    /*UIButton *show_menu_button=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35, 20, 35, 35)];
     [show_menu_button setBackgroundImage:[UIImage imageNamed:@"menu_btn.png"] forState:UIControlStateNormal];
     [headerView addSubview:show_menu_button];*/
    //  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //[show_menu_button addTarget:appDelegate.main_menu action:@selector(menu_btn_selected:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = YES;
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"MAIN Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    menuearr = [[NSMutableArray alloc] init];
    urlarr = [[NSMutableArray alloc] init];
    [menuearr addObject:@"لطفًا يتم تحميل الأخبار"];
    [urlarr addObject:@""];
    labText=0;
    //[self updateDisplay];
    [self startTimer];
    
    NSURL *url = [NSURL URLWithString:@"http://api.madarsoft.com/mut2/v1/new.aspx"];
    NSURLRequest* request = [NSURLRequest requestWithURL:url ];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ([data length] >0 && error == nil)
                               {
                                   // DO YOUR WORK HERE
                                   NSString* content = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                                   
                                   NSArray *allLines = [content componentsSeparatedByString: @"::"];
                                   
                                   for (int x=0; x <[allLines count]-1; x++) {
                                       NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
                                       NSArray *Lines = [line componentsSeparatedByString: @"||"];
                                       [menuearr addObject:[Lines objectAtIndex:0]];
                                       [urlarr addObject:[Lines objectAtIndex:1]];
                                       
                                   }
                                   if ([allLines count]>0) {
                                       [menuearr removeObjectAtIndex:0];
                                       [urlarr removeObjectAtIndex:0];
                                       
                                       [mainTable reloadData];
                                   }
                                   
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   NSLog(@"Nothing was downloaded.");
                               }
                               else if (error != nil){
                                   NSLog(@"Error = %@", error);
                               }
                               // ...
                           }];
    
    
    
    
   // [self drawTemp];
     NSURL *tempStr = [NSURL URLWithString:[NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=1939897&u=c"]];
    NSURLRequest* chRequest = [NSURLRequest requestWithURL:tempStr];
    
    [NSURLConnection sendAsynchronousRequest:chRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ([data length] >0 && error == nil)
                               {
                                   RXMLElement *parser = [RXMLElement elementFromXMLData:data];
                                   [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
                                       NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
                                       NSLog(@"txtgg%@",  txt);
                                       NSArray *tmp = [txt componentsSeparatedByString:@" "];
                                       NSString *currentTemp;
                                       if ([[tmp objectAtIndex:5]length ]>2) {
                                           currentTemp = [tmp objectAtIndex:6];
                                       }
                                       else {
                                           currentTemp = [tmp objectAtIndex:5];
                                       }
                                       if (currentTemp==NULL) {
                                           currentTemp = @"";
                                       }
                                       [citylabel setText:[NSLocalizedString(@"maka",@"test") stringByAppendingString:[NSString stringWithFormat:@" %@c",currentTemp]]];
                                   }];
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   
                                   [citylabel setText:[NSLocalizedString(@"maka",@"test") stringByAppendingString:[NSString stringWithFormat:@" 0c"]]];
                               }
                               else if (error != nil){
                                   [citylabel setText:[NSLocalizedString(@"maka",@"test") stringByAppendingString:[NSString stringWithFormat:@" 0c"]]];

                               }
                               
                           }];

    

    [self drawHeader];

    if ([_myData countForTable:@"Crowd"]== 0) {
        //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getCrowdData];
        // });
    }
    

   
}


- (void)viewDidUnload
{
   // [self setBackImage:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) drawHeader{
 
    //hijriDate
    Date melady_today;
	NSMutableArray* hijridate = [HijriTime toHegry:melady_today];
	int hegryToday[3];
	for(int i = 0 ; i < 3 ; i++)
		hegryToday[i] = [[hijridate objectAtIndex:i] intValue];
	int	month_number = hegryToday[1];
    int	year_number = hegryToday[2];
    NSString* month_name = NSLocalizedString([HijriTime getArabicMonth:month_number], @"");
	NSString* year=[NSString stringWithFormat:@"%ld",(long)year_number];
    
/*
    NSDate* date = [NSDate date];
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"YYYY/MM/dd"];*/
    //Get the string date
    // NSLocalizedString(_title_lable_text,@"test");
 //   NSString *str= [NSLocalizedString(@"maka",@"test") stringByAppendingString:[NSString stringWithFormat:@" %@c",[self getMacTemp]] ];
// NSString *str = [NSString stringWithFormat:@"%@c",[self getMacTemp]];
  //  citylabel.text = str;
    timeLabel.text = [NSString stringWithFormat:@"%d %@ %@",hegryToday[0],month_name,year];
    

    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count];
}
#pragma mark - news
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = nil;
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else
    {
        // the cell is being recycled, remove old embedded controls
        UIView *viewToRemove = nil;
        viewToRemove = [cell.contentView viewWithTag:kMyTag];
        if (viewToRemove)
            [viewToRemove removeFromSuperview];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

    cell.backgroundColor = [UIColor clearColor];
    UIView *cellView = [[UIView alloc]initWithFrame:cell.frame];
    UILabel *cellLab = [[UILabel alloc]initWithFrame:CGRectMake(11, 0, 235, 25)];
      UIView *labBgView = [[UIView alloc]initWithFrame:CGRectMake(11, 0, 245, 25)];
    UIImageView *rightDot = [[UIImageView alloc]initWithFrame:CGRectMake(247, 2, 21, 21)];
    rightDot.image = [UIImage imageNamed:@"dot.png"];
    
    UIImageView *leftDot = [[UIImageView alloc]initWithFrame:CGRectMake(0, 2, 21, 21)];
    leftDot.image = [UIImage imageNamed:@"dot.png"];
    
    if (indexPath.row % 2!=0) {
     labBgView.backgroundColor =[UIColor colorWithRed:0.929 green:0.914 blue:0.867 alpha:1] /*#ede9dd*/;
        cellLab.backgroundColor = [UIColor colorWithRed:0.929 green:0.914 blue:0.867 alpha:1] /*#ede9dd*/;
    }
    else{
       labBgView.backgroundColor =[UIColor colorWithRed:0.965 green:0.953 blue:0.925 alpha:1] /*#f6f3ec*/ ;
        cellLab.backgroundColor = [UIColor colorWithRed:0.965 green:0.953 blue:0.925 alpha:1] /*#f6f3ec*/ ;
  	}
  cellLab.text=[menuearr objectAtIndex:indexPath.row];

    cellLab.textAlignment = NSTextAlignmentRight;
    cellLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
    cellLab.numberOfLines = 0;
    cellLab.textColor = [UIColor colorWithRed:0.361 green:0.318 blue:0.208 alpha:1]; /*#5c5135*/
    cellLab.lineBreakMode = NSLineBreakByWordWrapping;
    
    [cellView addSubview:labBgView];
    [cellView addSubview:cellLab];
    [cellView addSubview:rightDot];
    [cellView addSubview:leftDot];
    [cell.contentView addSubview:cellView ];
    [cellLab release];
    [rightDot release];
    [leftDot release];
    [cellView release];
    
    
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
     /*
     DetailViewController *detailViewController = [[DetailViewController alloc] initWithNibName:@"Nib name" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    if ([urlarr count]>1) {
        
        _news_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        _news_view.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"makeNews from MAIN Screen/Iphone "];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        
       /* UIView *news_header=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        news_header.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
        [_news_view addSubview:news_header];*/
        UILabel *header_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,30)];
    //    header_lbl.text=NSLocalizedString(@"muatefNews", @"News");
         header_lbl.text=NSLocalizedString(@"muatefNews", @"News");
        [header_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
        header_lbl.textAlignment=NSTextAlignmentCenter;
        header_lbl.textColor=[UIColor whiteColor];
        [_news_view addSubview:header_lbl];
        UIButton *back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
        back_btn.layer.cornerRadius=12.5;
        back_btn.backgroundColor=[UIColor clearColor];
        [back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
        [back_btn addTarget:self action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];


        myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
        myWeb.delegate=self;
        [_news_view setFrame:CGRectMake( -self.view.frame.size.width,  0,self.view.frame.size.width, self.view.frame.size.height)]; //notice this is OFF screen!
        [UIView beginAnimations:@"animateTableView" context:nil];
        [UIView setAnimationDuration:0.4];
        [self.view addSubview:_news_view];
        [_news_view setFrame:CGRectMake( 0, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
        [UIView commitAnimations];
        [_news_view addSubview:myWeb];
        [_news_view addSubview:back_btn];
        NSString *urlAddress = [urlarr objectAtIndex:indexPath.row];
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"news" ofType:@"html" inDirectory:@"www"]];
        NSString *URLString = [url absoluteString];
        NSString *queryString =[NSString stringWithFormat:@"?id=%@",urlAddress];
        NSString *URLwithQueryString = [URLString stringByAppendingString: queryString];
        NSURL *finalURL = [NSURL URLWithString:URLwithQueryString];
        NSURLRequest *request = [NSURLRequest requestWithURL:finalURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)30.0 ];
        [myWeb loadRequest:request];
        [self.view bringSubviewToFront:_botimg];
    }
    
}



-(IBAction)btnClicked:(id)sender{
   
    _news_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _news_view.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LiveStream Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    
    /* UIView *news_header=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
     news_header.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
     [_news_view addSubview:news_header];*/
    UILabel *header_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width,30)];
  //  header_lbl.text=NSLocalizedString(@"muatefNews", @"News");
      header_lbl.text=@"البث المباشر";
    [header_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
    header_lbl.textAlignment=NSTextAlignmentCenter;
    header_lbl.textColor=[UIColor whiteColor];
    [_news_view addSubview:header_lbl];
    UIButton *back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
    back_btn.layer.cornerRadius=12.5;
    back_btn.backgroundColor=[UIColor clearColor];
    [back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [back_btn addTarget:self action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];
    
    
    myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height-60)];
    myWeb.delegate=self;
    [_news_view setFrame:CGRectMake( -self.view.frame.size.width,  0,self.view.frame.size.width, self.view.frame.size.height)]; //notice this is OFF screen!
    [UIView beginAnimations:@"animateTableView" context:nil];
    [UIView setAnimationDuration:0.4];
    [self.view addSubview:_news_view];
    [_news_view setFrame:CGRectMake( 0, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
    [UIView commitAnimations];
    [_news_view addSubview:myWeb];
    [_news_view addSubview:back_btn];
  //  NSString *urlAddress = [urlarr objectAtIndex:indexPath.row];
    NSURL *url;
    if ([sender tag]==1) {
           url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video" ofType:@"html" inDirectory:@"www"]];
    }
    else{
           url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"video2" ofType:@"html" inDirectory:@"www"]];
    }
   NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:(NSTimeInterval)10.0 ];
    [myWeb loadRequest:request];
    [self.view bringSubviewToFront:_botimg];

    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"ios:"]) {
        
        // Call the given selector
        [self performSelector:@selector(webToNativeCall)];
        // Cancel the location change
        return NO;
    }
    
    return YES;
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    NSLog(@"%d",text_size);
    [myWeb stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setsize(%d)",text_size]];
}

- (void)webToNativeCall
{
    NSString *returnvalue =  [myWeb stringByEvaluatingJavaScriptFromString:@"getsize()"];
    text_size=[returnvalue intValue];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"size"
                                                     ofType:@"txt"];
    NSString *size=[[NSString alloc] init];
    size=[NSString stringWithFormat:@"%d",text_size];
    [size writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    [[NSUserDefaults standardUserDefaults] setObject:size forKey:@"size_text"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)close_view:(id)sender{
    [UIView animateWithDuration:0.4 delay:0  options:0 animations: ^{
        [_news_view setFrame:CGRectMake( -self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)]; //notice this is ON screen!
    } completion: ^(BOOL completed) {
        [_news_view removeFromSuperview];
    }];
}
-(IBAction)removeNews:(id)sender{
    [myWeb removeFromSuperview];
}
-(void) drawMedArea{
    /// Ka3baView
    int hh ;
    if (highf ==1 ) {
        hh = 300;
    }
    else{
        hh = 348;
    }
    ka3baView = [[UIView alloc]initWithFrame:CGRectMake(0, 30*highf, 320, hh)];
    ka3baView.tag = 0;
    ka3baView.alpha = 1.0;
    [self.view addSubview:ka3baView];
    ka3baimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ka3ba.png"]];
    ka3baimg.frame = CGRectMake(0,-10*highf, 320, hh+10);
    ka3baimg.tag= 11;
    
    [self.ka3baView addSubview:ka3baimg];
    
    [self.view insertSubview:ka3baView belowSubview:headerView];
    [mobViews addObject:ka3baView];
    
    
    //// sa3yView
    sa3yView = [[UIView alloc]initWithFrame:CGRectMake(320, 30*highf, 320, hh)];
    sa3yView.tag = 1;
    sa3yView.alpha = 0.0;
    UIImageView *sa3yimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sa3yview.png"]];
    sa3yimg.frame = CGRectMake(0,-10*highf, 320, hh+10);
    sa3yimg.tag = 11;
    [self.sa3yView addSubview:sa3yimg];
    [self.view addSubview:sa3yView];
    [self.view insertSubview:sa3yView belowSubview:headerView];
    [mobViews addObject:sa3yView];
    
    
    ///jamarat
    jamaratView = [[UIView alloc]initWithFrame:CGRectMake(-320,30*highf, 320,hh)];
    jamaratView.tag = 2;
    jamaratView.alpha = 0.0;
    UIImageView *jamaratimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jamarat.png"]];
    jamaratimg.frame = CGRectMake(0,-10*highf, 320, hh+10);
    jamaratimg.tag =11;
    
    
    btn_refresh_clicked=0;
    refresh_btn=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35, 35)];
    [refresh_btn addTarget:self action:@selector(refresh_btn_click) forControlEvents:UIControlEventTouchUpInside];
    refresh_view=[[UIView alloc] initWithFrame:CGRectMake(5, jamaratView.frame.size.height-30, refresh_btn.frame.size.width+refresh_view.frame.size.width, refresh_btn.frame.size.height)];
    
    [refresh_btn setImage:[UIImage imageNamed:@"refresh.png"] forState:UIControlStateNormal];
    _first_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,20, 23)];
    _first_image.image=[UIImage imageNamed:@"refresh_left.png"];
    _refresh_lbl=[[UILabel alloc] init];
    
  /*  if ([last_time isEqualToString:@"لا يوجد اتصال بالانترنت"]) {
        _refresh_lbl.text=[NSString stringWithFormat:@"%@",last_time];
    }else{
        _refresh_lbl.text=[NSLocalizedString(@"lastUpdated", @"date") stringByAppendingString:[NSString stringWithFormat:@"%@",last_time ]];

     
    }*/
    [_refresh_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:12]];
    
    _refresh_lbl.textColor=[UIColor whiteColor];
  //  [_refresh_lbl sizeToFit];
    _refresh_lbl.frame=CGRectMake(_first_image.frame.origin.x+_first_image.frame.size.width,_first_image.frame.origin.y, self.view.frame.size.width-170,_first_image.frame.size.height);
    _refresh_lbl.backgroundColor=[UIColor colorWithRed:0.153 green:0.722 blue:0.714 alpha:0.7];
    _right_image=[[UIImageView alloc] initWithFrame:CGRectMake(_refresh_lbl.frame.size.width+_refresh_lbl.frame.origin.x,_refresh_lbl.frame.origin.y,30, _refresh_lbl.frame.size.height)];
    _right_image.image=[UIImage imageNamed:@"refresh_right.png"];
    refresh_lbl_view=[[UIView alloc] initWithFrame:CGRectMake(0, 8,0, 25)];
    [refresh_lbl_view addSubview:_first_image];
    [refresh_lbl_view addSubview:_right_image];
    [refresh_lbl_view addSubview:_refresh_lbl];
    
    [refresh_view addSubview:refresh_lbl_view];
    [refresh_view addSubview:refresh_btn];
    [refresh_btn.layer removeAnimationForKey:@"rotationanimation"];
    refresh_view.frame=CGRectMake(refresh_view.frame.origin.x, refresh_view.frame.origin.y, _first_image.frame.size.width+_right_image.frame.size.width+_refresh_lbl.frame.size.width+refresh_btn.frame.size.width, refresh_view.frame.size.height);
    
    //refresh_view.frame=CGRectMake(5, jamaratView.frame.size.height-30, refresh_btn.frame.size.width, refresh_btn.frame.size.height);
    refresh_lbl_view.clipsToBounds=YES;
    refresh_view.clipsToBounds=YES;
    
    [refresh_view addSubview:refresh_lbl_view];

    
    [self init_refresh_view];
    [refresh_view addSubview:refresh_btn];
    [self.view addSubview:refresh_view];
    
    [self.jamaratView addSubview:jamaratimg];
    [self.view addSubview:jamaratView];
    [self.view insertSubview:jamaratView belowSubview:headerView];
     [mobViews addObject:jamaratView];

    
}
-(void)refresh_btn_click{
    CAKeyframeAnimation *rotationanimation=[CAKeyframeAnimation animationWithKeyPath:@"transform"];
    rotationanimation.duration=1;
    rotationanimation.values=[NSArray arrayWithObjects:[NSValue valueWithCATransform3D:CATransform3DIdentity],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI, 0, 0, 1)],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(2*M_PI, 0, 0, 1)], nil];
    rotationanimation.repeatCount=HUGE_VALF;
    [refresh_btn.layer addAnimation:rotationanimation forKey:@"rotationanimation"];
    save_data_thread =[[NSThread alloc] initWithTarget:self selector:@selector(updateAndsave) object:nil];

    [save_data_thread start];
    btn_refresh_clicked=1;
}
-(void)init_refresh_view{
    
    if ([last_time isEqualToString:@"لا يوجد اتصال بالانترنت"]) {
        _refresh_lbl.text=[NSString stringWithFormat:@"%@",last_time];
    }else{
        _refresh_lbl.text=[NSLocalizedString(@"lastUpdated", @"date") stringByAppendingString:[NSString stringWithFormat:@"%@",last_time ]];
    }
   // _refresh_lbl.frame=CGRectMake(_first_image.frame.origin.x+_first_image.frame.size.width,_first_image.frame.origin.y, _refresh_lbl.frame.size.width+5,_first_image.frame.size.height);
    _refresh_lbl.frame=CGRectMake(_first_image.frame.origin.x+_first_image.frame.size.width,_first_image.frame.origin.y, self.view.frame.size.width,_first_image.frame.size.height);
    _right_image.frame=CGRectMake(_refresh_lbl.frame.size.width+_refresh_lbl.frame.origin.x,_refresh_lbl.frame.origin.y,30, _refresh_lbl.frame.size.height);

    refresh_lbl_view.frame=CGRectMake(24, 8, 0, 25);


}
-(void) stop_animation{
    /*CAKeyframeAnimation *rotationanimation=[CAKeyframeAnimation animationWithKeyPath:@"transform"];
    rotationanimation.duration=1;
    rotationanimation.values=[NSArray arrayWithObjects:[NSValue valueWithCATransform3D:CATransform3DIdentity],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI, 0, 0, 1)],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(2*M_PI, 0, 0, 1)], nil];
    rotationanimation.repeatCount=HUGE_VALF;
    
    [refresh_btn.layer addAnimation:rotationanimation forKey:@"rotationanimation"];*/
    
    CAKeyframeAnimation *rotationanimation1=[CAKeyframeAnimation animationWithKeyPath:@"transform"];
    rotationanimation1.duration=1;
    rotationanimation1.values=[NSArray arrayWithObjects:[NSValue valueWithCATransform3D:CATransform3DIdentity],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(3, 0,0, 1)],[NSValue valueWithCATransform3D:CATransform3DMakeRotation(6,0,0, 1)], nil];
    rotationanimation1.repeatCount=HUGE_VALF;
    [refresh_btn.layer addAnimation:rotationanimation1 forKey:@"rotationanimation"];

    [UIView animateWithDuration:1 delay:0 options:0 animations: ^{
        refresh_lbl_view.frame=CGRectMake(refresh_lbl_view.frame.origin.x, refresh_lbl_view.frame.origin.y, 0, refresh_lbl_view.frame.size.height);
        
    } completion: ^(BOOL completed) {
        [refresh_btn.layer removeAnimationForKey:@"rotationanimation"];

    }];
}

-(void) start_animation{
    [self init_refresh_view];
    [UIView animateWithDuration:1 delay:0 options:0 animations: ^{
        refresh_lbl_view.frame=CGRectMake(refresh_lbl_view.frame.origin.x, refresh_lbl_view.frame.origin.y, _first_image.frame.size.width+_refresh_lbl.frame.size.width+_right_image.frame.size.width, refresh_lbl_view.frame.size.height);
    } completion: ^(BOOL completed) {
        /*[refresh_btn.layer removeAnimationForKey:@"rotationanimation"];
        refresh_btn_timer = [[NSTimer alloc] scheduledTimerWithTimeInterval:6.0
                                         target:self
                                       selector:@selector(stop_animation)
                                       userInfo:nil
                                        repeats:NO];
        [refresh_btn_timer sc]*/
        [refresh_btn.layer removeAnimationForKey:@"rotationanimation"];

        [self performSelector:@selector(stop_animation) withObject:nil afterDelay:3.0];

    }];

}
-(void) drawMob{
    UIImageView *mobimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"live-bg.png"]];
    mobimg.frame = CGRectMake(10, 300*highf, 300, 40*highf);
        [self.view addSubview:mobimg];
    
    floorView = [[UIView alloc]initWithFrame:CGRectMake(10, 300*highf, 300, 40*highf)];
    floorView.backgroundColor = [UIColor clearColor];
    
    liveImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"live-active.png"]];
    liveImage.frame = CGRectMake(100, 0, 100, 42*highf);
    [self.floorView addSubview:liveImage];
    

    leftLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30*highf)];
    leftLab.backgroundColor = [UIColor clearColor];
    leftLab.textAlignment=NSTextAlignmentCenter;
    leftLab.textColor =[UIColor whiteColor];
    leftLab.text =NSLocalizedString(@"gamrat", @"main view");
    leftLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    [moblabs addObject:leftLab];
    [floorView addSubview:leftLab];
    
    leftbut = [UIButton buttonWithType:UIButtonTypeCustom];
    leftbut.frame =CGRectMake(0, 0, 100, 40*highf);
    leftbut.backgroundColor = [UIColor clearColor];
    leftbut.tag = 2;
    [leftbut addTarget:self action:@selector(moveMansk:) forControlEvents:UIControlEventTouchUpInside];
    [floorView addSubview:leftbut];

    
    centLab = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, 100, 30*highf)];
    centLab.backgroundColor = [UIColor clearColor];
    centLab.textAlignment=NSTextAlignmentCenter;
    centLab.textColor =[UIColor colorWithRed:0.369 green:0.388 blue:0.176 alpha:1]; /*#5e632d*/
    centLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    centLab.text = NSLocalizedString(@"tawaf", @"tawaf view");
    [moblabs addObject:centLab];
    [floorView addSubview:centLab];
    
    centbut = [UIButton buttonWithType:UIButtonTypeCustom];
    centbut.frame =CGRectMake(100, 0, 100,40*highf);
    centbut.backgroundColor = [UIColor clearColor];
    centbut.tag=0;
     [centbut addTarget:self action:@selector(moveMansk:) forControlEvents:UIControlEventTouchUpInside];
    [floorView addSubview:centbut];

    
    rightLab = [[UILabel alloc] initWithFrame:CGRectMake(200, 0, 100, 30*highf)];
    rightLab.backgroundColor = [UIColor clearColor];
    rightLab.textAlignment=NSTextAlignmentCenter;
    rightLab.textColor =[UIColor whiteColor];
    rightLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:14];
    rightLab.text =NSLocalizedString(@"sa3i", @"plist");
    [moblabs addObject:rightLab];
    [floorView addSubview:rightLab];
   
    rightbut = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbut.frame =CGRectMake(200, 0, 100, 40*highf);
    rightbut.backgroundColor = [UIColor clearColor];
    rightbut.tag = 1;
    [rightbut addTarget:self action:@selector(moveMansk:) forControlEvents:UIControlEventTouchUpInside];
    [floorView addSubview:rightbut];
    [self.view addSubview:floorView];
}
-(void) drawAnimatwithInt:(int)curTag{

    [ka3baimg setFrame:CGRectMake(ka3baimg.frame.origin.x, ka3baimg.frame.origin.y, 360, ka3baimg.frame.size.height)];

    [ka3baView setFrame:CGRectMake(ka3baView.frame.origin.x, ka3baView.frame.origin.y, 360, ka3baView.frame.size.height)];
   
    for(int i=0; i<3; i++){
        UIView *tmpView = (UIView *)[mobViews objectAtIndex:i];
    if ( tmpView.alpha == 0.0) {
            tmpView.alpha = 1.0;
        }

        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveLinear
                         animations:^{
                             if (tmpView.tag == curView){
                                 if (curTag==1) {
                                     [tmpView setFrame:CGRectMake(0-tmpView.frame.size.width, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                 }
                                 else if(curTag ==0){
                                     
                                     [UIView animateWithDuration:0.3f
                                                           delay:0.0f
                                                         options:UIViewAnimationCurveLinear
                                                      animations:^{
                                                          if (prevTag==1) {
                                                              [tmpView setFrame:CGRectMake(0+tmpView.frame.size.width, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                                          }
                                                          else if (prevTag==2){
                                                              [tmpView setFrame:CGRectMake(0-tmpView.frame.size.width, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                                          }
                                                      //
                                                          [ka3baView setFrame:CGRectMake(-40, ka3baView.frame.origin.y, 360, ka3baView.frame.size.height)];
                                                          
                                                      }
                                                      completion:^(BOOL finished){
                                                          if (tmpView.frame.origin.x >=320) {
                                                        //      tmpView.alpha = 0.0;
                                                          }
                                                        
                                                          
                                                      }];
                                     
                                     
                                     
                                 }
                                 else if (curTag==2){
                                     [tmpView setFrame:CGRectMake(0+tmpView.frame.size.width, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                     
                                 }
                             }
                             else if(tmpView.tag == curTag){
                                 if (tmpView.tag==0) {
                                     [tmpView setFrame:CGRectMake(-40, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                 }
                                 else{
                                     [tmpView setFrame:CGRectMake(0, tmpView.frame.origin.y, tmpView.frame.size.width, tmpView.frame.size.height)];
                                 }
                             }
                             
                         }
                         completion:^(BOOL finished){
                             
                             if (tmpView.frame.origin.x >=320) {
                               //  tmpView.alpha = 0.0;
                                 [UIView animateWithDuration:0.5f
                                                       delay:0.0f
                                                     options:UIViewAnimationCurveLinear
                                                  animations:^{
                                                  tmpView.alpha = 0.0;                                                  }
                                                  completion:^(BOOL finished){
                                                  }];
                             }

                         }];
        
        
    }
    if (curTag ==1) {
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseIn
                         animations:^{
                             [liveImage setFrame:CGRectMake(194, 0, 100, 40*highf)];
                             
                         }
                         completion:^(BOOL finished){
                             rightLab.textColor =[UIColor colorWithRed:0.369 green:0.388 blue:0.176 alpha:1];
                             centLab.textColor =[UIColor whiteColor];
                             leftLab.textColor =[UIColor whiteColor];
                             
                         }];
        
    }
    else if (curTag==0){
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseIn
                         animations:^{
                             [liveImage setFrame:CGRectMake(100, 0, 100, 40*highf)];
                             
                         }
                         completion:^(BOOL finished){
                             
                             leftLab.textColor =[UIColor whiteColor];
                             centLab.textColor =[UIColor colorWithRed:0.369 green:0.388 blue:0.176 alpha:1];
                             rightLab.textColor =[UIColor whiteColor];
                         }];
        
    }
    else if (curTag==2){
        [UIView animateWithDuration:0.3f
                              delay:0.0f
                            options:UIViewAnimationCurveEaseIn
                         animations:^{
                             [liveImage setFrame:CGRectMake(4, 0, 100, 40*highf)];
                             
                         }
                         completion:^(BOOL finished){
                             rightLab.textColor =[UIColor whiteColor];
                             centLab.textColor =[UIColor whiteColor];
                             leftLab.textColor =[UIColor colorWithRed:0.369 green:0.388 blue:0.176 alpha:1];
                             
                         }];
        
    }
    prevTag = curTag;
    curView = curTag;
    if (curView==0) {
        [self drawTawefAnimat];
    }
    else if (curView==1)
    {
        [self drawSa3ayAnimat];
    }
    else if (curView==2){
        [self drawJameratAnimat];
    }

    
}
-(IBAction)moveMansk:(id)sender{
    UIButton *btn=sender;
    int curTag =(int)[btn tag];
     if (curTag==prevTag) {
        return;
    }
    else{
        [self drawAnimatwithInt:curTag];
    }
   
    
    
    
    
}

-(void) drawTawefAnimat{
    [UIView animateWithDuration:1 delay:0 options:0 animations: ^{
        [ka3baimg setFrame:CGRectMake(ka3baimg.frame.origin.x, ka3baimg.frame.origin.y, 360, ka3baimg.frame.size.height)];
        
        [ka3baView setFrame:CGRectMake(-40, ka3baView.frame.origin.y, 360, ka3baView.frame.size.height)];
    } completion: ^(BOOL completed) {

        
    }];
    

    crowdId = 0;
     [self getCrowd];
    NSMutableArray *floordd = [[NSMutableArray alloc]initWithObjects:@"ground",@"pedestrianSuspesion",@"cartsSuspesion",@"firstFloor",@"surface",nil];
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
    for (UIView *vv in ka3baView.subviews) {
        if (vv.tag!=11) {
            [vv removeFromSuperview];
        }
        
    }
    
    if ([crowd count]>0) {
      
    
    for (int i=0;i<5;i++ ){
    int x = [[crowd objectAtIndex:i]intValue ]-1;

        NSString *str =NSLocalizedString([crowdNames objectAtIndex:x], @"textLabel");
         NSString *str2 = NSLocalizedString([floordd objectAtIndex:i], @"textlabel");
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", str2,str] ];
        
        // Set font, notice the range is for the whole string
        UIFont *font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange([str2 length], [str length])];
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, [str length])];

        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [str2 length])];
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor whiteColor]
                                 range:NSMakeRange(0, [str2 length])];

        CGSize size = [attributedString size];
        
        UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(260-(i*10), -10,size.width+20, 22)];
        tmpView.tag= i;
        tmpView.alpha = 0.0;
      //  tmpView.backgroundColor =[crowdColors objectAtIndex:x] ;
        
        [[tmpView layer] setCornerRadius:10.0f];
        [[tmpView layer] setMasksToBounds:YES];
        
        
        
        UILabel *tmpLab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width+6, 22)];
        tmpLab.backgroundColor =[crowdColors objectAtIndex:x] ;/*#fbbc1b*/
        [tmpLab setAttributedText:attributedString];
       
        tmpLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
        tmpLab.textAlignment = NSTextAlignmentCenter;
          [tmpView addSubview:tmpLab];

        UIImageView *tmpimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[crowdImgs objectAtIndex:x]]];
        tmpimg.frame = CGRectMake(size.width+5,0, 14, 22);
        [tmpView addSubview:tmpimg];
        [self.ka3baView addSubview:tmpView];
        [tmpArray addObject:tmpView];

        
       
        
    }
    
    
    [UIView animateWithDuration:0.5f
                          delay:0.1f
                        options:UIViewAnimationCurveLinear
                     animations:^{
                         UIView *vv = (UIView *)[tmpArray objectAtIndex:0];
                         vv.alpha= 0.9;
                         [vv setFrame:CGRectMake(vv.frame.origin.x, 220*highf, vv.frame.size.width, vv.frame.size.height)];
                         
                         
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5f
                                               delay:0.0f
                                             options:UIViewAnimationCurveLinear
                                          animations:^{
                                              UIView *vv = (UIView *)[tmpArray objectAtIndex:1];
                                              vv.alpha= 1.0;
                                              [vv setFrame:CGRectMake(vv.frame.origin.x, 200*highf, vv.frame.size.width, vv.frame.size.height)];
                                              
                                          }
                                          completion:^(BOOL finished){
                                              
                                              [UIView animateWithDuration:0.5f
                                                                    delay:0.0f
                                                                  options:UIViewAnimationCurveLinear
                                                               animations:^{
                                                                   UIView *vv = (UIView *)[tmpArray objectAtIndex:2];
                                                                   vv.alpha= 1.0;
                                                                   [vv setFrame:CGRectMake(vv.frame.origin.x, 180*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                   
                                                               }
                                                               completion:^(BOOL finished){
                                                                   
                                                                   
                                                                   [UIView animateWithDuration:0.5f
                                                                                         delay:0.0f
                                                                                       options:UIViewAnimationCurveLinear
                                                                                    animations:^{
                                                                                        UIView *vv = (UIView *)[tmpArray objectAtIndex:3];
                                                                                        vv.alpha= 1.0;
                                                                                        [vv setFrame:CGRectMake(vv.frame.origin.x, 160*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                        
                                                                                    }
                                                                                    completion:^(BOOL finished){
                                                                                        
                                                                                        [UIView animateWithDuration:0.5f
                                                                                                              delay:0.0f
                                                                                                            options:UIViewAnimationCurveLinear
                                                                                                         animations:^{
                                                                                                             UIView *vv = (UIView *)[tmpArray objectAtIndex:4];
                                                                                                             vv.alpha= 1.0;
                                                                                                             [vv setFrame:CGRectMake(vv.frame.origin.x, 140*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                                             
                                                                                                         }
                                                                                                         completion:^(BOOL finished){
                                                                                                             
                                                                                                             
                                                                                                             
                                                                                                         }];
                                                                                        
                                                                                    }];
                                                                   
                                                                   
                                                               }];
                                              
                                              
                                          }];
                         
                         
                         
                     }];
}

  
    
}
-(void) drawSa3ayAnimat{
    crowdId = 1;
   
      [self getCrowd];
    
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
     NSMutableArray *floordd = [[NSMutableArray alloc]initWithObjects:@"crypt",@"ground",@"firstFloor",@"secondFloor",@"surface1",@"surface2",nil];

    for (UIView *vv in sa3yView.subviews) {
        if (vv.tag!=11) {
             [vv removeFromSuperview];
        }
       
    }
     if ([crowd count]>0) {
    
    for (int i=0;i<6;i++ ){
         int x = [[crowd objectAtIndex:i]intValue ]-1;
        
        
        NSString *str = NSLocalizedString([crowdNames objectAtIndex:x], @"test");
        NSString *str2 =NSLocalizedString([floordd objectAtIndex:i], @"test") ;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", str2,str] ];
        
        // Set font, notice the range is for the whole string
        UIFont *font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange([str2 length], [str length])];
        
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange([str2 length], [str length])];
        
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [str2 length])];
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor whiteColor]
                                 range:NSMakeRange(0, [str2 length])];
        
      
         CGSize size = [attributedString size];
        UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(90-(i*15), -10,size.width+20, 22)];
        tmpView.tag= i;
        tmpView.alpha = 0.0;
        [[tmpView layer] setCornerRadius:10.0f];
        [[tmpView layer] setMasksToBounds:YES];
        
        UILabel *tmpLab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width+6, 22)];
        tmpLab.backgroundColor =[crowdColors objectAtIndex:x] ;
        [tmpLab setAttributedText:attributedString];
        tmpLab.textAlignment = NSTextAlignmentCenter;
        tmpLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
         [tmpView addSubview:tmpLab];
        
        
        
        UIImageView *tmpimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[crowdImgs objectAtIndex:x]]];
        tmpimg.frame = CGRectMake(size.width+5,0, 14, 22);
        [tmpView addSubview:tmpimg];
        [self.sa3yView addSubview:tmpView];
        [tmpArray addObject:tmpView];
        
    }
    
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationCurveLinear
                     animations:^{
                         UIView *vv = (UIView *)[tmpArray objectAtIndex:0];
                         vv.alpha= 1.0;
                         [vv setFrame:CGRectMake(vv.frame.origin.x, 217*highf, vv.frame.size.width, vv.frame.size.height)];
                         
                         
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5f
                                               delay:0.0f
                                             options:UIViewAnimationCurveLinear
                                          animations:^{
                                              UIView *vv = (UIView *)[tmpArray objectAtIndex:1];
                                              vv.alpha= 1.0;
                                              [vv setFrame:CGRectMake(vv.frame.origin.x, 195*highf, vv.frame.size.width, vv.frame.size.height)];
                                              
                                          }
                                          completion:^(BOOL finished){
                                              
                                              [UIView animateWithDuration:0.5f
                                                                    delay:0.0f
                                                                  options:UIViewAnimationCurveLinear
                                                               animations:^{
                                                                   UIView *vv = (UIView *)[tmpArray objectAtIndex:2];
                                                                   vv.alpha=1.0;
                                                                   [vv setFrame:CGRectMake(vv.frame.origin.x, 173*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                   
                                                               }
                                                               completion:^(BOOL finished){
                                                                   [UIView animateWithDuration:0.5f
                                                                                         delay:0.0f
                                                                                       options:UIViewAnimationCurveLinear
                                                                                    animations:^{
                                                                                        UIView *vv = (UIView *)[tmpArray objectAtIndex:3];
                                                                                        vv.alpha= 1.0;
                                                                                        [vv setFrame:CGRectMake(vv.frame.origin.x, 151*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                        
                                                                                    }
                                                                                    completion:^(BOOL finished){
                                                                                        [UIView animateWithDuration:0.5f
                                                                                                              delay:0.0f
                                                                                                            options:UIViewAnimationCurveLinear
                                                                                                         animations:^{
                                                                                                             UIView *vv = (UIView *)[tmpArray objectAtIndex:4];
                                                                                                             vv.alpha= 1.0;
                                                                                                             [vv setFrame:CGRectMake(vv.frame.origin.x, 129*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                                             
                                                                                                         }
                                                                                                         completion:^(BOOL finished){
                                                                                                             [UIView animateWithDuration:0.5f
                                                                                                                                   delay:0.0f
                                                                                                                                 options:UIViewAnimationCurveLinear
                                                                                                                              animations:^{
                                                                                                                                  UIView *vv = (UIView *)[tmpArray objectAtIndex:5];
                                                                                                                                  vv.alpha= 1.0;
                                                                                                                                  [vv setFrame:CGRectMake(vv.frame.origin.x, 107*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                                                                  
                                                                                                                              }
                                                                                                                              completion:^(BOOL finished){
                                                                                                                                  
                                                                                                                                  
                                                                                                                              }];
                                                                                                             
                                                                                                         }];
                                                                                        
                                                                                    }];
                                                                   
                                                               }];
                                              
                                              
                                          }];
                         
                         
                         
                     }];
    }
    
    
}
-(void) drawJameratAnimat{
    
    
    crowdId = 2;
    [self getCrowd];
     if ([crowd count]>0) {
         
    NSMutableArray *tmpArray = [[NSMutableArray alloc]init];
  //  NSMutableArray *crowddd = [[NSMutableArray alloc]initWithObjects: @"2",@"4",@"1",@"3",nil ];

         NSMutableArray *floordd = [[NSMutableArray alloc]initWithObjects:@"carts",@"pedestrianSurafce",@"pedestrianGround",@"pedestrianFirstFloor",@"suspensionBridge", nil];
    for (UIView *vv in jamaratView.subviews) {
        if (vv.tag!=11) {
            [vv removeFromSuperview];
        }
        
    }
    
    for (int i=0;i<4;i++ ){
        int x = [[crowd objectAtIndex:i]intValue]-1;
        
        NSString *str =NSLocalizedString([crowdNames objectAtIndex:x], @"[crowdNames objectAtIndex:x]") ;
        NSString *str2 =NSLocalizedString([floordd objectAtIndex:i], @"[floordd objectAtIndex:i]") ;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", str2,str] ];
        
        // Set font, notice the range is for the whole string
        UIFont *font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange([str2 length], [str length])];
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, [str length])];
        
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [str2 length])];
        
        // Set foreground color for entire range
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor whiteColor]
                                 range:NSMakeRange(0, [str2 length])];
        CGSize size = [attributedString size];
        
        UIView *tmpView = [[UIView alloc]initWithFrame:CGRectMake(102-(i*20), -10,size.width+20, 22)];
       /* if (i==2) {
            [tmpView setFrame:CGRectMake(30, -10,size.width+20, 22)];
        }
        else if(i==3){
            [tmpView setFrame:CGRectMake(102-(i*25), -10,size.width+20, 22)];
        }*/
        tmpView.tag= i;
        tmpView.alpha = 0.0;
        [[tmpView layer] setCornerRadius:10.0f];
        [[tmpView layer] setMasksToBounds:YES];
        
        UILabel *tmpLab  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width+6, 20)];
        tmpLab.backgroundColor =[crowdColors objectAtIndex:x] ;
        tmpLab.textAlignment = NSTextAlignmentCenter;
        [tmpLab setAttributedText:attributedString];
        tmpLab.font = [UIFont fontWithName:@"Arial-BoldMT" size:12];
                [tmpView addSubview:tmpLab];
        
        
        
        UIImageView *tmpimg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[crowdImgs objectAtIndex:x]]];
        tmpimg.frame = CGRectMake(size.width+5,0, 14, 20);
        [tmpView addSubview:tmpimg];
        [self.jamaratView addSubview:tmpView];
        [tmpArray addObject:tmpView];
        
    }
    
    
    [UIView animateWithDuration:0.5f
                          delay:0.1f
                        options:UIViewAnimationCurveLinear
                     animations:^{
                         UIView *vv = (UIView *)[tmpArray objectAtIndex:0];
                         vv.alpha= 1.0;
                         [vv setFrame:CGRectMake(vv.frame.origin.x, 210*highf, vv.frame.size.width, vv.frame.size.height)];
                         
                         
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.5f
                                               delay:0.0f
                                             options:UIViewAnimationCurveLinear
                                          animations:^{
                                              UIView *vv = (UIView *)[tmpArray objectAtIndex:1];
                                              vv.alpha= 1.0;
                                              [vv setFrame:CGRectMake(vv.frame.origin.x, 190*highf, vv.frame.size.width, vv.frame.size.height)];
                                              
                                          }
                                          completion:^(BOOL finished){
                                              
                                              [UIView animateWithDuration:0.5f
                                                                    delay:0.0f
                                                                  options:UIViewAnimationCurveLinear
                                                               animations:^{
                                                                   UIView *vv = (UIView *)[tmpArray objectAtIndex:2];
                                                                   vv.alpha= 1.0;
                                                                   [vv setFrame:CGRectMake(vv.frame.origin.x, 170*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                   
                                                               }
                                                               completion:^(BOOL finished){
                                                                   [UIView animateWithDuration:0.5f
                                                                                         delay:0.0f
                                                                                       options:UIViewAnimationCurveLinear
                                                                                    animations:^{
                                                                                        UIView *vv = (UIView *)[tmpArray objectAtIndex:3];
                                                                                        vv.alpha= 1.0;
                                                                                        [vv setFrame:CGRectMake(vv.frame.origin.x, 150*highf, vv.frame.size.width, vv.frame.size.height)];
                                                                                        
                                                                                    }
                                                                                    completion:^(BOOL finished){
                                                                                        
                                                                                    }];
                                                                   
                                                               }];
                                              
                                              
                                          }];
                         
                         
                         
                     }];
    
     }
    
}

-(void) drawTimePlayers
{
    
    NSDate* curDate = [NSDate date];
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* dateOnlyToday = [cal components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit)fromDate:curDate];
    int d = (int)[dateOnlyToday day];
    int m = (int)[dateOnlyToday month];
    int y = (int)[dateOnlyToday year];
    PrayersCaculator *calM = [[PrayersCaculator alloc]initWithDay:d Month:m Year:y Lat:21.4266667 Long:39.8261111 Zone:3 Mazhab:1 Way:3 DLS:0];
    self.prayerTimes = [calM getPrayersStrings];
    self.prayTimes = [calM getPrayersDoubles];
  
    // [calM release];
    praysname = [[NSMutableArray alloc] initWithObjects:@"fajr",@"shrooq",@"dohr",@"3asr",@"mghreb",@"3esha2",nil];
    prayIndex = [self getPrayerID];
    int pmFlag = [self getPrayerPM];
    
    labId = prayIndex;
    int hh, ihh ;
    if (highf ==1 ) {
        hh = 25;
        ihh = 63;
    }
    else
    {
        hh = 30*highf;
        ihh = 68;
    }
    prayImage =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"active-pray.png"]];
    prayImage.frame = CGRectMake(116, hh, 88, ihh);
    [self.view addSubview:prayImage];
    
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(255, 60*highf, 30, 30*highf);
    rightBtn.tag = 1;
    [rightBtn setImage:[UIImage imageNamed:@"rightBtn.png"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(moveView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rightBtn];
    
    leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(42, 60*highf, 30, 30*highf);
    leftBtn.tag =2;
    [leftBtn setImage:[UIImage imageNamed:@"leftBtn.png"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(moveView:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:leftBtn];
    
    for (int i=5; i>-1; i--) {
        
        NSString *str =NSLocalizedString([praysname objectAtIndex:i], @"fromPrayTime") ;
        NSString *str2 = [prayerTimes objectAtIndex:i];
        NSString *pm;
        if (i>=pmFlag) {
            pm = @"PM";
        }
        else{
            pm = @"AM";
        }
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ \n%@%@", str,str2,pm] ];
        
        // Set font, notice the range is for the whole string
        UIFont *font = [UIFont fontWithName:@"Arial-BoldMT" size:6.0];// arabic 8
        [attributedString addAttribute:NSFontAttributeName value:font range:NSMakeRange([str length]+[str2 length]+2, 2)];
        
        UIFont *font2 = [UIFont fontWithName:@"Arial-BoldMT" size:10.0];// arabic 12
        
        [attributedString addAttribute:NSFontAttributeName value:font2 range:NSMakeRange( [str length]+2, [str2 length])];
        
        UIFont *font3 = [UIFont fontWithName:@"Arial-BoldMT" size:10.0];// arabic 14
        
        [attributedString addAttribute:NSFontAttributeName value:font3 range:NSMakeRange( 0, [str length])];
        
        UILabel *ttLabel = [[UILabel alloc] initWithFrame:CGRectMake((140-((i-prayIndex)*80)), 52*highf, 45, 45*highf)];
        ttLabel.backgroundColor = [UIColor clearColor];
        ttLabel.alpha = 1.0;
        ttLabel.tag = i;
        
        [ttLabel setAttributedText:attributedString];
        if (i==prayIndex) {
            ttLabel.textColor = [UIColor whiteColor];
            [ttLabel setFrame:CGRectMake((140-((i-prayIndex)*80)), 33*highf, 45, 45*highf)];
        }
        else{
            ttLabel.textColor = [UIColor colorWithRed:0.639 green:0.627 blue:0.42 alpha:1]; /*#a3a06b*/
        }
        if (ttLabel.frame.origin.x < 60 || timeLabel.frame.origin.x > 220 ) {
             ttLabel.alpha = 0.0;
        }
        
        
        ttLabel.textAlignment=NSTextAlignmentCenter;
        ttLabel.numberOfLines =0;
        
        [self.view addSubview:ttLabel];
        [praysLab addObject:ttLabel];
        [ttLabel release];
        
    }


}


-(IBAction)moveView:(id)sender{
    [prayImage setImage:[UIImage imageNamed:@"unactive-pray.png"]];
    UIButton *btn=sender;
    if ((int)[btn tag]==1) {
        if ( labId>=5) {
            return;
        }
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationCurveLinear
                         animations:^{
                             
                             for (int i=5; i>-1; i--) {
                                 UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                 tmplab.alpha = 1.0;
                                 
                                 if (tmplab.tag==labId+1) {
                                     [tmplab setCenter:CGPointMake(160, 59*highf)];
                                 }
                                 else if (tmplab.tag==labId+2){
                                     [tmplab setCenter:CGPointMake(80, 75*highf)];
                                 }
                                 else if (tmplab.tag==labId){
                                     [tmplab setCenter:CGPointMake(240, 75*highf)];
                                 }
                                 else {
                                     [tmplab setCenter:CGPointMake(tmplab.center.x+60,77*highf)];
                                     tmplab.alpha = 0.0;
                                 }
                                 if (tmplab.tag==prayIndex) {
                                     tmplab.textColor =[UIColor colorWithRed:0.29 green:0.753 blue:0.706 alpha:1] /*#4ac0b4*/;
                                     
                                 }
                             }
                             
                         }
                         completion:^(BOOL finished){
                             for (int i=5; i>-1; i--) {
                                 UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                 if (tmplab.center.x==160) {
                                     if (tmplab.tag==prayIndex) {
                                         tmplab.textColor =[UIColor whiteColor] ;
                                         [prayImage setImage:[UIImage imageNamed:@"active-pray.png"]];
                                     }
                                 }
                             }
                             
                         }];
        
        labId++;
    }
    else{
        if ( labId<=0) {
            return;
        }
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationCurveLinear
                         animations:^{
                             
                             for (int i=0; i<6; i++) {
                                 UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                 tmplab.alpha = 1.0;
                                 if (tmplab.tag==labId-1){
                                     [tmplab setCenter:CGPointMake(160, 57*highf)];
                                 }
                                 else if (tmplab.tag==labId-2){
                                     [tmplab setCenter:CGPointMake(240, 75*highf)];
                                 }
                                 else if (tmplab.tag==labId){
                                     [tmplab setCenter:CGPointMake(80, 75*highf)];
                                 }
                                 else {
                                     [tmplab setCenter:CGPointMake(tmplab.center.x-60,77*highf)];
                                     tmplab.alpha = 0.0;
                                 }
                                 if (tmplab.tag==prayIndex) {
                                     tmplab.textColor =[UIColor colorWithRed:0.29 green:0.753 blue:0.706 alpha:1] /*#4ac0b4*/;
                                     
                                 }
                             }
                             
                         }
                         completion:^(BOOL finished){
                             for (int i=5; i>-1; i--) {
                                 UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                 //tmplab.alpha = 1.0;
                                 
                                 
                                 
                                 if (tmplab.center.x==160) {
                                     if (tmplab.tag==prayIndex) {
                                         tmplab.textColor =[UIColor whiteColor] ;
                                         [prayImage setImage:[UIImage imageNamed:@"active-pray.png"]];
                                     }
                                     
                                     
                                 }
                             }
                         }];
        
        labId--;
        
    }
    
    
}

- (void)move:(UIPanGestureRecognizer *)gestureRecognizer
{
    if ( [gestureRecognizer state] == UIGestureRecognizerStateEnded )
    {
        CGPoint location = [gestureRecognizer locationInView:gestureRecognizer.view];
        CGRect someRect = CGRectMake(0, 0, 320, 90*highf);
        CGRect sRect = CGRectMake(0, 110*highf, 320, 250*highf);
        CGPoint velocity = [gestureRecognizer velocityInView:self.view];
        if (CGRectContainsPoint(someRect, location)) {
            [prayImage setImage:[UIImage imageNamed:@"unactive-pray.png"]];
            if(velocity.x > 0)//right
            {
                if ( labId>=5) {
                    return;
                }
                [UIView animateWithDuration:0.5f
                                      delay:0.0f
                                    options:UIViewAnimationCurveLinear
                                 animations:^{
                                     
                                     for (int i=5; i>-1; i--) {
                                         UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                         
                                         
                                         tmplab.alpha = 1.0;
                                         
                                         if (tmplab.tag==labId+1) {
                                             [tmplab setCenter:CGPointMake(160, 57*highf)];
                                         }
                                         else if (tmplab.tag==labId+2){
                                             [tmplab setCenter:CGPointMake(80, 77*highf)];
                                         }
                                         else if (tmplab.tag==labId){
                                             [tmplab setCenter:CGPointMake(240, 77*highf)];
                                         }
                                         else {
                                             [tmplab setCenter:CGPointMake(tmplab.center.x+60,77*highf)];
                                             // tmplab.hidden = YES;
                                             tmplab.alpha = 0.0;
                                         }
                                         if (tmplab.tag==prayIndex) {
                                             tmplab.textColor =[UIColor colorWithRed:0.29 green:0.753 blue:0.706 alpha:1] /*#4ac0b4*/;
                                             
                                         }
                                     }
                                     
                                 }
                                 completion:^(BOOL finished){
                                     for (int i=5; i>-1; i--) {
                                         UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                         if (tmplab.center.x==160) {
                                             if (tmplab.tag==prayIndex) {
                                                 tmplab.textColor =[UIColor whiteColor] ;
                                                 [prayImage setImage:[UIImage imageNamed:@"active-pray.png"]];
                                             }
                                         }
                                     }
                                     
                                     
                                 }];
                labId++;
            }
            else if(velocity.x < 0)//left
            {
                if ( labId<=0) {
                    return;
                }
                [UIView animateWithDuration:0.5f
                                      delay:0.0f
                                    options:UIViewAnimationCurveLinear
                                 animations:^{
                                     
                                     for (int i=0; i<6; i++) {
                                         UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                         
                                         /*  if (tmplab.hidden) {
                                          tmplab.hidden = NO;
                                          }*/
                                         tmplab.alpha = 1.0;
                                         
                                         if (tmplab.tag==labId-1){
                                             [tmplab setCenter:CGPointMake(160, 57*highf)];
                                             
                                         }
                                         else if (tmplab.tag==labId-2){
                                             [tmplab setCenter:CGPointMake(240, 77*highf)];
                                             
                                         }
                                         else if (tmplab.tag==labId){
                                             [tmplab setCenter:CGPointMake(80, 77*highf)];
                                             
                                         }
                                         
                                         
                                         
                                         else {
                                             [tmplab setCenter:CGPointMake(tmplab.center.x-60,77*highf)];
                                             //tmplab.hidden = YES;
                                             tmplab.alpha = 0.0;
                                         }
                                         //[tmplab setAlpha:1];
                                         if (tmplab.tag==prayIndex) {
                                             tmplab.textColor =[UIColor colorWithRed:0.29 green:0.753 blue:0.706 alpha:1] /*#4ac0b4*/;
                                             
                                         }
                                     }
                                     
                                 }
                                 completion:^(BOOL finished){
                                     for (int i=5; i>-1; i--) {
                                         UILabel *tmplab = (UILabel *)[praysLab objectAtIndex:i];
                                         if (tmplab.center.x==160) {
                                             if (tmplab.tag==prayIndex) {
                                                 tmplab.textColor =[UIColor whiteColor] ;
                                                 [prayImage setImage:[UIImage imageNamed:@"active-pray.png"]];
                                             }
                                         }
                                     }
                                 }];
                labId--;
            }
        }
        else if (CGRectContainsPoint(sRect, location)) {
            viewId = curView;
            
            if(velocity.x > 0)//right
            {
                viewId--;
                if (viewId<0) {
                    viewId=2;
                    //  viewId=0;
                    // return;
                }
                //[self drawAnimatwithInt:2];
            }
            else if(velocity.x < 0)//left
            {
                viewId++;
                if (viewId>2) {
                    viewId=0;
                    // viewId=2;
                    //  return;
                }
                //[self drawAnimatwithInt:1];
            }
            [self drawAnimatwithInt:viewId];
        }
        
        
        
    }
    
}

-(int)  getPrayerID
{
    Date dt;
    double curTimeDouble = dt.getClockDouble();
//    NSLog(@"%f",curTimeDouble);
    
    int idd=5;
    for (int i=0; i<6; i++) {
        /**double time = [[prayTimes objectAtIndex:5-i]doubleValue];
         NSLog(@"%f",time);
        if (curTimeDouble > time) {
            return idd=i;
        }*/
        double time = [[prayTimes objectAtIndex:i]doubleValue];
//        NSLog(@"%f",time);
        if ( time > curTimeDouble) {
            return idd=i;
        }
    }
    return idd;
}
-(int)  getPrayerPM{
    int idd=5;
    for (int i=0; i<6; i++) {
        /**double time = [[prayTimes objectAtIndex:5-i]doubleValue];
         NSLog(@"%f",time);
         if (curTimeDouble > time) {
         return idd=i;
         }*/
        double time = [[prayTimes objectAtIndex:i]doubleValue];
       // NSLog(@"%f",time);
        if ( time > 12.0) {
            return idd=i;
        }
    }
    return idd;

}

-(NSString *) getMacTemp{
    
    NSString *Mtemps =[NSString stringWithFormat:@""] ;
    NSMutableArray * temps = [[NSMutableArray alloc] init ];
    
    

        NSString *tmpUrl = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=1939897&u=c"];
        
        
        NSURL *url = [NSURL URLWithString:tmpUrl];
        NSURLRequest* chRequest = [NSURLRequest requestWithURL:url cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval:5];
        NSError* theError;
        NSData* response = [NSURLConnection sendSynchronousRequest:chRequest returningResponse:nil error:&theError];
        RXMLElement *parser = [RXMLElement elementFromXMLData:response];
        [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
            NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
            NSArray *tmp = [txt componentsSeparatedByString:@" "];
            NSString *currentTemp;
            if ([[tmp objectAtIndex:5]length ]>2) {
                currentTemp = [tmp objectAtIndex:6];
            }
            else {
                currentTemp = [tmp objectAtIndex:5];
            }
            if (currentTemp==NULL) {
                currentTemp = @"";
            }
            [temps addObject:currentTemp];
        }];
  
    
    
    if ([temps count]==0) {
        for (int i=0; i<2; i++) {
            [temps addObject:@"0"];
        }
    }
    Mtemps = [temps objectAtIndex:0];
    NSString *str= [NSLocalizedString(@"maka",@"test") stringByAppendingString:[NSString stringWithFormat:@" %@c",[temps objectAtIndex:0]] ];
    [citylabel setText:str];
    
    NSLog(@"gg%@",[temps objectAtIndex:0]);
    return Mtemps;
    
  
    

   
}

-(void)getCrowdData{
    
    NSMutableArray *urlStr = [[NSMutableArray alloc]initWithObjects:@"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=1",
    @"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=2",
    @"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=3",nil];
    
    for (int a=0; a<3; a++){
        NSURL *url = [NSURL URLWithString:[urlStr objectAtIndex:a]];
        NSURLRequest* request = [NSURLRequest requestWithURL:url ];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                                   
                                   if ([data length] >0 && error == nil)
                                   {
                                       // DO YOUR WORK HERE
                                       NSString* content = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
                                       NSArray *allLines = [content componentsSeparatedByString: @","];
                                       if ([allLines count]>1) {
                                           [_myData deleteFromTable:@"Crowd" withField:@"crowd_Id" ByID:a ];
                                           
                                       }
                                       for (int x=0; x <[allLines count]-1; x++) {
                                           @try {
                                               [_myData insertRecordIntoTableNamed:@"Crowd" withField:@"crowd_Id,crowd_data,date" withValue:[NSString stringWithFormat:@"%d,%@,%@",a,[allLines objectAtIndex:x],[allLines objectAtIndex:[allLines count]-1]]];
                                           } @catch(NSException *theException) {
                                           }
                                       }
                                   }
                                   else if ([data length] == 0 && error == nil)
                                   {
                                       NSLog(@"Nothing was downloaded.");
                                   }
                                   else if (error != nil){
                                       NSLog(@"Error = %@", error);
                                   }
                                   // ...
                               }];
        }
        
    
    
    
}

-(void)updateAndsave{
    
    
    crowd = [[NSMutableArray alloc] init];
    NSURL *url;
    
    if (crowdId==0) {
        url = [NSURL URLWithString:@"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=1"];
    }
    else if (crowdId==1){
        url = [NSURL URLWithString:@"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=2"];
    }
    else if (crowdId==2){
        url = [NSURL URLWithString:@"http://api.madarsoft.com/mutawef/v1/newcrowling2.aspx?pid=3"];
    }
    
    
    

    NSURLRequest* request = [NSURLRequest requestWithURL:url cachePolicy:0 timeoutInterval:10];
    NSURLResponse* response=nil;
    NSError* error=nil;
    NSData* data=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSString* content = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"%@",content);
    NSArray *allLines = [content componentsSeparatedByString: @","];
    if ([allLines count]>1) {
        [_myData deleteFromTable:@"Crowd" withField:@"crowd_Id" ByID:crowdId ];
        
    }
    for (int x=0; x <[allLines count]-1; x++) {
        @try {
            [_myData insertRecordIntoTableNamed:@"Crowd" withField:@"crowd_Id,crowd_data,date" withValue:[NSString stringWithFormat:@"%d,%@,%@",crowdId,[allLines objectAtIndex:x],[allLines objectAtIndex:[allLines count]-1]]];
        } @catch(NSException *theException) {
        }
    }
    
   crowd=[_myData getdata:[NSString stringWithFormat:@"SELECT crowd_data from Crowd where crowd_Id=%d",crowdId] forType:@"text"];
    
    if ([crowd count]>0) {
        last_time=[[_myData getdata:[NSString stringWithFormat:@"SELECT date from Crowd where crowd_Id=%d",crowdId] forType:@"text"] objectAtIndex:0];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[last_time integerValue]];
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
         [formatter1 setDateFormat:@"yyyy/MM/dd"];
        [formatter2 setDateFormat:@"HH:mm:ss"];
        
        [formatter1 setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"ar_SA"]];
        
        
        NSDateComponents *gregorianComponents = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit  fromDate:date];
        
        int hhour =(int)[gregorianComponents hour];
        int mminte =  (int)[gregorianComponents minute];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 7.1) {
            // Then create an Islamic calendar
            NSCalendar *hijriCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSIslamicCalendar];
            // And grab those date components for the same date
            NSDateComponents *hijriComponents = [hijriCalendar components:(NSDayCalendarUnit |NSMonthCalendarUnit |NSYearCalendarUnit
                                                                           |NSMinuteCalendarUnit|NSHourCalendarUnit)
                                                                 fromDate:date];
            
            last_time=[[[NSString stringWithFormat:@"%ld/%ld/%ld ",(long)[hijriComponents year],(long)[hijriComponents month],(long)[hijriComponents day]]stringByAppendingString:NSLocalizedString(@"clock", @"elsa3a")]stringByAppendingString:[NSString stringWithFormat:@"%d:%d",hhour,mminte]];
            NSLog(@"%@", last_time);
        
        }
        else{
            //     NSString *selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
            last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "] stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date]];
            /*  if ([selectedLanguage isEqualToString:@"ar"]) {
             last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "] stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date]];
             }else{
             last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date] ]stringByAppendingString:@" "]stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")];
             }*/
        }
    }
    else{
        last_time = @"لا يوجد اتصال بالانترنت";
        
        
    
    }
    if (btn_refresh_clicked==1) {
        [self start_animation];
        btn_refresh_clicked=0;
        [self performSelector:@selector(updateAndsave) onThread:save_data_thread withObject:nil waitUntilDone:NO];
        save_data_thread = nil;
        if (crowdId==0) {
            [self drawTawefAnimat];
        }
        else if (crowdId==1)
        {
            [self drawSa3ayAnimat];
        }
        else if (crowdId==2){
            [self drawJameratAnimat];
        }
       
    }
    
    
    
    
}
- (void) findContact{
//    NSMutableArray* arrids=[_myData getdata:[NSString stringWithFormat:@"SELECT * from Crowd"] forType:@"text"];
}
-(void) getCrowd {
    
    crowd = [[NSMutableArray alloc] init];
crowd=[_myData getdata:[NSString stringWithFormat:@"SELECT crowd_data from Crowd where crowd_Id=%d",crowdId] forType:@"text"];
    if ([crowd count]>0) {
        last_time=[[_myData getdata:[NSString stringWithFormat:@"SELECT date from Crowd where crowd_Id=%d",crowdId] forType:@"text"] objectAtIndex:0];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[last_time integerValue]];
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
        //[formatter1 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC+3"]];
        // [formatter1 setDateFormat:@"yyyy/MM/dd الساعة HH:mm:ss"];
        [formatter1 setDateFormat:@"yyyy/MM/dd"];
        [formatter2 setDateFormat:@"HH:mm:ss"];
        
        [formatter1 setLocale:[[NSLocale alloc]initWithLocaleIdentifier:@"ar_SA"]];
        
        
        NSDateComponents *gregorianComponents = [[NSCalendar currentCalendar] components:NSHourCalendarUnit | NSMinuteCalendarUnit  fromDate:date];
        
        int hhour =(int)[gregorianComponents hour];
        int mminte =  (int)[gregorianComponents minute];
        
        if ([[[UIDevice currentDevice] systemVersion] floatValue] <= 7.1) {
            // Then create an Islamic calendar
            NSCalendar *hijriCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSIslamicCalendar];
            // And grab those date components for the same date
            NSDateComponents *hijriComponents = [hijriCalendar components:(NSDayCalendarUnit |NSMonthCalendarUnit |NSYearCalendarUnit
                                                                           |NSMinuteCalendarUnit|NSHourCalendarUnit)
                                                                 fromDate:date];
            
            last_time=[[[NSString stringWithFormat:@"%ld/%ld/%ld ",(long)[hijriComponents year],(long)[hijriComponents month],(long)[hijriComponents day]]stringByAppendingString:NSLocalizedString(@"clock", @"elsa3a")]stringByAppendingString:[NSString stringWithFormat:@"%d:%d",hhour,mminte]];
            //               last_time = [NSString stringWithFormat:@"%ld/%ld/%ld  %d:%d",(long)[hijriComponents year],(long)[hijriComponents month],(long)[hijriComponents day],hhour,mminte];
        }
        else{
            //     NSString *selectedLanguage = [[NSLocale preferredLanguages] objectAtIndex:0];
            last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "] stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date]];
            /*  if ([selectedLanguage isEqualToString:@"ar"]) {
             last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "] stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date]];
             }else{
             last_time=[[[[[formatter1 stringFromDate:date]stringByAppendingString:@" "]stringByAppendingString:[formatter2 stringFromDate:date] ]stringByAppendingString:@" "]stringByAppendingString:NSLocalizedString(@"clock", @"sa3a")];
             }*/
        }
    }
    else{
        last_time = @" قم بتحميل البيانات من النت أولاُ ";
     }
    
}
-(void) startTimer{
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                  target:self
                                                selector:@selector(updateDisplay)
                                                userInfo:nil
                                                 repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    [self.timer fire];
    
}
-(void)updateDisplay{
    if (labText ==0) {
        
       [self.first_title setText:@"إمارة منطقة مكة المكرمة"];
        labText =1;
        
    }
    else if (labText==1){
       [self.first_title setText:@"لجنة الحج المركزية"];
        labText =0;
        
    }

}



@end
