//
//  ContainerViewController.h
//  GadawalV1
//
//  Created by waleed on 8/25/13.
//
//
#import "oldImg.h"
#import "SKMenu.h"
#import <UIKit/UIKit.h>
#import "adsViewController.h"

//@interface ContainerViewController : UIViewController
@interface ContainerViewController : adsViewController

@property (nonatomic, retain) IBOutlet UIView* contView;
@property (nonatomic, retain) IBOutlet UIView* tabView;
@property (nonatomic, retain) IBOutlet UIButton* homeBtn;
@property (nonatomic, retain) IBOutlet UIButton* settBtn;
@property (nonatomic, retain) IBOutlet UIButton* tablBtn;
@property (nonatomic, retain) IBOutlet UIButton* usBtn;
@property (nonatomic, retain) NSMutableArray *btnImgs;
@property (nonatomic, retain) NSMutableArray *btnImgsOV;

-(void)drawTab;
-(void)drawCont;
-(IBAction)btnClicked:(id)sender;

@end
