//
//  MainViewController.h
//  mutnav2
//
//  Created by walid nour on 4/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "prayertimecalculator.h"
#import "date.h"


@class RXMLElement;
@class Reachability;


@interface MainViewController : UIViewController

@property (nonatomic,retain)  NSArray *prayerTimes;
@property (nonatomic,retain)  NSArray *prayTimes;
@property (retain, nonatomic) IBOutlet UIImageView *backImage;
@property (retain, nonatomic) NSMutableArray *tawef;
@property (retain, nonatomic) NSMutableArray *saai;



-(NSMutableArray *) getTemp;


-(void) drawHeader;
-(void) drawTimePlayers;
-(void) drawFooter;

-(void) drawTemp;

-(void) drawCrowd;

-(int)  getPrayerID;






@end
