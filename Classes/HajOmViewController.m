//
//  HajOmViewController.m
//  mutnav2
//
//  Created by waleed on 7/16/13.
//
//

#import "HajOmViewController.h"
#import "OmraListViewController.h"
#import "HajViewController.h"

@interface HajOmViewController ()

@end

@implementation HajOmViewController
@synthesize bgImage,mainTable,mainarr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.frame=CGRectMake(0, 0, 320, 430*highf);
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,430*highf);
    
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 45*highf, 320, 350*highf) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=50.0*highf;
    mainTable.hidden = NO;
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"الحج والعمرة";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];
    
    
    mainarr=[[NSMutableArray alloc]initWithObjects:@"أحكام الحج",@"أحكام العمرة", nil ];
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 2;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
      cell.selectionStyle=UITableViewCellSelectionStyleNone;
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
        [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    [cell.contentView addSubview:bgView];
    [cell setBackgroundView:bgView];
    cell.backgroundColor = [UIColor clearColor];
    [bgView release];
    // Configure the cell...
	
	cell.textLabel.text=[mainarr objectAtIndex:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    cell.textLabel.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
    //[[tableView cellForRowAtIndexPath:indexPath] setBackgroundView:bgView];
    if (indexPath.row ==0) {
        HajViewController *detailViewController = [[HajViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        detailViewController.type=1;
        detailViewController.titleName=@"حج";
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
        
    }
    else if (indexPath.row==1){
        OmraListViewController *detailViewController = [[OmraListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];
        
    }

}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *currentSelectedIndexPath = [tableView indexPathForSelectedRow];
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (indexPath.row%2==0) {
        [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
        [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    if (currentSelectedIndexPath != nil)
    {
        [[tableView cellForRowAtIndexPath:currentSelectedIndexPath] setBackgroundView:bgView];
    }
    
    return indexPath;
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.frame.size.height)];
    if (cell.isSelected == YES)
    {
       //[bgView setImage:[UIImage imageNamed:@"i3.png"]];
        
        //[cell setBackgroundView:bgView];
        
    }
    else
    {
        
        if (indexPath.row%2==0) {
            [bgView setImage:[UIImage imageNamed:@"i1.png"]];
        }
        else{
            [bgView setImage:[UIImage imageNamed:@"i2.png"]];
        }
        [cell setBackgroundView:bgView];
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
}



@end
