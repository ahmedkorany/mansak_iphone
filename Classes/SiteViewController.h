//
//  SiteViewController.h
//  mutnav2
//
//  Created by Waleed Nour on 4/2/15.
//
//

#import <UIKit/UIKit.h>
#import "Global.h"
@interface SiteViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate> {
    
    UIWebView	*theWebView;
    NSString	*urlString;
    UIActivityIndicatorView  *whirl;
    
}

-(void) updateToolbar;
@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (nonatomic, retain) NSString *urlString;
@property (nonatomic, strong) NSString *title;
@property int type;
@end

