//
//  manasik.h
//  mutnav2
//
//  Created by rashad on 7/21/14.
//
//

#import <UIKit/UIKit.h>
#import "CMSCoinView.h"

@interface manasik : UIViewController<UIGestureRecognizerDelegate>{
    UIView *view1;
    UIView *view2;
    int check;
}
@property (strong, nonatomic) UILabel *title_lable;

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
- (IBAction)first_mansak_click:(id)sender;
@property (strong, nonatomic) IBOutlet CMSCoinView *adab_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *e7ram_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *tawaf_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *maqam_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *zmzm_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *sa3i_view;
@property (strong, nonatomic) IBOutlet CMSCoinView *ta7ll_view;

@property (strong, nonatomic) IBOutlet UIView *left_arrow;
@property (strong, nonatomic) IBOutlet UIView *down_arrow;
@property (strong, nonatomic) IBOutlet UIView *down_arrow_2;
@property (strong, nonatomic) IBOutlet UIView *up_arrow;
@property (strong, nonatomic) IBOutlet UIView *down_arrow_3;
@property (strong, nonatomic) IBOutlet UIView *up_arrow_2;

@property (strong, nonatomic) IBOutlet UIView *content_view_3in;
@property (strong, nonatomic) IBOutlet UIView *content_view_4in;

-(void)hide_header_view;
@end
