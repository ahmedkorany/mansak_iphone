//
//  CalendarBusiness.m
//  AlMosaly
//
//  Created by Sara Ali on 2/24/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalendarBusiness.h"
#import "HijriTime.h"
#import "date.h"

static NSInteger now=0;

@implementation CalendarBusiness


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
    }
    return self;
}

+ (NSMutableArray*) hegryCalendar:(NSInteger)offset TodayCellIndex:(int*)todayCellIndex;
{
    now = -1;
    NSMutableArray* container= [[NSMutableArray alloc] initWithCapacity:3];
    NSMutableArray* hegryCal  = [[NSMutableArray alloc] initWithCapacity:42];
    NSMutableArray* meladCal = [[NSMutableArray alloc] initWithCapacity:42];
    NSMutableArray* datesArray = [[NSMutableArray alloc] initWithCapacity:42];
    Date melady;
    
    melady = melady.addDays((int)offset * 30);
    NSMutableArray* hegryToday1 =[HijriTime toHegry:melady];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hegryToday1 objectAtIndex:i] intValue];
    
    NSInteger shift = -hegryToday[0] +1;
    Date firstDay = melady.addDays((int)shift);
    
    
    NSInteger dayOfWeek = firstDay.dayOfWeek();
    
    NSInteger hegryMonth = hegryToday[1];
    NSInteger currentCell = dayOfWeek;
    for(int a = 0 ; a < currentCell ; a++)
    {
        [hegryCal addObject:@""];
        [meladCal addObject:@""];
        Date temp = firstDay;
        temp = temp.addDays(-(int)(dayOfWeek - a));
        
        [datesArray addObject:temp.date];
    }
    
    Date currentDay = firstDay;
    
    while(  currentCell < 42)
    {
        NSMutableArray* hegryDate1 = [HijriTime toHegry:currentDay];
        int hegryDate[3];
        for(int i = 0 ; i < 3 ; i++)
            hegryDate[i] = [[hegryDate1 objectAtIndex:i] intValue];
        if(hegryDate[1] != hegryMonth)
            break;
        
        NSString* hegryDay = [NSString stringWithFormat:@"%d",hegryDate[0]];
        [hegryCal addObject:hegryDay];
        
        
        NSString* meladyDay = [NSString stringWithFormat:@"%d",currentDay.day()];
        [meladCal addObject:meladyDay];
        
        [datesArray addObject:currentDay.date];
        
        ////// Determine today
        Date nowDate;
        
        if(currentDay.isEqualDay(nowDate) )
            now = currentCell;
        
        
        currentCell++;
        currentDay=	currentDay.addDays(1);
    }
    
    [container addObject:hegryCal];
    [container addObject:meladCal];
    [container addObject:datesArray];
    
    if(todayCellIndex != nil)
        *todayCellIndex =(int)now;
    
    [container autorelease];
    [hegryCal autorelease];
    [meladCal autorelease];
    [datesArray autorelease];
    
    return container;
    
}


+ (NSMutableArray*) meladyCalendar:(NSInteger)offset TodayCellIndex:(int*)todayCellIndex
{
    now = -1;
    
    NSMutableArray* container= [[NSMutableArray alloc] initWithCapacity:3];
    NSMutableArray* hegryCal  = [[NSMutableArray alloc] initWithCapacity:35];
    NSMutableArray* meladCal = [[NSMutableArray alloc] initWithCapacity:35];
    NSMutableArray* datesArray = [[NSMutableArray alloc] initWithCapacity:35];
    Date melady;
    
    melady.setMonth((int)(melady.month() + offset));
    
    NSMutableArray* hegryToday1 =[HijriTime toHegry:melady];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hegryToday1 objectAtIndex:i] intValue];
    
    NSInteger shift = -melady.day() +1;
    Date firstDay = (melady.addDays((int)shift));
    
    
    NSInteger dayOfWeek = firstDay.dayOfWeek();
    NSInteger currentCell = dayOfWeek;
    for(int a = 0 ; a < currentCell ; a++)
    {
        [hegryCal addObject:@""];
        [meladCal addObject:@""];
        Date temp = firstDay;
        temp = temp.addDays(-(int)(dayOfWeek - a));
        
        [datesArray addObject:temp.date];
    }
    
    Date currentDay = firstDay;
    
    while(  currentCell < 35)
    {
        NSMutableArray* hegryDate1 = [HijriTime toHegry:currentDay];
        int hegryDate[3];
        for(int i = 0 ; i < 3 ; i++)
            hegryDate[i] = [[hegryDate1 objectAtIndex:i] intValue];
        
        
        NSString* hegryDay = [NSString stringWithFormat:@"%d",hegryDate[0]];
        [hegryCal addObject:hegryDay];
        
        
        NSString* meladyDay = [NSString stringWithFormat:@"%d",currentDay.day()];
        [meladCal addObject:meladyDay];
        
        [datesArray addObject:currentDay.date];
        
        ////// Determine today
        Date nowDate;
        
        if(currentDay.isEqualDay(nowDate) )
            now = currentCell;
        
        
        currentCell++;
        currentDay=	currentDay.addDays(1);
        
        
    }
    
    [container addObject:hegryCal];
    [container addObject:meladCal];
    [container addObject:datesArray];
    
    if(todayCellIndex != nil)
        *todayCellIndex = (int)now;
    
    [container autorelease];
    [hegryCal autorelease];
    [meladCal autorelease];
    [datesArray autorelease];
    
    return container;
    
}


+ (NSString*) getTitles:(NSInteger) increment{
    
    NSDate* melady = [NSDate date];
    melady= [melady dateByAddingTimeInterval:(increment * 29*24*60*60)];
    
    NSMutableArray* hegryToday1 = [HijriTime toHegry:melady];
    int hegryToday[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryToday[i] = [[hegryToday1 objectAtIndex:i] intValue];
    NSDate* firstDay = [melady dateByAddingTimeInterval:((-hegryToday[0] +1) *24*60*60)];//to get the later time
    NSMutableArray* hegryFirst1 = [HijriTime toHegry:firstDay];
    int hegryFirst[3];
    for(int i = 0 ; i < 3 ; i++)
        hegryFirst[i] = [[hegryFirst1 objectAtIndex:i]intValue];
    NSString* hegryTitle = NSLocalizedString([HijriTime getArabicMonth:(hegryFirst[1])], @"") ;
    hegryTitle=[NSString stringWithFormat:@"%@" "%i",hegryTitle,hegryFirst[2]];
    
    NSCalendar *calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *dateComponents = [calendar components:unitFlags fromDate:firstDay];
    NSInteger Month = [dateComponents month];
    NSInteger Year = [dateComponents year];
    NSString* meladyTitle=[NSString stringWithFormat:@"%ld" @"/" "%ld",(long)Month ,(long)Year];
    
    NSMutableArray *title = [[NSMutableArray alloc] init];
    [title insertObject:hegryTitle atIndex:0];
    [title insertObject:meladyTitle atIndex:1];
    
    NSString* Title = (NSString*)title;
    
    
    return Title;
}


- (void)dealloc {
    [super dealloc];
}


@end
