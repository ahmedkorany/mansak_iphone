//
//  secondmenue.h
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import"secondmenue.h"

#import"detailsviewcontroller.h"
#import"detailsintable.h"
#import"imageviewcontroller.h"
#import"electronicmutawef.h"
#import "mapViewController.h"
#import "PrayTimeViewController.h"
#import "InfoViewController.h"
#import "HomeListViewController.h"

@interface secondmenue : UITableViewController {
	//NSString *name;
	NSNumber *myid;
	NSString *plist;
	NSMutableArray *menuearr;
	NSMutableDictionary *sdic;
}
//@property (nonatomic, retain)NSString *name;
@property (nonatomic, retain)NSNumber *myid;
@property (nonatomic, retain)NSString *plist;
@property(nonatomic,retain)NSMutableArray *menuearr;
@property(nonatomic,retain)NSMutableDictionary *sdic;
@end
