//
//  Global_style.h
//  mutnav2
//
//  Created by rashad on 7/6/14.
//
//

#import <Foundation/Foundation.h>
@protocol SampleProtocolDelegate <NSObject>
@required
- (void) processCompleted;
@end
@interface Global_style : NSObject{
    NSMutableArray *array1;
    NSMutableArray *array2;
    NSMutableArray *array3;
    int index1;
    id <SampleProtocolDelegate> _delegate;
}
+(UIView *)header_view:(UIView *)your_header view_frame:(CGRect )super_view_frame;
+(UIView *)footer_view:(UIView *)your_footer page_no:(int )page_no;

-(void)global_animation:(int)index;
-(void)scale_animation:(UIView *)view_scaled;
-(void)move_animation:(UIView *)view_moved direction:(NSString *)dir;
-(void)setArray:(NSMutableArray *)array array2:(NSMutableArray*)array_2 array3:(NSMutableArray*)array_3;
+(void)custom_page:(UIPageControl *)pagecontrol;
+(UILabel *)global_text:(UILabel *)lbl_style;
@property (nonatomic,strong) id delegate;

-(void)startSampleProcess;
@end
