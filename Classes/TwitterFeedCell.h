//
//  TwitterFeedCell.h
//  mutnav2
//
//  Created by waleed on 5/28/13.
//
//

#import <UIKit/UIKit.h>
#import <Accounts/Accounts.h>
extern NSString * const TwitterFeedCellId;

@interface TwitterFeedCell : UITableViewCell


@property (strong, nonatomic)ACAccount *twitterAccount;

@end
