//
//  CalCellView.m
//  Almosaly
//
//  Created by Sara Ali on 2/19/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CalCellView.h"


@implementation CalCellView
@synthesize hjriLabel,meladyLabel,dayLabel,drawCellRect , delegate , highliteColor;
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self drawCell];
        self.backgroundColor =[UIColor clearColor];
        self.drawCellRect=YES;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
    meladyLabel.center = [self convertPoint:self.center fromView:self.superview];
    if (self.drawCellRect) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        // Drawing with a white stroke color
        CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.7);
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(context, 0.5);
        
        CGSize mySize = self.bounds.size;
        // Add Rect to the current path, then stroke it
        CGContextAddRect(context, CGRectMake(0, 0, mySize.width ,mySize.height));
        CGContextStrokePath(context);
    }
    
}

- (void)drawCell
{
    //meladyLabel
    CGRect meladyframe = CGRectMake(20, 20, 25, 15);
    
    UIFont* meladyFont= [UIFont fontWithName:@"Arial" size:14];
    meladyLabel = [[UILabel alloc] initWithFrame:meladyframe];
    meladyLabel.textAlignment = NSTextAlignmentCenter;
    meladyLabel.font =meladyFont;
    meladyLabel.text = @"31";
    meladyLabel.textColor = [UIColor blackColor];
    meladyLabel.backgroundColor = [UIColor clearColor];
    
    
    [self addSubview:meladyLabel];
    
    self.opaque = NO;
    
}


- (void) setMelady:(NSString*)melady andHijri:(NSString*)hijri
{
    hjriLabel.text = hijri;
    meladyLabel.text = melady;
}

-(void) setHighlited:(BOOL)h
{
    if (h) {
        meladyLabel.textColor = highliteColor ? highliteColor : [UIColor redColor];
        hjriLabel.textColor = highliteColor ? highliteColor : [UIColor redColor];
        
    }
    else
    {
        meladyLabel.textColor =  [UIColor blackColor];
        hjriLabel.textColor =  [UIColor blackColor];
    }
}

-(void) setDotted:(BOOL)d
{
    
    _dotted = d;
    
    if(_dotted)
    {
        UIImageView* dot = [[UIImageView alloc] initWithImage:[UIImage imageNamed:  @"calDot.png"]];
        CGRect labelRect = self.meladyLabel.bounds;
        labelRect.origin.x += labelRect.size.width-4;
        labelRect.size.width = 7;
        labelRect.size.height = 7;
        dot.frame = labelRect;
        self.meladyLabel.clipsToBounds = NO;
        [self.meladyLabel addSubview:dot];
        
    }
    
}

-(BOOL)dotted
{
    return _dotted;
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
}


@end
