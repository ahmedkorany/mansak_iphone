#include "prayertimecalculator.h"
#include <math.h>
#include "MyMath.h"
#include "date.h"

#import <sstream>


@implementation PrayersCaculator

-initWithDay:(NSInteger)day Month:(NSInteger)mnth Year:(NSInteger)year Lat:(double)lat Long:(double)lng Zone:(int)zone
      Mazhab:(NSInteger)mzhb Way:(NSInteger)way DLS:(NSInteger)dls
{
    self = [super init];
    if(self != nil)
    {
        calc = new PrayerTimeCalculator((int)day ,(int) mnth , (int)year , lat, lng , zone ,(int)mzhb , (int)way , (int)dls);
    }
    
    return self;
}

-(NSArray*) getPrayersStrings
{
    vector<string> prayersCpp = calc->calculateDailyPrayers_String();
    NSMutableArray* prayers = [NSMutableArray arrayWithCapacity:6];
    int size =(int) prayersCpp.size();
    
    for (int i = 0; i < size  ; i++) {
        NSString* prayer = [NSString stringWithCString:prayersCpp[i].data() encoding:NSUTF8StringEncoding];
        [prayers addObject:prayer];
    }
    return prayers;
}


-(NSArray*) getPrayersDoubles
{
    vector<double> prayersCpp = calc->calculateDailyPrayers();
    NSMutableArray* prayers = [NSMutableArray arrayWithCapacity:6];
    int size =(int) prayersCpp.size();
    for (int i = 0; i < size  ; i++) {
        NSNumber* prayer = [NSNumber numberWithDouble:prayersCpp[i]];
        [prayers addObject:prayer];
        
    }
    return prayers;
    
}

-(NSArray*) getWeeklyPrayers:(Date)date
{
    map<int , vector<string> > weekly = calc->calculateWeeklyPrayers(date);
    NSMutableArray* weekPrayers = [NSMutableArray arrayWithCapacity:7];
    
    for (int i = 0;  i < weekly.size() ;  i++) {
        
        vector<string> prayersCpp = weekly[i];
        NSMutableArray* prayers = [NSMutableArray arrayWithCapacity:6];
        int size =(int) prayersCpp.size();
        for (int y = 0; y < size  ; y++)
        {
            NSString* prayer = [NSString stringWithCString:prayersCpp[y].data() encoding:NSUTF8StringEncoding];
            [prayers addObject:prayer];
        }
        
        [weekPrayers addObject:prayers];
    }
    
    return weekPrayers;
}

-(double) getShrooqTime
{
    vector<double> prayersCpp = calc->calculateDailyPrayers();
    double sh = prayersCpp[1];
    return sh;
}


-(void)dealloc
{
    delete calc;
    [super dealloc];
}
@end


// this is the standard C++ implementation, go out
PrayerTimeCalculator::PrayerTimeCalculator()
{
}

PrayerTimeCalculator::PrayerTimeCalculator(int day, int mnth, int yer, double lt, double lg, double tzone, int mz, int way,int DLSaving)
:TimePortions(7)
{
    
    
    this->TimePortions[0] =   ( 5 / 24.0 );
    this->TimePortions[1] =  ( 6 / 24.0  );
    this->TimePortions[2] =  ( 12 / 24.0  );
    this->TimePortions[3] =  (  13 / 24.0 );
    this->TimePortions[4] =  ( 18 / 24.0  );
    this->TimePortions[5] =  ( 18 / 24.0  );
    this->TimePortions[6] =  ( 18 / 24.0  );
    
    this->Day = day;
    this->Month = mnth;
    this->Year = yer;
    this->Lat = lt;
    this->Lng = lg;
    this->Tzone = tzone;
    this->Mazhab =1- mz;
    this->Way = way;
    this->SumTimeZone=DLSaving;
    
    this->JDate = this->calculateJulianDate(this->Day, this->Month, this->Year);
    
    
    
}

double PrayerTimeCalculator::calculateJulianDate(int day, int month, int year)
{
    if (month <= 2) {
        year -= 1;
        month += 12;
    }
    int A = year / 100;
    int B = A / 4;
    int C = 2 - A + B;
    int Dd = day;
    int Ee = (int) (365.25 * (year + 4716));
    int F = (int) (30.7* (month + 1));
    
    double JD = C + Dd + Ee + F - 1524.5 -1 ;
    
    return JD;
}


double PrayerTimeCalculator::calculateEquationOfTime(double JD, double TimePortion)
{
    double d = JD - 2451545.0 + TimePortion;
    double g = MyMath::fixAngle(357.529 + 0.98560028 * d);
    double q = MyMath::fixAngle(280.459 + 0.98564736 * d);
    double L = q + 1.915 * (MyMath::dSin(g)) + 0.020 * (MyMath::dSin(2 * g));
    double e = 23.439 - 0.00000036 * d;
    double RA = MyMath::dATan2((MyMath::dCos(e)) * (MyMath::dSin(L)),
                               (MyMath::dCos(L))) / 15;
    RA = MyMath::fixHours(RA);
    double Eqt = q / 15 - RA;
    return Eqt;
}


double PrayerTimeCalculator::calculateSunDeclination(double JD, double TimePortion)
{
    
    double d = JD - 2451545.0 + TimePortion;
    double g = 357.529 + 0.98560028 * d;
    double q = 280.459 + 0.98564736 * d;
    double L = q + 1.915 * (MyMath::dSin(g)) + 0.020 * (MyMath::dSin(2 * g));
    double e = 23.439 - 0.00000036 * d;
    double Decl = MyMath::dASin((MyMath::dSin(e)) * (MyMath::dSin(L)));
    return Decl;
}


// calculate prayer times for one definite day
vector<double> PrayerTimeCalculator::calculateDailyPrayers()
{
    vector<double> Prayers = this->calculatePrayersOneIteration();
    this->setTimePortions(Prayers);
    Prayers = this->calculatePrayersOneIteration();
    Prayers = this->adjustPrayers(Prayers);
    return Prayers;
}


vector<double> PrayerTimeCalculator::calculatePrayersOneIteration()
{
    this->Dhuhr = this->calculateDhuhrTime();
    this->Sunrise = this->calculateSunrise();
    this->Sunset = this->calculateSunset();
    this->Asr = this->calculateAsrTime();
    this->Maghreb = this->Sunset;
    vector<double> FAI = this->calculateFajrAndIshaTimes();
    this->Fajr = FAI[0];
    this->Isha = FAI[1];
    vector<double> Prayers(6);
    Prayers[0] = this->Fajr;
    Prayers[1] = this->Sunrise;
    Prayers[2] = this->Dhuhr;
    Prayers[3] = this->Asr;
    Prayers[4] = this->Maghreb;
    Prayers[5] = this->Isha;
    return Prayers;
}

//Dhuhr time calculation
double PrayerTimeCalculator::calculateDhuhrTime()
{
    this->Eqt = this->calculateEquationOfTime(this->JDate, this->TimePortions[2]);
    double Dhuhr = MyMath::fixHours(12 - this->Eqt);
    return Dhuhr;
}

double PrayerTimeCalculator::calculateMidDay(double t)
{     //t is time portion
    this->Eqt = this->calculateEquationOfTime(this->JDate, t);
    double Z = MyMath::fixHours(12 - this->Eqt);
    return Z;
}

//Sunrise and Sunset
double PrayerTimeCalculator::calculateT(double a, double TimePortion)
{
    //time period between mid-day and the time at which sun reaches an angle below the horizon
    this->Dcl = this->calculateSunDeclination(this->JDate, TimePortion);
    double A = MyMath::dSin(a);
    double B = (MyMath::dSin(this->Lat)) * (MyMath::dSin(this->Dcl));
    double C = (MyMath::dCos(this->Lat)) * (MyMath::dCos(this->Dcl));
    double D = (MyMath::dACos((-A - B) / C));
    double T1 = D / 15;
    return T1;
}

//Sunrise
double PrayerTimeCalculator::calculateSunrise() {
    double SRise = this->calculateMidDay(this->TimePortions[1]) - this->calculateT(0.8333, this->TimePortions[1]);
    return SRise;
}

//Sunset & Maghreb prayer
double PrayerTimeCalculator::calculateSunset() {
    double SSet = this->calculateMidDay(this->TimePortions[5]) + this->calculateT(0.8333, this->TimePortions[4]);
    return SSet;
}


void PrayerTimeCalculator::setTimePortions(vector<double> Prayers)
{
    this->TimePortions[0] = Prayers[0] / 24.0;
    this->TimePortions[1] = Prayers[1] / 24.0;
    this->TimePortions[2] = Prayers[2] / 24.0;
    this->TimePortions[3] = Prayers[3] / 24.0;
    this->TimePortions[4] = Prayers[4] / 24.0;
    this->TimePortions[5] = Prayers[4] / 24.0;
    this->TimePortions[6] = Prayers[5] / 24.0;
}


vector<double> PrayerTimeCalculator::adjustPrayers(vector<double> prayers)
{
    for (int i = 0; i < prayers.size(); i++)
    {
        prayers[i] = prayers[i] + this->Tzone - this->Lng / 15.0;
        prayers[i] = (ceil(prayers[i]*60))/60.0;
        if (this->isSummerTZone() == 1)
            prayers[i]++;
    }
    return prayers;
}


//Asr Prayer
double PrayerTimeCalculator::calculateAsrTime()
{
    double D = this->calculateSunDeclination(this->JDate, this->TimePortions[3]);
    double G = -MyMath::dACot(this->Mazhab + 1 + MyMath::dTan(abs(this->Lat - D)));
    return this->calculateAofK(G, this->TimePortions[3]);
}

//Fajr and Isha
vector<double> PrayerTimeCalculator::calculateFajrAndIshaTimes()
{
    double fajr = 0;
    double isha = 0;
    vector<double> FAI(2) ;
    
    if (this->Lat <= 50)
    {
        switch (this->Way)
        {
            case 0: //Egyptian General Authority of Survey
                fajr = this->calculateMidDay(this->TimePortions[0]) - this->calculateT(19.5, this->TimePortions[0]);
                isha = this->calculateMidDay(this->TimePortions[6]) + this->calculateT(17.5, this->TimePortions[6]);
                break;
            case 1:  //Karachi
                fajr = this->calculateMidDay(this->TimePortions[0]) - this->calculateT(18, this->TimePortions[0]);
                isha = this->calculateMidDay(this->TimePortions[6]) + this->calculateT(18, this->TimePortions[6]);
                break;
            case 2: //Islamic Society of North America (ISNA)
                fajr = this->calculateMidDay(this->TimePortions[0]) - this->calculateT(15, this->TimePortions[0]);
                isha = this->calculateMidDay(this->TimePortions[6]) + this->calculateT(15, this->TimePortions[6]);
                break;
            case 3: //Umm al-Qura, Makkah
                fajr = this->calculateMidDay(this->TimePortions[0]) - this->calculateT(18.5, this->TimePortions[0]);
                isha = this->Maghreb + 1.5;
                break;
            case 4: //Muslim World League(MWL)
                fajr = this->calculateMidDay(this->TimePortions[0]) - this->calculateT(18, this->TimePortions[0]);
                isha = this->calculateMidDay(this->TimePortions[6]) + this->calculateT(17, this->TimePortions[6]);
                break;
        }
    }
    else      //high latitude. One-Seventh of the Night method is used
    {
        //period between sunset-sunrise
        double P = 24 - (this->Sunset - this->Sunrise);
        fajr = this->Sunrise - 2 * P / 7.0 - 1 / 6.0;
        isha = this->Sunset + 2 * P / 7.0 - 1 / 15.0;
        
    }
    FAI[0] = fajr;
    FAI[1] = isha;
    return FAI;
}


//Difference between mid-day and the time at which the object's shadow equals k times
double PrayerTimeCalculator::calculateAofK(double G, double t)
{ //t is time portion
    double D = this->calculateSunDeclination(this->JDate, t);
    double Z = this->calculateMidDay(t);
    double V = ((double)(1 / 15.0)) * MyMath::dACos((-MyMath::dSin(G) - MyMath::dSin(D) * MyMath::dSin(this->Lat)) /
                                                    (MyMath::dCos(D) * MyMath::dCos(this->Lat)));
    return Z + (G > 90 ? -V : V);
}

vector<string> PrayerTimeCalculator::calculateDailyPrayers_String()
{
    vector<double> DPrayers = this->calculateDailyPrayers();
    vector<string> SPrayers(6);
    for (int i = 0; i < DPrayers.size(); i++)
    {
        SPrayers[i] = this->adjustToString(DPrayers[i]);
        
    }
    
    
    
    return SPrayers;
}


string num(int number)
{
    stringstream ss;//create a stringstream
    ss << number;//add number to the stream
    return ss.str();//return a string with the contents of the strea
}

string PrayerTimeCalculator::adjustToString(double pt)
{
    int hrs = (int)(pt);
    int min =  MyMath::round((pt - hrs) * 60);
    int hrs_12 = ((int)(pt) > 12) ? (int)(pt - 12) : (int)(pt);
    
    string mins = num(min);
    if(min < 10)
        mins.insert(0 , "0" );
    
    string hours =  num(hrs_12 );
    if(hrs_12 < 10)
        hours.insert(0 , "0");
    
    return hours + ":" + mins;
}


// calculate prayer times for whole week
map<int , vector<string> > PrayerTimeCalculator::calculateWeeklyPrayers(Date SomeDay)
{
    
    int DWeek = SomeDay.dayOfWeek();
    
    map<int , vector<string> > WeekPrayers;
    
    int startDay = 0 - DWeek;
    
    SomeDay = SomeDay.addDays(startDay);
    
    for (int i = 0; i < 7; i++)
    {
        this->setDate(SomeDay);
        WeekPrayers[i] = calculateDailyPrayers_String();
        SomeDay = SomeDay.addDays(1);
    }
    return WeekPrayers;
}


void PrayerTimeCalculator::setDate(Date dt)
{
    this->Day = dt.day();
    this->Month  = dt.month();
    this->Year = dt.year();
    this->JDate = this->calculateJulianDate(this->Day, this->Month, this->Year);
}