//
//  secondmenue.m
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "secondmenue.h"


@implementation secondmenue
@synthesize myid,menuearr,plist,sdic;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	
	//name=[sdic objectForKey:@"name"];
	//myid=[sdic objectForKey:@"id"];
	myid=[sdic objectForKey:@"id"];
	//NSLog(@"%@",[sdic objectForKey:@"id"]);
	//NSLog(@"%@",myid);
	//NSLog(@"%@",plist);
	plist=[[NSString alloc] initWithFormat:@"%@%@",plist,myid];
	self.title=[sdic objectForKey:@"name"];
	//NSLog(plistname);
	NSString *mypath=[[NSBundle mainBundle] pathForResource:plist ofType:@"plist"];
	menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count]-1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
	cell.textLabel.text=[dic objectForKey:@"name"];
    cell.textLabel.textAlignment = UITextAlignmentRight;
	NSString *imgname=[[NSString alloc] initWithFormat:@"n%d.png",indexPath.row+1];
	UIImageView *imgview=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imgname]];
	
     cell.accessoryView=imgview;
	[imgview release];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
	NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
	NSMutableDictionary *lastdic=[menuearr objectAtIndex:[menuearr count]-1];
	int targetpage=[[dic objectForKey:@"targetpage"] intValue];
	//NSLog(@"%d",targetpage);
	//NSLog(@"%@",[lastdic objectForKey:@"plist"] );
	if(targetpage==0){
	secondmenue *detailViewController2 = [[secondmenue alloc] initWithNibName:@"secondmenue" bundle:nil];
	detailViewController2.sdic=dic;	
	
	//detailViewController2.title=[dic objectForKey:@"name"];
	//detailViewController2.name=[dic objectForKey:@"name"];
	detailViewController2.myid=[dic objectForKey:@"id"];
	detailViewController2.plist=[lastdic objectForKey:@"plist"];
        NSLog(@"%@", plist);
      
	[self.navigationController pushViewController:detailViewController2 animated:YES];
	//[self presentModalViewController:detailViewController2 animated:YES];
	
	[detailViewController2 release];
	}
	else if(targetpage==1 || targetpage==2)
	{
		detailsviewcontroller *detailViewController2 = [[detailsviewcontroller alloc] initWithNibName:@"detailsviewcontroller" bundle:nil];
		detailViewController2.sdic=dic;
		//detailViewController2.dic=dic;
		//detailViewController2.name=[dic objectForKey:@"name"];
		//detailViewController2.myid=[dic objectForKey:@"id"];
		//detailViewController2.plist=[lastdic objectForKey:@"plist"];
		detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		//[self presentModalViewController:detailViewController2 animated:YES];
		
		[detailViewController2 release];
	}
	else if(targetpage==3){
	
	/*	detailsintable *detailViewController2 = [[detailsintable alloc] initWithNibName:@"detailsintable" bundle:nil];
		detailViewController2.sdic=dic;
		detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];  */
        InfoViewController *detailViewController2 = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
		detailViewController2.sdic=dic;
		detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];
		
	}
	else if(targetpage==4)
	{
		imageviewcontroller *detailViewController2 = [[imageviewcontroller alloc] initWithNibName:@"imageviewcontroller" bundle:nil];
		detailViewController2.sdic=dic;
		detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];
	}
	else if(targetpage==5)
	{
		electronicmutawef *detailViewController2 = [[electronicmutawef alloc] initWithNibName:@"electronicmutawef" bundle:nil];
		detailViewController2.from=[[dic objectForKey:@"id"] stringValue];
		//detailViewController2.sdic=dic;
		//detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];
	}
    else if(targetpage==8)
	{
		mapViewController *detailViewController2 = [[mapViewController alloc] initWithNibName:@"mapViewController" bundle:nil];
		//detailViewController2.from=[[dic objectForKey:@"id"] stringValue];
		detailViewController2.sdic=dic;
		//detailViewController2.plist=[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];
	}
    else if(targetpage==9)
	{
		HomeListViewController *detailViewController2 = [[HomeListViewController alloc] initWithNibName:@"HomeListViewController" bundle:nil];
		detailViewController2.grade=[[dic objectForKey:@"id"] stringValue];
        detailViewController2.type =[lastdic objectForKey:@"plist"];
		[self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];
        NSLog(@"%@", [lastdic objectForKey:@"plist"]);
        NSLog(@"%@", [dic objectForKey:@"id"]);
        
    
	}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
	//[sdic release];
	[plist release];
	[menuearr release];
	[myid release];
	[sdic release];
	//[name release];
    [super dealloc];
}


@end

