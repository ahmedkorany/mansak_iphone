//
//  CrowdViewController.m
//  mutnav2
//
//  Created by walid nour on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CrowdViewController.h"
#import "Reachability.h"
@implementation CrowdViewController


@synthesize bgImage, mainTable;
@synthesize info, infolabels, tawefLabels, tawef , saaea;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    info = [[NSMutableArray alloc] init];
    tawef = [[NSMutableArray alloc] init];
    saaea = [[NSMutableArray alloc] init];
    infolabels = [[NSMutableArray alloc] initWithObjects:@"الأرضي",@"الأول",@"الثاني",@"الثالث",@"الرابع",nil];
    tawefLabels = [[NSMutableArray alloc] initWithObjects:@"الأرضي", @"الأول", @"السطح",nil];
    // Do any additional setup after loading the view from its nib.
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,420);
    
    
    mainTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 30, 320, 390) style:UITableViewStyleGrouped]autorelease];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 0.7;
    mainTable.rowHeight=40.0;
    mainTable.hidden = NO;
    // mainTable.sectionHeaderHeight = 20.0;
    mainTable.allowsSelection = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
    [self getbuildingDetails];
    
    //self.title = @"حالة المناسك";
    
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont boldSystemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor greenColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = @"حالة المناسك";
    [titleView sizeToFit];
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{       [super viewWillAppear:animated];
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
      //  UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,50,30)];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(0, 0, 50, 30);
        [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        UIBarButtonItem *barBackItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
        self.navigationItem.hidesBackButton = TRUE;
        self.navigationItem.leftBarButtonItem = barBackItem;
        [barBackItem release];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count =0;
    if (section==0) {
        count =3;
    }
    else if(section==1)
    {
        count =5;
        
    }
    else if(section==2)
    {
        count =5;
    }
    return count;
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
          if(indexPath.section==0)
    {
        
        cell.textLabel.textAlignment   =  UITextAlignmentLeft;
  
      //  cell.textLabel.text = [info objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [tawefLabels objectAtIndex:indexPath.row];
        
    }
    else if (indexPath.section == 1)
    {
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        
        cell.textLabel.text = [saaea objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [infolabels objectAtIndex:indexPath.row];

    }
    else if (indexPath.section ==2)
    {
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        
        cell.textLabel.text = [info objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = [infolabels objectAtIndex:indexPath.row];

    }
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
    
}

- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}
-(void) getbuildingDetails{
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    //  [curReach set
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {

    NSURL *url0= [NSURL URLWithString:@"http://hinewz.com/testlink.aspx?pid=0&lid=2"];
    NSString *content0 = [NSString stringWithContentsOfURL:url0 encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@", content0);
    NSArray *allLines0 = [content0 componentsSeparatedByString: @";"];
    for (int x=0; x <[allLines0 count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines0 objectAtIndex:x]];
        NSArray *Lines = [line componentsSeparatedByString: @","];
        [tawef addObject:[Lines objectAtIndex:2]];
        
    }
    
    NSURL *url1 = [NSURL URLWithString:@"http://hinewz.com/testlink.aspx?pid=1&lid=2"];
    NSString *content1 = [NSString stringWithContentsOfURL:url1 encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@", content1);
    NSArray *allLines1 = [content1 componentsSeparatedByString: @";"];
    for (int x=0; x <[allLines1 count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines1 objectAtIndex:x]];
        NSArray *Lines = [line componentsSeparatedByString: @","];
        [saaea addObject:[Lines objectAtIndex:2]];
        
    }

    NSURL *url = [NSURL URLWithString:@"http://hinewz.com/testlink.aspx?pid=2&lid=2"];
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"%@", content);
    NSArray *allLines = [content componentsSeparatedByString: @";"];
    for (int x=0; x <[allLines count]-1; x++) {
        NSString *line = [NSString stringWithFormat:@"%@",[allLines objectAtIndex:x]];
        NSArray *Lines = [line componentsSeparatedByString: @","];
        [info addObject:[Lines objectAtIndex:2]];
        
    }
    }
    else
    {
        for (int i=0; i<3; i++) {
            [tawef addObject:@"--"];
            
        }
        for (int i=0; i<5; i++) {
            [saaea addObject:@"--"];
            [info addObject:@"--"];
            }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 30.0)];
	
	// create the button object
	UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
	headerLabel.backgroundColor = [UIColor clearColor];
	headerLabel.opaque = NO;
	headerLabel.textColor = [UIColor blackColor];
	headerLabel.highlightedTextColor = [UIColor whiteColor];
	headerLabel.font = [UIFont boldSystemFontOfSize:20];
    //	headerLabel.frame = CGRectMake(10.0, 0.0, 300.0, 44.0);
    
	// If you want to align the header text as centered
	headerLabel.frame = CGRectMake(100.0, 0.0, 300.0, 30.0);
    
    
	if (section==0) {
        headerLabel.text =@"حالة الطواف";
    }
    else if(section==1)
    {
        headerLabel.text = @"حالة السعى";
    }
    else if(section==2)
    {
        headerLabel.text = @"حالة الجمرات";
    }
    
	[customView addSubview:headerLabel];
    [headerLabel release];
	return customView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
	return 30.0;
    
}


- (void)dealloc
{
    [info release];
    [infolabels release];
    [super dealloc];
    
}

@end
