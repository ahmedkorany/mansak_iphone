//
//  HamaltViewController.m
//  mutnav2
//
//  Created by waleed on 3/17/13.
//
//

#import "HamaltViewController.h"
#import "Reachability.h"
#import "RXMLElement.h"

@interface HamaltViewController ()

@end

@implementation HamaltViewController

@synthesize bgImage, mainTable;
@synthesize mainarr, filearr ,iddarr;
@synthesize townID, listType, buildID;
@synthesize titleName ,fileName;
@synthesize searchID;
@synthesize searchTxt;
@synthesize searchBtn;
@synthesize searcharr,buildarr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    mainarr = [[NSMutableArray alloc] init];
    iddarr  = [[NSMutableArray alloc] init];
    filearr = [[NSMutableArray alloc] init];
    
    [self fillArr];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"عودة" style:UIBarButtonItemStyleBordered                        target:nil  action:nil];
    self.navigationItem.backBarButtonItem = backButton;
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"zeft_back.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,370);
    [self.view addSubview:bgImage];
    
    townName = @"السعودية";
    buildName = @"الرياض";
    typeName = @"السعر";
   
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    NSLog(@"%@",townName);
    CGRect rect;
    if (listType==-1) {
        rect = CGRectMake(0, 0, 320, 200);
        CGRect rect2 = CGRectMake(80, 260, 160, 40);
        searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [searchBtn setImage:[UIImage imageNamed:@"searchBtn.png"] forState:UIControlStateNormal];
        
        searchBtn.frame = rect2;
        searchBtn.alpha = 1;
        searchBtn.titleLabel.text = @"ابحث";
        [searchBtn setContentMode:UIViewContentModeCenter];
        [searchBtn addTarget:self action:@selector(searchPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:searchBtn];
        
    }
    else
    {
        rect = CGRectMake(0, 0.0, 320, 370);
    }
    
    mainTable = [[UITableView alloc] initWithFrame:rect style:UITableViewStyleGrouped];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=45.0;
    mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
    mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    // [mainTable setBackgroundView:bgImage];
    
    [self.view addSubview:mainTable];
    self.title = titleName;
    //[self.navigationController setNavigationBarHidden:YES animated:animated];
    
}
/*- (void) viewWillAppear:(BOOL)animated
 {
 [self.navigationController setNavigationBarHidden:YES animated:animated];
 [super viewWillAppear:animated];
 }
 
 - (void) viewWillDisappear:(BOOL)animated
 {
 [self.navigationController setNavigationBarHidden:NO animated:animated];
 [super viewWillDisappear:animated];
 }*/
- (void)viewDidUnload
{
    [self setMainarr:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [mainarr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
        
    }
    if (listType==-1) {
        if(indexPath.row==0)
        {
            
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.textAlignment = NSTextAlignmentRight;
            cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
            cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
            cell.textLabel.numberOfLines = 0;
            cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            cell.detailTextLabel.text = townName;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.lineBreakMode=UILineBreakModeWordWrap;
            cell.detailTextLabel.numberOfLines=0;
            // cell.backgroundColor = [UIColor clearColor];
        }
        else if(indexPath.row==1)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.textAlignment = NSTextAlignmentRight;
            cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
            cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
            cell.textLabel.numberOfLines = 0;
            cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            cell.detailTextLabel.text = buildName;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.lineBreakMode=UILineBreakModeWordWrap;
            cell.detailTextLabel.numberOfLines=0;
            // cell.backgroundColor = [UIColor clearColor];
        }
        else if(indexPath.row==2)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.textAlignment = NSTextAlignmentRight;
            cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
            cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
            cell.textLabel.numberOfLines = 0;
            cell.detailTextLabel.textAlignment = NSTextAlignmentLeft;
            cell.detailTextLabel.text = typeName;
            cell.detailTextLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.lineBreakMode=UILineBreakModeWordWrap;
            cell.detailTextLabel.numberOfLines=0;
            // cell.backgroundColor = [UIColor clearColor];
        }
    }
    else
    {
        // Configure the cell...
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text = [mainarr objectAtIndex:indexPath.row];
        cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.textColor = [UIColor blackColor];
        // cell.backgroundColor = [UIColor clearColor];
    }
    return cell;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (listType==-1) {
        if (indexPath.row!=0) {
            SearchListController *detailViewController = [[SearchListController alloc] initWithNibName:@"RootViewController" bundle:nil];
            // ...
            // Pass the selected object to the new view controller.
            
            detailViewController.listType=indexPath.row;
            if (indexPath.row==1) {
                detailViewController.checkID = townID;
            }
            else if (indexPath.row==2) {
                detailViewController.checkID =buildID ;
            }
            else if (indexPath.row==3) {
                detailViewController.checkID =searchID ;
            }
            detailViewController.titleName = [mainarr objectAtIndex:indexPath.row];
            detailViewController.sv = self;
            [self.navigationController pushViewController:detailViewController animated:YES];
          
        }
    }
   
}

- (void) fillArr
{
    if (listType==-1) {
        mainarr = [[NSMutableArray alloc] initWithObjects:@"اختر الدولة",@"اختر المدينة",@"اختر عرض محدد", nil];
        
        filearr = [[NSMutableArray alloc] initWithObjects:@"السعودية", nil];
        buildarr = [[NSMutableArray alloc] initWithObjects:@"الرياض", nil];
        searcharr = [[NSMutableArray alloc] initWithObjects:@"السعر", nil];
        
        
    }
   /* else if (listType==4)
    {
        filearr = [[NSMutableArray alloc] initWithObjects: @"hotel",@"apart",@"park",@"rest",@"parks",@"museum",@"retail",@"sport",@"hostel",@"exhib",@"hospital",@"trans",@"travel", nil];
        NSString *filePath = [[NSString alloc] initWithFormat:@"%@",[filearr objectAtIndex:buildID]];
        NSString *pathRoot = [[NSString alloc] initWithFormat:@"%i%@",townID,filePath];
        fileName = pathRoot;
        int indexCol;
        if (searchID==0) {
            if (townID==0||townID==1) {
                indexCol=3;
            }
            else
            {
                indexCol=2;
            }
        }
        else if(searchID==1)
        {
            if (townID==0||townID==1) {
                indexCol=4;
            }
            else
            {
                indexCol=3;
            }
        }
        NSLog(@"%@", fileName);
        NSString *path=[[NSBundle mainBundle] pathForResource:pathRoot ofType:@"dat"];
        NSError *error;
        NSString* content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        NSArray *allLines = [content componentsSeparatedByString: @"\n"];
        int count = [allLines count]-1;
        for (int i=0; i<count; i++) {
            NSString *line = [allLines objectAtIndex:i];
            NSArray *items = [line componentsSeparatedByString: @"|"];
            NSString *grad =  [items objectAtIndex:indexCol];
            if ([searchTxt isEqualToString:grad] ) {
                if (townID==0||townID==1) {
                    [self.mainarr addObject:[items objectAtIndex:3]];
                }
                else
                {
                    [self.mainarr addObject:[items objectAtIndex:2]];
                }
                [self.iddarr addObject:[items objectAtIndex:0]];
            }
        }
        
        if(mainarr.count==0)
        {
            [mainarr addObject:@"لا توجد بيانات"];
        }
        
    }*/
}

-(IBAction)searchPressed:(id)sender{
    NSMutableArray * temps = [[NSMutableArray alloc] init ];
    NSMutableArray * cities = [[NSMutableArray alloc] initWithObjects:@"1939897",@"1937801", nil];
    
    Reachability *curReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    if (netStatus !=NotReachable) {
        for (int i=0; i<2; i++) {
            NSString *tmpUrl = [NSString stringWithFormat:@"http://weather.yahooapis.com/forecastrss?w=%@&u=c", [cities objectAtIndex:i]];
            
            RXMLElement *parser = [RXMLElement elementFromURL:[NSURL URLWithString:tmpUrl]];
            [parser iterateWithRootXPath:@"//item" usingBlock: ^(RXMLElement *item) {
                NSLog(@"Player #5: %@", [item child:@"description"]);
                NSString *txt  = [NSString stringWithFormat:@"%@",[item child:@"description"]];
                NSArray *tmp = [txt componentsSeparatedByString:@" "];
                NSString *currentTemp;
                if ([[tmp objectAtIndex:5]length ]>2) {
                    currentTemp = [tmp objectAtIndex:6];
                }
                else {
                    currentTemp = [tmp objectAtIndex:5];
                }
                // NSLog(@"====%@",currentTemp);
                if (currentTemp==NULL) {
                    currentTemp = @"";
                }
                NSLog(@"%@",currentTemp);
                [temps addObject:currentTemp];
                
            }];
            }
    }
    else
    {
        for (int i=0; i<2; i++) {
            [temps addObject:@"0"];
        }
    }
    
    //[parser release];
    
    
    
    
    
  /*  SearchViewController *detailViewController = [[SearchViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    
    detailViewController.listType=4;
    detailViewController.titleName = @"نتيجة البحث";
    detailViewController.townID = self.townID;
    detailViewController.buildID = self.buildID;
    detailViewController.searchID = self.searchID;
    detailViewController.searchTxt = self.searchTxtField.text;
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];*/
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    searchTxt = textField.text;
    
	[textField resignFirstResponder];
    return YES;
}





@end
