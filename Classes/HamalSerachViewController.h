//
//  HamalSerachViewController.h
//  mutnav2
//
//  Created by waleed on 6/2/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "HamalListViewController.h"
@interface HamalSerachViewController : UIViewController <NIDropDownDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UIButton *stateBtn;
@property (retain, nonatomic) IBOutlet UIButton *townBtn;
@property (retain, nonatomic) IBOutlet UIButton *offerBtn;
@property (retain, nonatomic) IBOutlet UIButton *searchBtn;

@property (retain, nonatomic)  NIDropDown *dropDown;
@property int type;



- (IBAction)selectClicked:(id)sender;
- (IBAction)goToPage:(id)sender;

-(void)rel;

@end
