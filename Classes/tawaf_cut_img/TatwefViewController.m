//
//  TatwefViewController.m
//  mutnav2
//
//  Created by Shaima Magdi on 12/1/14.
//
//

#import "TatwefViewController.h"
#import "Global.h"
#import "Global_style.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@interface TatwefViewController (){
    /**
     *  @brief int counterFlag it count number of clicks counterBtn.
     */
    int counterFlag;
    
    /**
     *  @brief bool used to determine that the view is tawaf or sa3i to choose the suitable text.
     */
    BOOL isTawafView;
    
    /**
     *  @brief bool used to determine that the user pressed manasik button or do3aa button
     */
    BOOL isMnasikBtn;
    
    /**
     *  @brief bool used to determine the shown view is textView or imageView , to swipe between them.
     */
    BOOL isTextView;
    
    /**
     *  @brief float is using in counter drawing.
     */
    float angle;
}

@end

@implementation TatwefViewController
@synthesize tawaf_sa3i_view;

- (void)viewDidLoad {
    [super viewDidLoad];
    //set default view tawafView so isTextView =no .
    isTawafView=YES;
    isTextView=NO;
    
    // set default button is manasik.
    isMnasikBtn=YES;
    
    // defult value to angel that will use in counterView.
    angle=0;
    
    //defult value(start) to counter in counterView.
    counterFlag = 0;
    
    self.view.backgroundColor=[UIColor whiteColor];
    screen_size=[[UIScreen mainScreen] bounds];
    self.view.clipsToBounds=YES;
    self.view.frame=screen_size;
    
    // draw headerView from "Global_style" and set title.
    header_view=[[UIView alloc] init];
    header_view=[Global_style header_view:header_view view_frame:self.view.frame];
    view_title=[[UILabel alloc] initWithFrame:CGRectMake((header_view.frame.size.width-50)/2, 30, 60, 60)];
    view_title.text=NSLocalizedString(@"tawaf",@"test");
    view_title.textColor=[UIColor colorWithRed:0.396 green:0.396 blue:0.388 alpha:1];
    
    // draw sa3i button
    sa3i=[[UIButton alloc] init];
    [sa3i setTitle:NSLocalizedString(@"sa3i",@"test") forState:UIControlStateNormal];
    sa3i.titleLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:14];
    sa3i.frame=CGRectMake((header_view.frame.size.width/2)-((header_view.frame.size.width/2)-70),header_view.frame.size.height-25, (header_view.frame.size.width/2)-70, 25);
    [sa3i setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [sa3i addTarget:self action:@selector(sa3i_click:) forControlEvents:UIControlEventTouchUpInside];
    
    // draw splitter
    UILabel *splitter=[[UILabel alloc] initWithFrame:CGRectMake(sa3i.frame.size.width+sa3i.frame.origin.x+1, sa3i.frame.origin.y+5, 1, sa3i.frame.size.height-8)];
    splitter.backgroundColor=[UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
    
    // draw tawaaf button
    tawaaf=[[UIButton alloc] init];
    [tawaaf setTitle:NSLocalizedString(@"tawaf",@"test")forState:UIControlStateNormal];
    tawaaf.titleLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:14];
    tawaaf.frame=CGRectMake(sa3i.frame.size.width+sa3i.frame.origin.x+3,sa3i.frame.origin.y, 80, sa3i.frame.size.height);
    [tawaaf setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [tawaaf addTarget:self action:@selector(tawaaf_click:) forControlEvents:UIControlEventTouchUpInside];
    
    // call "draw_footer_view" to draw footerView
    [self draw_footer_view];
    
    [header_view addSubview:tawaf_sa3i_image];
    [header_view addSubview:sa3i];
    [header_view addSubview:tawaaf];
    [header_view addSubview:splitter];
    [header_view addSubview:view_title];
    
    // draw centerView that hold centerImage and 2 button (arrows)
    center_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    center_view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    int Actual_center_size=self.view.frame.size.height-(header_view.frame.size.height+footer_view.frame.size.height);
    
    // draw left&right buttons -->arrows
    right_btn=[[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-35,((Actual_center_size/2)+header_view.frame.size.height)-(150/2),35, 150)];
    [right_btn setBackgroundImage:[UIImage imageNamed:@"right arrow.png"] forState:UIControlStateNormal];
    right_btn.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [right_btn addTarget:self action:@selector(swiperight:) forControlEvents:UIControlEventTouchUpInside];
    
    left_btn=[[UIButton alloc] initWithFrame:CGRectMake(0,right_btn.frame.origin.y,35, 150)];
    [left_btn setBackgroundImage:[UIImage imageNamed:@"left arrow.png"] forState:UIControlStateNormal];
    left_btn.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [left_btn addTarget:self action:@selector(swipeleft:) forControlEvents:UIControlEventTouchUpInside];
    
    // set Center image for tawaf and sa3i and swipe between them in run time
    center_image_tawaf=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, center_view.frame.size.width, center_view.frame.size.height)];
    center_image_tawaf.image=[UIImage imageNamed:@"tawaf-5.png"];
    center_image_tawaf.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [center_view addSubview:center_image_tawaf];
    
    center_image_sa3i=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, center_view.frame.size.width, center_view.frame.size.height)];
    center_image_sa3i.image=[UIImage imageNamed:@"sa3y-4s.png"];
    center_image_sa3i.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    
    // add swipe gesture to centerView
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [center_view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [center_view addGestureRecognizer:swiperight];
    
    // draw text area view (scrollView , textLabel)
    scroll_text_view=[[UIScrollView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height, self.view.frame.size.width,20)];
    scroll_text_view.autoresizingMask=UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    scroll_text_view.backgroundColor=[UIColor colorWithRed:255 green:255 blue:255 alpha:0.96];
    text_label=[[UILabel alloc] initWithFrame:CGRectMake(5,header_view.frame.size.height+header_view.frame.origin.y+10, scroll_text_view.frame.size.width-15, scroll_text_view.frame.size.height)];
    text_label.lineBreakMode = NSLineBreakByWordWrapping;
    text_label.numberOfLines = 0;
    [Global_style global_text:text_label];
    text_label.font=[UIFont fontWithName:@"Arial" size:text_size];
    text_label.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    text_label.textAlignment=NSTextAlignmentCenter;
    [scroll_text_view setContentSize:CGSizeMake(scroll_text_view.frame.size.width, text_label.frame.size.height+footer_view.frame.size.height)];
    [scroll_text_view addSubview:text_label];
    

    [self.view addSubview:center_view];
    [self.view addSubview:right_btn];
    [self.view addSubview:left_btn];
    [self.view addSubview:scroll_text_view];
    [self.view addSubview:header_view];
    [self.view addSubview:footer_view];
    [self.view addSubview:counter_view];
    [self.view addSubview:increase_font];
    [self.view addSubview:decrease_font];
    
    // draw sizeBtn view that hold "A" button when you pressed it, 2 button will appear .
    size_button_view=[[UIView alloc] initWithFrame:CGRectMake(5, footer_view.frame.origin.y+23, 40, 40)];
    size_button_view.layer.cornerRadius=20;
    size_button_view.backgroundColor=[UIColor colorWithRed:0.89 green:0.584 blue:0.004 alpha:1];
    
    // draw "A" button when you click on it , you show "A+" & "A-" and this buton will hold "X" instead of "A"
    font_button=[[UIButton alloc] initWithFrame:size_button_view.frame];
    font_button.backgroundColor=[UIColor whiteColor];
    font_button.layer.cornerRadius=20;
    [font_button setTitleColor:[UIColor colorWithRed:0.329 green:0.329 blue:0.329 alpha:1] forState:UIControlStateNormal];
    [font_button setTitle:@"A" forState:UIControlStateNormal];
    [font_button addTarget:self action:@selector(font_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    // draw "A+"
    increase_font=[[UIButton alloc] initWithFrame:CGRectMake(2.5, 2.5, 35, 35)];
    increase_font.layer.cornerRadius=17.5;
    increase_font.backgroundColor=[UIColor whiteColor];
    increase_font.titleLabel.font=[UIFont systemFontOfSize:14];
    [increase_font setTitle:@"A+" forState:UIControlStateNormal];
    [increase_font setTitleColor:[UIColor colorWithRed:0.329 green:0.329 blue:0.329 alpha:1] forState:UIControlStateNormal];
    [increase_font addTarget:self action:@selector(increase_font_size:) forControlEvents:UIControlEventTouchUpInside];
    
    // draw "A-"
    decrease_font=[[UIButton alloc] initWithFrame:CGRectMake(increase_font.frame.origin.x, increase_font.frame.origin.y, 35, 35)];
    decrease_font.layer.cornerRadius=17.5;
    decrease_font.titleLabel.font=[UIFont systemFontOfSize:14];
    decrease_font.backgroundColor=[UIColor whiteColor];
    [decrease_font setTitle:@"A-" forState:UIControlStateNormal];
    [decrease_font setTitleColor:[UIColor colorWithRed:0.329 green:0.329 blue:0.329 alpha:1] forState:UIControlStateNormal];
    [decrease_font addTarget:self action:@selector(decrease_font_size:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:size_button_view];
    [size_button_view addSubview:increase_font];
    [size_button_view addSubview:decrease_font];
    [self.view addSubview:font_button];
    
    // add value(0 as default) of counterFlag to counterLabel in counterView
    count_label.text=[NSString stringWithFormat:@"%d",counterFlag];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Tatwef Screen/Iphone "];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewWillAppear:(BOOL)animated{
   
    // get textSize from userDefault and set the value to textView
    NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"size_text"];
    text_size=[savedValue intValue];
    text_label.font=[UIFont fontWithName:@"Arial" size:text_size];
    [text_label sizeToFit];
    text_label.frame=CGRectMake(text_label.frame.origin.x, text_label.frame.origin.y, scroll_text_view.frame.size.width-10, text_label.frame.size.height);
}
/**
 *  this method used to draw footerView with counterView and 3 buttons "manasik" & "do3aa" & "minus"
 */
-(void)draw_footer_view{
    
    // draw footer view
    footer_view= [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-150, self.view.frame.size.width, 150)];
    
    // draw footer background image
    footer_back_img=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, footer_view.frame.size.width, footer_view.frame.size.height)];
    footer_back_img.image=[UIImage imageNamed:@"footer_view.png"];
    
    // draw minus button
    minus_btn=[[UIButton alloc] initWithFrame:CGRectMake(footer_view.frame.size.width-43,23, 40, 40)];
    [minus_btn setBackgroundImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
    [minus_btn addTarget:self action:@selector(minus_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    // draw manasik button
    manasik=[[UIButton alloc] initWithFrame:CGRectMake(footer_view.frame.size.width-90,minus_btn.frame.origin.y+minus_btn.frame.size.height+3, 90, 40)];
    [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
    [manasik addTarget:self action:@selector(manasik_click:) forControlEvents:UIControlEventTouchUpInside];
    manasikLabel=[[UILabel alloc]initWithFrame:CGRectMake(manasik.frame.origin.x+40, manasik.frame.origin.y-5,50, manasik.frame.size.height-1)];
    manasikLabel.text=NSLocalizedString(@"manasik", @"manasik label");
    manasikLabel.adjustsFontSizeToFitWidth = YES;
    manasikLabel.textColor=[UIColor blackColor];
    
    // draw do3aa button
    do3aa=[[UIButton alloc] initWithFrame:CGRectMake(0,minus_btn.frame.origin.y+minus_btn.frame.size.height+3, 90, 40)];
    [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
    [do3aa addTarget:self action:@selector(do3aa_click:) forControlEvents:UIControlEventTouchUpInside];
    do3aaLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, do3aa.frame.origin.y-1,50, do3aa.frame.size.height-1)];
    do3aaLabel.text=NSLocalizedString(@"doaa", @"do3aa label");
    do3aaLabel.adjustsFontSizeToFitWidth = YES;
    do3aaLabel.textColor=[UIColor blackColor];
    
    // add subviews
    [footer_view addSubview:footer_back_img];
    [footer_view addSubview:minus_btn];
    [footer_view addSubview:manasik];
    [footer_view addSubview:manasikLabel];
    [footer_view addSubview:do3aa];
    [footer_view addSubview:do3aaLabel];
    
    
    // draw counter view
    counter_view=[[UIView alloc] initWithFrame:CGRectMake((footer_view.frame.size.width-160)/2, footer_view.frame.size.height+footer_view.frame.origin.y-180,160, 160)];
    counter_view.autoresizingMask=UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    counter_view.layer.cornerRadius=counter_view.frame.size.width/2;
    
    // counter background image
    counter_back_img=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, counter_view.frame.size.width, counter_view.frame.size.height)];
    counter_back_img.image=[UIImage imageNamed:@"counter-4s_04-1.png"];
    
    counter_btn= [[UIButton alloc] initWithFrame:CGRectMake(0, 0,counter_view.frame.size.width, counter_view.frame.size.width)];
    counter_btn.autoresizingMask=UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    [counter_btn addTarget:self action:@selector(counter_btn_click:) forControlEvents:UIControlEventTouchUpInside];
    
    // draw counterLabel
    count_label=[[UILabel alloc] initWithFrame:CGRectMake((counter_btn.frame.size.width-50)/2, (counter_btn.frame.size.height-50)/2, 50, 50)];
    count_label.textAlignment=NSTextAlignmentCenter;
    count_label.textColor=[UIColor colorWithRed:0.929 green:0.624 blue:0.035 alpha:1];
    count_label.font=[UIFont fontWithName:@"Arial" size:60];
    circle_bg=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    circle_bg.image=[UIImage imageNamed:@"circle.png"];
    circle_bg.center=counter_btn.center;
    
    // draw counter arrow.
    UIImageView *arrow=[[UIImageView alloc] initWithFrame:CGRectMake((circle_bg.frame.size.width-15)/2, 0, 15, 15)];
    arrow.image=[UIImage imageNamed:@"small arrow.png"];
    
    // counter view add subviews
    [counter_btn addSubview:circle_bg];
    [circle_bg addSubview:arrow];
    [counter_btn addSubview:count_label];
    [counter_view addSubview:counter_back_img];
    [counter_view addSubview:counter_btn];
    
    // call "tawaaf_click" to display tawaf view as defult.
    [self tawaaf_click:tawaaf];
}

/**
 *  This method made to handel tawafBtn when you click "tawafBtn" it check if the view is "sa3iView" it display "tawafView" and disable "sa3iView".
 *
 *  @param sender tawafBtn (under headerView).
 */
-(IBAction)tawaaf_click:(id)sender{
    if (!isTawafView) {
        //change flag to yes as tawafView will show now.
        isTawafView=YES;
        // change color and image of buttons & title to TAWAF
        [tawaaf setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [sa3i setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:1 delay:0 options:0 animations: ^{
            if (isTawafView) {
                scroll_text_view.transform = CGAffineTransformIdentity;
                scroll_text_view.frame = CGRectMake(0, self.view.frame.size.height, scroll_text_view.frame.size.width, 0);
            }
        } completion: ^(BOOL completed) {
            if (completed) {
                [self change_view_animation_tawaf];
            }
        }];
        view_title.text=NSLocalizedString(@"tawaf",@"test");
    }
}

/**
 *  This method made to handel sa3iBtn when you click "sa3iBtn" it check if the view is "tawafView" it display "sa3iView" and disable "tawafView".
 *
 *  @param sender sa3iView (under headerView).
 */
-(IBAction)sa3i_click:(id)sender{
    if (isTawafView) {
        //change flag to yes as sa3iView will show now.
        isTawafView=NO;
        // change color and image of buttons & title to SA3I
        [sa3i setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [tawaaf setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
        [UIView animateWithDuration:1 delay:0 options:0 animations: ^{
            if (!isTawafView) {
                scroll_text_view.transform = CGAffineTransformIdentity;
                scroll_text_view.frame = CGRectMake(0, self.view.frame.size.height, scroll_text_view.frame.size.width, 0);
            }
        } completion: ^(BOOL completed) {
            if (completed) {
                [self change_view_animation_sa3i];
            }
        }];
        view_title.text=NSLocalizedString(@"sa3i",@"test");
    }
}

/**
 *  this method made to handel when you press "do3aaBtn" it check the shown view is textView or centerImageView(tawaf || sa3ai View) and it swipe between them.
 *
 *  @param sender do3aaBtn
 */
-(IBAction)do3aa_click:(id)sender{
    if (!isTextView) {
        //  change flag to no as the pressedBtn now do3aaBtn not manasikBtn.
        isMnasikBtn=NO;
        // add text to textView according the pressedButton and counterFlag
        [self addTextToView];
        //  change image of buttons & show the textView animated
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a-off.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.8];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        scroll_text_view.transform = CGAffineTransformIdentity;
        scroll_text_view.frame = CGRectMake(0, 0, scroll_text_view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        //  change flag to Yes as the showen view now is "textView".
        isTextView=YES;
    }else{
        // this when the "isTextView" is =Yes .. it hide the textView animated and show centerImage.
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.8];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        scroll_text_view.transform = CGAffineTransformIdentity;
        scroll_text_view.frame = CGRectMake(0,self.view.frame.size.height, self.view.frame.size.width,20);
        [UIView commitAnimations];
        //  change flag to NO as the showen view now is "centerImageView".
        isTextView=NO;
    }
}

/**
 *   this method made to handel when you press "manasikBtn" it check the shown view is textView or centerImageView(tawaf || sa3ai View) and it swipe between them.
 *
 *  @param sender manasikBtn
 */
-(IBAction)manasik_click:(id)sender{
    if (!isTextView) {
        //  change flag to no as the pressedBtn now manasikBtn not do3aaBtn.
        isMnasikBtn=YES;
        // add text to textView according the pressedButton and counterFlag
        [self addTextToView];
        // change image of buttons & show the textView animated
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik-off.png"] forState:UIControlStateNormal];
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.8];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        scroll_text_view.transform = CGAffineTransformIdentity;
        scroll_text_view.frame = CGRectMake(0, 0, scroll_text_view.frame.size.width, self.view.frame.size.height);
        [UIView commitAnimations];
        //  change flag to Yes as the showen view now is "textView".
        isTextView=YES;
    }else{
         // this when the "isTextView" is =Yes .. it hide the textView animated and show centerImage.
        [manasik setBackgroundImage:[UIImage imageNamed:@"manasik.png"] forState:UIControlStateNormal];
        [do3aa setBackgroundImage:[UIImage imageNamed:@"do3a.png"] forState:UIControlStateNormal];
        isTextView=NO;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.8];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        scroll_text_view.transform = CGAffineTransformIdentity;
        scroll_text_view.frame = CGRectMake(0,self.view.frame.size.height, self.view.frame.size.width,20);
        [UIView commitAnimations];
        //  change flag to NO as the showen view now is "centerImageView".
        isTextView=NO;
    }
}

/**
 *  This method used to resetCounter .. it remove angelImages and reset counert to ZERO.
 *  @warning this method not used now.
 */
-(void)reset_counter{
    // loop on the counterView to remove angelView
    for (int i=0;i<counterFlag;i++) {
        circle_bg.center=CGPointMake(counter_btn.center.x, counter_btn.center.y);
        circle_bg.transform=CGAffineTransformMakeRotation ((angle-51.42857142857143)*M_PI/180);
        [[counter_view viewWithTag:6] removeFromSuperview];
        angle-=51.42857142857143;
        [self addTextToView];
    }
    // set counterFlag to zero and add this value to counterLabel
    counterFlag=0;
    count_label.text=[NSString stringWithFormat:@"%d",counterFlag];
}

/**
 *  This method made to handel "minusBtn" "-" .. it remove angelImage form counterView and change value of counterFlag & call "addTextToView" to change text according counter ;
 *  @param sender minusBtn
 */
-(IBAction)minus_btn_click:(id)sender{
    if (counterFlag!=0) {
        //remove angelImage and decrease angel Value
        circle_bg.center=CGPointMake(counter_btn.center.x, counter_btn.center.y);
        circle_bg.transform=CGAffineTransformMakeRotation ((angle-51.42857142857143)*M_PI/180);
        [[counter_view viewWithTag:6] removeFromSuperview];
        angle-=51.42857142857143;
        // decrease counerFlag Value and change Value of counterLbel & call "addTextToView";
        counterFlag--;
        count_label.text=[NSString stringWithFormat:@"%d",counterFlag];
        [self addTextToView];
    }
}

/**
 *  This method made to handel "counterBtn"  .. it add and remove angelImage of counterView and change value of counter according counterFlag & call "addTextToView" to change text according counter ;
 * by default this method increase value and add image angel expect when the counterflag=7.
 *  @param sender counterBtn
 */
-(IBAction)counter_btn_click:(id)sender{
    if (counterFlag==7) {
        //if counterFlag =7 it loop on counter and reset the counter
        for (UIImageView * image in [counter_view subviews]) {
            if (image.tag==6) {
                [image removeFromSuperview];
            }
        }
        counterFlag=0;
        angle=0;
    }else{
        // add image angel
        circle_bg.center=CGPointMake(counter_btn.center.x, counter_btn.center.y);
        circle_bg.transform=CGAffineTransformMakeRotation ((angle+51.42857142857143)*M_PI/180);
        UIImageView *image=[[UIImageView alloc] initWithFrame:CGRectMake(counter_view.frame.size.width/2-32, 40, 62, 80)];
        image.tag=6;
        image.image=[UIImage imageNamed:@"counter-4s_04.png"];
        image.layer.anchorPoint=CGPointMake(0, 1);
        image.transform=CGAffineTransformMakeRotation (angle*M_PI/180);
        [counter_view addSubview:image];
        [counter_view sendSubviewToBack:image];
        [counter_view sendSubviewToBack:counter_back_img];
        // increase angel value & counterFlag
        angle+=51.42857142857143;
        counterFlag++;
        
    }
    // call "addTextToView" and change value of counterlabel.
    [self addTextToView];
    count_label.text=[NSString stringWithFormat:@"%d",counterFlag];
}

/**
 *  This Method made to handel "A-" button .. it decrease value of text size by ONE every click and set size of text by the new value & save it to userDefault to use it later.
 *
 *  @param sender decreaseBtn "A-"
 */
-(void)decrease_font_size:(id)sender{
    // decrease size by 1 & set the new size to text & adapt scrollText to changes
    text_size-=1;
    text_label.font=[UIFont fontWithName:@"Arial" size:text_size];
    [text_label sizeToFit];
    text_label.frame=CGRectMake(text_label.frame.origin.x, text_label.frame.origin.y, scroll_text_view.frame.size.width-10, text_label.frame.size.height);
    [scroll_text_view setContentSize:CGSizeMake(scroll_text_view.frame.size.width, text_label.frame.size.height+footer_view.frame.size.height+header_view.frame.size.height+30)];
    
    // save new value to userDefault.
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",text_size] forKey:@"size_text"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 *  This Method made to handel "A+" button .. it increase value of text size by ONE every click and set size of text by the new value & save it to userDefault to use it later.
 *
 *  @param sender increaseBtn "A+"
 */
-(void)increase_font_size:(id)sender{
    // increase size by 1 & set the new size to text & adapt scrollText to changes
    text_size+=1;
    text_label.font=[UIFont fontWithName:@"Arial" size:text_size];
    [text_label sizeToFit];
    text_label.frame=CGRectMake(text_label.frame.origin.x, text_label.frame.origin.y, scroll_text_view.frame.size.width-10, text_label.frame.size.height);
    [scroll_text_view setContentSize:CGSizeMake(scroll_text_view.frame.size.width, text_label.frame.size.height+footer_view.frame.size.height+header_view.frame.size.height+30)];
    
    // save new value to userDefault.
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",text_size] forKey:@"size_text"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/**
 *   This Method made to handel "A" button .. it check the shown view is 1 btn (A) or 3 buttons(X,A-,A+) and transition between those view animated.
 *
 *  @param sender fontBtn "A"
 */
-(void)font_btn_click:(id)sender{
    // it check the current view according the hight of view .. if the height =117 then the 3 buttons are shown and it transition to 1 btn values and change the title to "A" instead of "x".
    [UIView animateWithDuration:0.3 delay:0 options:0 animations: ^{
        if (size_button_view.frame.size.height==117.5) {
            size_button_view.frame = CGRectMake(size_button_view.frame.origin.x, size_button_view.frame.origin.y+77.5,size_button_view.frame.size.width, size_button_view.frame.size.height-77.5);
            decrease_font.frame=CGRectMake(increase_font.frame.origin.x, decrease_font.frame.origin.y-37.5, 35, 35);
            
            [font_button setTitle:@"A" forState:UIControlStateNormal];
            font_button.transform = CGAffineTransformRotate(font_button.transform, -3.141593);
            
        }else{
            //if the height less than 117 that mean that one btn is shown "A" and it transition to 3 btns values and change the title to "X" instead of "A" to use to close this view later.
            size_button_view.frame = CGRectMake(size_button_view.frame.origin.x, size_button_view.frame.origin.y-77.5,size_button_view.frame.size.width, size_button_view.frame.size.height+77.5);
            decrease_font.frame=CGRectMake(increase_font.frame.origin.x, decrease_font.frame.origin.y+37.5, 35, 35);
            
            [font_button setTitle:@"X" forState:UIControlStateNormal];
            font_button.transform = CGAffineTransformRotate(font_button.transform, +3.141593);
            
        }
        
    } completion: ^(BOOL completed) {
    }];
}

/**
 *  This method used when pressed sa3i button .. it made to show center image of sa3i view animated.
 */
-(void)change_view_animation_sa3i{
    // chane the value of flag as the centerImage will shown now .
    isTextView=NO;
    // add sa3i image to centerView and send it to back.
    [center_view addSubview:center_image_sa3i];
    center_image_sa3i.frame = CGRectMake(0, center_image_sa3i.frame.origin.y, center_image_sa3i.frame.size.width, center_image_sa3i.frame.size.height);
    [center_view sendSubviewToBack:center_image_sa3i];
    //change the value of centerImage to make the image show animated and remove tawaf image .
    [UIView animateWithDuration:0.5 delay:0 options:0 animations: ^{
        center_image_tawaf.transform = CGAffineTransformIdentity;
        center_image_tawaf.frame = CGRectMake(self.view.frame.size.width, center_image_tawaf.frame.origin.y, center_image_tawaf.frame.size.width, center_image_tawaf.frame.size.height);
    } completion: ^(BOOL completed) {
        [center_image_tawaf removeFromSuperview];
    }];
}

/**
 *  This method used when pressed tawaf button .. it made to show center image of tawaf view animated.
 */
-(void)change_view_animation_tawaf{
    // chane the value of flag as the centerImage will shown now .
    isTextView=NO;
    // add tawaf image to centerView and send it to back.
    [center_view addSubview:center_image_tawaf];
    center_image_tawaf.frame = CGRectMake(0, center_image_tawaf.frame.origin.y, center_image_tawaf.frame.size.width, center_image_tawaf.frame.size.height);
    [center_view sendSubviewToBack:center_image_tawaf];
    //change the value of centerImage to make the image show animated and remove sa3i image .
    [UIView animateWithDuration:0.5 delay:0 options:0 animations: ^{
        center_image_sa3i.transform = CGAffineTransformIdentity;
        center_image_sa3i.frame = CGRectMake(-self.view.frame.size.width, center_image_sa3i.frame.origin.y, center_image_sa3i.frame.size.width, center_image_sa3i.frame.size.height);
    } completion: ^(BOOL completed) {
        if (completed) {
            [center_image_sa3i removeFromSuperview];
        }
    }];
}

/**
 *  This method made to hande swipe left .. and call "tawaaf_click" to handel its action .
 *
 *  @param sender swipeLeftGesture.
 */
-(void)swipeleft:(id)sender{
    [self tawaaf_click:tawaaf];
}

/**
 *  This method made to hande swipe right .. and call "sa3i_click" to handel its action
 *
 *  @param sender swipeRightGesture
 */
-(void)swiperight:(id)sender{
    [self sa3i_click:sa3i];
}

/**
 *  This method made to add suitable TEXT to TextView .. it work according to counterFlag and "isMnasikBtn" bool to transition between manasik texts and doaa texs.
 */
-(void)addTextToView{
    NSString *text;
    // check flag is it "manasik or doaa" and assign value of var "text" according counter value.
    if (isMnasikBtn) {
        if (isTawafView) {
            if (counterFlag==0) {
                text=@"shar7TawafStepByStep";
            }else if (counterFlag==1){
                text=@"shar7TawafFisrt";
            }else{
                text=@"shar7TawafScond";
            }
        }else{
            if (counterFlag==0) {
                text = @"shar7Sa3iStepByStep";
            }
            else if(counterFlag==1||counterFlag==3||counterFlag==5||counterFlag==7){
                text = @"shar7Sa3iSafa";
            }
            else if(counterFlag==2||counterFlag==4||counterFlag==6){
                text = @"shar7Sa3iMarwa";
            }
        }
    }else{
        // if the target button was "doaa" it get random text.
        NSInteger randomint=arc4random()%14+1;
        text=[NSString stringWithFormat:@"doaa%ld",(long)randomint];
    }
    // if counter =0 and it isn't doaaa it show 2 texts to the user .. the first hold the value of manask .. the second hold NOTE msg to user (to start press the counter).
    if (counterFlag ==0 && ![text hasPrefix:@"doaa"]) {
        NSString *str =NSLocalizedString(text, @"test");
        NSString *str2 = NSLocalizedString(@"start",@"test");
        
        // make attributed string to change color of some of text.
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ \n%@", str,str2] ];
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blackColor]
                                 range:NSMakeRange(0, [str length])];
        
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor redColor]
                                 range:NSMakeRange([str length]+2, [str2 length])];
        // add new  attributedString to textView.
        [text_label setAttributedText:attributedString];
    }else{
        // add new text to textView.
        text_label.text=NSLocalizedString(text, @"");
    }
    // adapt scrollText view to new text.
    [text_label sizeToFit];
    text_label.frame=CGRectMake(text_label.frame.origin.x, text_label.frame.origin.y, scroll_text_view.frame.size.width-10, text_label.frame.size.height);
    [scroll_text_view setContentSize:CGSizeMake(scroll_text_view.frame.size.width, text_label.frame.size.height+footer_view.frame.size.height+header_view.frame.size.height+header_view.frame.origin.y+40)];
    [scroll_text_view setContentOffset:CGPointMake(0, 0) animated:YES];
}


@end
