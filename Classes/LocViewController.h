//
//  LocViewController.h
//  mutnav2
//
//  Created by walid nour on 2/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h> 
#import <CoreLocation/CoreLocation.h>
#import "MapViewAnnotation.h"
#import "MyCLController.h"


@interface LocViewController : UIViewController
<MKMapViewDelegate , CLLocationManagerDelegate, MyCLControllerDelegate>{
    int type1;
}


@property (retain, nonatomic) IBOutlet MKMapView *myMapView;
@property (nonatomic, retain) CLLocationManager *locationManager;  
@property (nonatomic, retain) MyCLController *locationController;
-(void)displayMap;
-(void)setAnnotations;
- (void) getUserLoc;


- (void)locationUpdate:(CLLocation *)location; 
- (void)locationError:(NSError *)error;


@end
