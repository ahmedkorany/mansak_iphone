//
//  HajOmViewController.h
//  mutnav2
//
//  Created by waleed on 7/16/13.
//
//

#import "oldImg.h"
#import <UIKit/UIKit.h>


@interface HajOmViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;
@property (nonatomic, retain) NSMutableArray *mainarr;

@end
