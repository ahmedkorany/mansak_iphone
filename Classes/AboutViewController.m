//
//  AboutViewController.m
//  itour
//
//  Created by walid nour on 6/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import "AboutViewController.h"
#import "AboutDetailViewController.h"

@implementation AboutViewController

@synthesize bgImage ,scrollView;

//@synthesize adsView;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    for (UIView *v in self.view.subviews ) {
        [v removeFromSuperview];
    }
  
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgAbout.png"]];
    bgImage.frame = CGRectMake(0, 0, 320, 420);
    [self.view addSubview:bgImage];
    
 //   UIImageView *footView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footerAbout.png"]]autorelease];
       UIImageView *footView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footerAbout.png"]];
    footView.frame = CGRectMake(0, 375, 320, 40);
    [self.view addSubview:footView];
    
  //  UIImageView *txtImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboTxt.png"]]autorelease];
      UIImageView *txtImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboTxt.png"]];
    txtImage.frame = CGRectMake(10, 0, 300, 130);
    [self.view addSubview:txtImage];
    
  //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
      UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(120, 100, 80, 72);
    btn1.tag =0;
    [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setImage:[UIImage imageNamed:@"a1.png"] forState:UIControlStateNormal];
    [self.view addSubview:btn1];
    //[btn1 release];
    int i=0;
    for (int y=0; y<2; y++) {
        for (int x=0; x<3; x++) {
           // NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"a2.png",@"a3.png",@"a4.png",@"a5.png",@"a6.png",@"a7.png", nil]autorelease];
             NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a2.png",@"a3.png",@"a4.png",@"a5.png",@"a6.png",@"a7.png", nil];
         //   UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
               UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
            btn1.frame = CGRectMake(40+(x*80), 172+(y*72), 80, 72);
            btn1.tag =i+1;
            [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:i]] forState:UIControlStateNormal];
            [self.view addSubview:btn1];
            //   [btn1 release];
            i++;
        }
    }
    
    for (int x=0; x<4; x++) {
      //  NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"a8.png",@"a9.png",@"a10.png",@"a11.png", nil]autorelease];
        
          NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a8.png",@"a9.png",@"a10.png",@"a11.png", nil];
      //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
          UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame = CGRectMake(16+(x*72), 316, 72, 50);
        btn1.tag =x+7;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        //  [btn1 release];
    }
    
    for (int x=0; x<7; x++) {
     //   NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"b1.png",@"b2.png",@"b3.png",@"b4.png",@"b5",@"b6",@"b7", nil]autorelease];
           NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"b1.png",@"b2.png",@"b3.png",@"b4.png",@"b5",@"b6",@"b7", nil];
 //       UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
        UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];

        btn1.frame = CGRectMake(30+(x*38), 380, 38, 30);
        btn1.tag =x+11;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        //  [btn1 release];
    }
    
    // UIButton *more =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
    UIButton *more =[UIButton buttonWithType:UIButtonTypeCustom];
    more.frame = CGRectMake(10, 110, 60, 30);
    more.tag = 20;
    [more addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [more setBackgroundImage:[UIImage imageNamed:@"moreBtn.png"] forState:UIControlStateNormal];
    [self.view addSubview:more];
    
  /*  if (flag==0) {
        adsView = [[AdsViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        adsView.adsID = 2;
        [adsView getURLS];
        if (adsView.statusID==0) {
            [self.view addSubview:adsView.view];
            [self performSelector: @selector(stopAds) withObject: nil afterDelay:10];
        }
        flag=1;
    }*/

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)stopAds{
  //  flag=0;
  // [adsView.view removeFromSuperview];
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
   
}
- (void) viewWillDisappear:(BOOL)animated
{
   // [self.navigationController setNavigationBarHidden:NO animated:YES];
    [super viewWillDisappear:animated];
}

-(void)updataView{
  
   for (UIView *v in self.view.subviews ) {
        [v removeFromSuperview];
   //    [v release];
    }
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgAbout.png"]];
    bgImage.frame = CGRectMake(0, 0, 320, 420);
    [self.view addSubview:bgImage];
    
    
 //   UIScrollView *scrolView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)]autorelease];
       UIScrollView *scrolView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 420)];
    scrolView.backgroundColor = [UIColor clearColor];
    scrolView.delegate =self;
     [self.view addSubview:scrolView];
    
   // UIImageView *txtImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboText.png"]]autorelease];
    UIImageView *txtImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboText.png"]];
    txtImage.frame = CGRectMake(10, 0, 300, 210);
    //[self.scrollView addSubview:txtImage];
    [scrolView addSubview:txtImage];
     

 //   UIView *subview = [[[UIView alloc] initWithFrame:CGRectMake(5, 7, 290, 185)]autorelease];
       UIView *subview = [[UIView alloc] initWithFrame:CGRectMake(5, 7, 290, 185)];
    subview.backgroundColor =[UIColor clearColor];
        
   // UIWebView *textWeb = [[[UIWebView alloc] initWithFrame:subview.frame]autorelease];
     UIWebView *textWeb = [[UIWebView alloc] initWithFrame:subview.frame];
    textWeb.delegate= self;
    textWeb.autoresizingMask = YES;
    NSString *path = [[NSBundle mainBundle] bundlePath];
    NSURL *baseURL = [NSURL fileURLWithPath:path];
    
    NSString *kalma = @"<h1>كلمة رئيس مجلس الإدارة</h1>"
    "<img src='00001.png' width=70 height=72 />"
    "<h1>الرفيق للبيت العتيق</h1>"
    "<p> بسم الله الرحمن الرحيم \n،والحمد لله رب العالمين ،ونصلي على المبعوث رحمة للعالمين.\n"
    "فبعد أن صدر مشروع 'المطوف' بنسخة الجافا ثلاث سنوات متوالية ، واستفاد منه أكثر من مليون حاج ومعتمر ، هانحن الآن ولله الحمد ننتهي من إصدار مشروع 'المطوف' في تطبيقات جديدة ( آيفون – إندرويد – بلاك بيري )  ليكتمل  عقد لآلئ هذا المشروع . </p>"
    " <p>ولقد ابتدأت فكرة المشروع بدراسة واقع الحج والعمرة بالنسبة لزوار بيت الله الحرام ومايحصل في ذلك من جهد ومشقة وخطأ وعمل كثير على غير سنة النبي صلى الله عليه وسلم ،وانتهت بنتائج وتوصيات يسر الله بفضله وكرمه أن تم تطويعها جميعها فخرجت في صورة مشروع 'المطوف'، والذي هو في حقيقته عدة برامج في برنامج واحد فهو يحتوي المناسك الشرعية والخرائط الإرشادية والسكن والخدمات والجهات الرسمية والنصائح الطبية ، كما أنه مشروع متكامل لكل حملات الحج والعمرة وفوق هو برنامج لحظي يعطيك الحرم بطوافه وسعيه مباشرة .</p> "
    "<p>ولقد جعلنا الكتاب بسبعة عشر لغة وهي أهم اللغات التي يتكلمها حجاج بيت الله الحرام .</p>"
    "<p>وماتم الآن هو جزء بسيط من فكرة ضخمة في برمجتها وعظيمة في أثرها ، ونسأل الله أن يكتب التوفيق في رسمها وأن يعين على إكمالها .</p>"
    "<p>وهذا المشروع هو مشروع مبارك ، يمكن لكل مسلم مقتدر أن يقدم من خلاله الرعاية والدعاية التي تقيمه وتعينه ، وأجره عند الله عظيم .</p>"
   " <table border=1 cellpadding=5>"
   " <tr> <td> <label >رئيس مجلس الإدارة : صويان بن شايع الهاجري</label><br/><label >sowayyan@madarsoft.com</label></td></tr>"
    "<tr><td>"
    "<label >مؤسسة مدار البرمجة التجارية</label><br/>"
  "  <label >المملكة العربية السعودية - الدمام – شارع الملك فهد </label><br/>"
   " <label >هاتف : +966 38433739</label><br/>"
   " <label >فاكس : +966 38437751</label><br/>"
   " <label >جوال  : +966 556885373</label><br/>"
   " <label >البريد الإلكتروني لفريق العلاقات العامة : mm@madarsoft.com</label>"
  " </td> </tr></table>";
    
    NSString *cssCode = @" \n"
    "body{ font-family:Times New Roman,Times,serif;font-size:14px;line-height:17px;color:#000;direction:rtl;padding: 0px;margin: 0px;background-color:#fff;width:80%;margin-left:auto;margin-right:auto;text-align:justify;max-width:300px;} \n"
    "h1 {color:#8b29ef;font-size:20px;text-align:justify} \n"
    "p {margin:0px;	padding: 5px 20px;font-size: 20px;line-height: 25px;} \n"
    "table{border : 2px solid #000000;width:80%;margin: 0px auto;    } \n";
    
    NSString *htmlCode = [NSString stringWithFormat:@""
                          "<html> "
                          "     <head> "
                          "     <title></title> "
                          "     <style>%@</style></head>" //CSS
                          "     <body> %@ </body>\n\n "
                          "</html>  \n",cssCode,[kalma stringByReplacingOccurrencesOfString:@"\n" withString:@"<br />"]];
    
    
    textWeb.opaque = NO;
    textWeb.backgroundColor = [UIColor clearColor];
    [textWeb loadHTMLString:htmlCode baseURL:baseURL];
    //[subview addSubview:textWeb];
    //[self.scrollView addSubview:subview];
    [scrolView addSubview:textWeb];
    
 //   UIButton *more =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
       UIButton *more =[UIButton buttonWithType:UIButtonTypeCustom];
    more.frame = CGRectMake(10, 190, 60, 30);
    [more addTarget:self action:@selector(firstView ) forControlEvents:UIControlEventTouchUpInside];
    [more setImage:[UIImage imageNamed:@"closeBtn.png"] forState:UIControlStateNormal];
    [more setTitle:@"المزيد" forState:UIControlStateNormal];
    [more setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  //  [self.scrollView addSubview:more];
      [scrolView addSubview:more];
  //  [more release];
      
  //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
      UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(120, 180, 80, 72);
    btn1.tag =0;
    [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setImage:[UIImage imageNamed:@"a1.png"] forState:UIControlStateNormal];
   // [self.scrollView addSubview:btn1];
    [scrolView addSubview:btn1];
 //   [btn1 release];
    
    int i=0;
    for (int y=0; y<2; y++) {
        for (int x=0; x<3; x++) {
            NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a2.png",@"a3.png",@"a4.png",@"a5.png",@"a6.png",@"a7.png", nil];
            
         //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
           UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];

            btn1.frame = CGRectMake(40+(x*80), 252+(y*72), 80, 72);
            btn1.tag =i+1;
            [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:i]] forState:UIControlStateNormal];
           // [self.scrollView addSubview:btn1];
            // [btn1 release];
            [scrolView addSubview:btn1];
            i++;
        }
        
    }
    
    for (int x=0; x<4; x++) {
        NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a8.png",@"a9.png",@"a10.png",@"a11.png", nil];
      //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
         UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame = CGRectMake(16+(x*72), 396, 72, 50);
        btn1.tag =x+7;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
       // [self.scrollView addSubview:btn1];
        [scrolView addSubview:btn1]; 
       // [btn1 release];
    }
    
    //scrollView.contentSize = CGSizeMake(320,500);
    scrolView.contentSize = CGSizeMake(320,500);
    //[self.view addSubview:scrollView];
   // [self.view addSubview:scrolView];

    
    UIImageView *footView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footerAbout.png"]];
    footView.frame = CGRectMake(0, 375, 320, 40);
    [self.view addSubview:footView];
   // [footView release];
    for (int x=0; x<7; x++) {
        NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"b1.png",@"b2.png",@"b3.png",@"b4.png",@"b5",@"b6",@"b7", nil];
       UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
      //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
        btn1.frame = CGRectMake(30+(x*38), 380, 38, 30);
        btn1.tag =x+11;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        //  [btn1 release];
    }
}

-(void)firstView{
    for (UIView *v in self.view.subviews ) {
        [v removeFromSuperview];
        //    [v release];
    }
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bgAbout.png"]];
    bgImage.frame = CGRectMake(0, 0, 320, 420);
    [self.view addSubview:bgImage];
    
    //   UIImageView *footView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footerAbout.png"]]autorelease];
    UIImageView *footView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"footerAbout.png"]];
    footView.frame = CGRectMake(0, 375, 320, 40);
    [self.view addSubview:footView];
    
    //  UIImageView *txtImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboTxt.png"]]autorelease];
    UIImageView *txtImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"aboTxt.png"]];
    txtImage.frame = CGRectMake(10, 0, 300, 130);
    [self.view addSubview:txtImage];
    
    //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
    UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
    btn1.frame = CGRectMake(120, 100, 80, 72);
    btn1.tag =0;
    [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [btn1 setImage:[UIImage imageNamed:@"a1.png"] forState:UIControlStateNormal];
    [self.view addSubview:btn1];
    //[btn1 release];
    int i=0;
    for (int y=0; y<2; y++) {
        for (int x=0; x<3; x++) {
            // NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"a2.png",@"a3.png",@"a4.png",@"a5.png",@"a6.png",@"a7.png", nil]autorelease];
            NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a2.png",@"a3.png",@"a4.png",@"a5.png",@"a6.png",@"a7.png", nil];
            //   UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
            UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
            btn1.frame = CGRectMake(40+(x*80), 172+(y*72), 80, 72);
            btn1.tag =i+1;
            [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:i]] forState:UIControlStateNormal];
            [self.view addSubview:btn1];
            //   [btn1 release];
            i++;
        }
    }
    
    for (int x=0; x<4; x++) {
        //  NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"a8.png",@"a9.png",@"a10.png",@"a11.png", nil]autorelease];
        
        NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"a8.png",@"a9.png",@"a10.png",@"a11.png", nil];
        //  UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
        UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
        btn1.frame = CGRectMake(16+(x*72), 316, 72, 50);
        btn1.tag =x+7;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        //  [btn1 release];
    }
    
    for (int x=0; x<7; x++) {
        //   NSMutableArray *imgPath = [[[NSMutableArray alloc]initWithObjects:@"b1.png",@"b2.png",@"b3.png",@"b4.png",@"b5",@"b6",@"b7", nil]autorelease];
        NSMutableArray *imgPath = [[NSMutableArray alloc]initWithObjects:@"b1.png",@"b2.png",@"b3.png",@"b4.png",@"b5",@"b6",@"b7", nil];
        //       UIButton *btn1 =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
        UIButton *btn1 =[UIButton buttonWithType:UIButtonTypeCustom];
        
        btn1.frame = CGRectMake(30+(x*38), 380, 38, 30);
        btn1.tag =x+11;
        [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn1 setImage:[UIImage imageNamed:[imgPath objectAtIndex:x]] forState:UIControlStateNormal];
        [self.view addSubview:btn1];
        //  [btn1 release];
    }
    
    // UIButton *more =[[UIButton buttonWithType:UIButtonTypeCustom]autorelease];
    UIButton *more =[UIButton buttonWithType:UIButtonTypeCustom];
    more.frame = CGRectMake(10, 110, 60, 30);
    more.tag = 20;
    [more addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [more setBackgroundImage:[UIImage imageNamed:@"moreBtn.png"] forState:UIControlStateNormal];
    [self.view addSubview:more];
}


-(IBAction)btnPressed:(id)sender{
    int i =[sender tag];
    NSLog(@"%i",i);
       NSLog(@"You Tapped %@",sender);
    if (i==20) {
     
        
        [self updataView];
    }
    else
    {
    AboutDetailViewController *detailViewController = [[AboutDetailViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
    detailViewController.pid = i;
        [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];	
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
