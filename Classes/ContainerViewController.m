//
//  ContainerViewController.m
//  GadawalV1
//
//  Created by waleed on 8/25/13.
//
//

#import "ContainerViewController.h"
#import "AboutViewController.h"
//#import "DashBoardViewController.h"
#import "NewMainViewController.h"
#import "MoreViewController.h"
#import "SafartiViewController.h"
#import "HajOmViewController.h"

@interface ContainerViewController ()
@end

@implementation ContainerViewController
@synthesize contView,tabView;
@synthesize homeBtn,settBtn,usBtn,tablBtn;
@synthesize btnImgs,btnImgsOV;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    btnImgs =[[NSMutableArray alloc]initWithObjects:@"us.png",@"settings.png",@"table.png",@"home1.png", nil];
    btnImgsOV =[[NSMutableArray alloc]initWithObjects:@"us_over.png",@"settings_over.png",@"table_over.png",@"home_over.png", nil];
   contView = [[UIView alloc]initWithFrame:CGRectMake(0, -20*highf,320, 400*highf)];
   contView.backgroundColor = [UIColor clearColor];
   [self.view addSubview:contView];
   tabView = [[UIView alloc]initWithFrame:CGRectMake(0, 370*highf, 320, 90*highf)];
   tabView.backgroundColor=[UIColor clearColor];
   [self.view addSubview:tabView];
   [self drawTab];
    UIImageView * _splashad=[[[UIImageView alloc] initWithFrame:CGRectMake(0, 0*highf, 320, 480*highf)] autorelease];
    
    UIButton *btnclose_splash=[[UIButton alloc] initWithFrame:CGRectMake((_splashad.frame.origin.x+_splashad.frame.size.width)-20, _splashad.frame.origin.y+5, 15, 15)];
    btnclose_splash.backgroundColor=[UIColor redColor];
    [btnclose_splash setTitle:@"x" forState:UIControlStateNormal];
    [btnclose_splash setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    
  //  [self loadSplash:_splashad closingbutton:btnclose_splash ];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden=YES;
    [self drawCont];
}


-(void)drawTab{
    UIImageView *tabBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 90*highf)];
    tabBg.image = [UIImage imageNamed:@"tabImg.png"];
    tabBg.tag=9;
    [tabView addSubview:tabBg];
   
    usBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [usBtn setFrame:CGRectMake(120, 5*highf, 40*highf, 40*highf)];
    [usBtn setTag:0];
    [usBtn setImage:[UIImage imageNamed:@"us.png"] forState:UIControlStateNormal];
    [usBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:usBtn];
    
    settBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [settBtn setFrame:CGRectMake(165, 5*highf, 40*highf, 40*highf)];
    [settBtn setTag:1];
    [settBtn setImage:[UIImage imageNamed:@"settings.png"] forState:UIControlStateNormal];
    [settBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:settBtn];
    
    tablBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [tablBtn setFrame:CGRectMake(210, 5*highf, 40*highf, 40*highf)];
    [tablBtn setTag:2];
    [tablBtn setImage:[UIImage imageNamed:@"table.png"] forState:UIControlStateNormal];
    [tablBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:tablBtn];
    
    homeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [homeBtn setFrame:CGRectMake(255, 5*highf, 40*highf, 40*highf)];
    [homeBtn setTag:3];
    [homeBtn setImage:[UIImage imageNamed:@"home.png"] forState:UIControlStateNormal];
    [homeBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:homeBtn];
    
    
    UIButton *btnads=[[[UIButton alloc] initWithFrame:CGRectMake(0, 375*highf, 104, 40*highf)] autorelease];
    btnads.backgroundColor=[UIColor clearColor];
    [btnads setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   [self loadThirdAd:btnads];
    
    UIWebView * _footerad=[[[UIWebView alloc] initWithFrame:CGRectMake(-10, 415*highf, 320, 55*highf)] autorelease];
   // _footerad.backgroundColor=[UIColor blueColor];
    _footerad.opaque = NO;
    _footerad.backgroundColor = [UIColor clearColor];
  
     UIButton *btnclose=[[UIButton alloc] initWithFrame:CGRectMake((_footerad.frame.origin.x+_footerad.frame.size.width)-20, (_footerad.frame.origin.y+5)*highf, 15*highf, 15*highf)];
    btnclose.backgroundColor=[UIColor redColor];
    [btnclose setTitle:@"x" forState:UIControlStateNormal];
    [btnclose setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
    [self loadfooter:_footerad closingbutton:nil];
}

-(IBAction)btnClicked:(id)sender{
    int i = [sender tag];
     for (UIView *v in self.tabView.subviews) {
        if ([v isKindOfClass:[UIButton class]]) {
            UIButton *tmpBtn = (UIButton *)v;
            if (tmpBtn.tag==i) {
                [tmpBtn setImage:[UIImage imageNamed:[btnImgsOV objectAtIndex:i]] forState:UIControlStateNormal];
            }
            else{
                  [tmpBtn setImage:[UIImage imageNamed:[btnImgs objectAtIndex:tmpBtn.tag]] forState:UIControlStateNormal];
            }
        }
    }
    
   for (UIView *vv in self.contView.subviews) {
        [vv removeFromSuperview];
   }
    
    if (i==0) {
       MoreViewController *myAboutViewController = [[MoreViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myAboutViewController];
       [self.contView addSubview:navController.view];
       }
    else if (i==1) {
        SafartiViewController *myFirstViewController = [[SafartiViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myFirstViewController];
        [self.contView addSubview:navController.view];
        }
    else if (i==2) {
        HajOmViewController *myGadViewController = [[HajOmViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
       
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myGadViewController];
        [self.contView addSubview:navController.view];
    }
    else if (i==3){
        NewMainViewController *mainViewController = [[NewMainViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
      
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
        [self.contView addSubview:navController.view];
    }
}


-(void)drawCont{
        [homeBtn setImage:[UIImage imageNamed:@"home_over.png"] forState:UIControlStateNormal];
        NewMainViewController *mainViewController = [[NewMainViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
     
         UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
        [self.contView addSubview:navController.view];
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
