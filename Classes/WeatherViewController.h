//
//  WeatherViewController.h
//  mutnav2
//
//  Created by walid nour on 2/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BugSense-iOS/BugSenseController.h>
@class RXMLElement;
@class Reachability;

@interface WeatherViewController : UIViewController
<UITableViewDataSource,UITableViewDelegate>
@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property (retain, nonatomic) IBOutlet UITableView *mainTable;

-(NSMutableArray *) getTemp;

@end
