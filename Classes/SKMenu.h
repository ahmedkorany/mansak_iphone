//
//  SKMenu.h
//
//
//
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

//////DataBase Classess prosessing
#import <UIKit/UIKit.h>
#import "SKDatabase.h"

@interface SKMenu : NSObject {
    
    SKDatabase *myDB;
}


- (id)initWithFile:(NSString *)dbFile;
- (int)countForMenuWithParent:(int)parentid FromTable:(NSString *)table;

- (id)contentForMenuWithParent:(int)parentid Row:(int)row content:(NSString *)contenttype FromTable:(NSString *)table;
- (int)integerForMenuWithParent:(int)parentid Row:(int)row content:(NSString *)contenttype FromTable:(NSString *)table;
- (NSMutableArray *)contentForMenuWithParent:(int)parentid content:(NSString *)contenttype FromTable:(NSString *)table;
- (id)contentForMenuWithID:(int)row content:(NSString *)contenttype FromTable:(NSString *)table;

- (id)contentWithID:(int)row content:(NSString *)contenttype FromTable:(NSString *)table;

- (void)insertRecordIntoTableNamed:(NSString *)tableName withField:(NSString *)field withValue:(NSString *)value;

-(NSMutableArray*) getdata: (NSString*) myquery forType:(NSString *)rettype;

- (int)countFoTable:(NSString *)table;
- (int)countForTable:(NSString *)table;

- (void) deleteFromTable:(NSString *)tableName;

- (void) deleteFromTable:(NSString *)tableName ByID:(int)adsID;

- (id) getRow: (NSString *)sql;

- (int)countFoTable:(NSString *)table withField:(NSString *)field withValue:(NSString *)value;
- (int) getDataINT: (NSString *)sql;
- (void) deleteFromTable:(NSString *)tableName withField:(NSString *)field ByID:(int)adsID;
@end
