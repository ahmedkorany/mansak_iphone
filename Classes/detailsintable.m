//
//  detailsintable.m
//  mutnav2
//
//  Created by amr on 1/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "detailsintable.h"


@implementation detailsintable
@synthesize sdic,plist,myid,arabicdic;

#pragma mark -
#pragma mark View lifecycle


- (void)viewDidLoad {
    [super viewDidLoad];
	myid=[sdic objectForKey:@"id"];
	self.title=[sdic objectForKey:@"name"];
	arabicdic=[[NSMutableDictionary alloc] init];
	[arabicdic setValue:@"الاسم" forKey:@"name"];
	[arabicdic setValue:@"الهاتف" forKey:@"phone"];
	[arabicdic setValue:@"الفاكس" forKey:@"fax"];
	[arabicdic setValue:@"العنوان" forKey:@"address"];
	NSLog(@"sssssssssssssssssssssssssssssssssss");
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [arabicdic count]+1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //NSLog(@"dddddddddddddddddddddddd");
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell...
	//if(indexPath.row>1){
	if(indexPath.row!=4){
	id mykey=[[arabicdic allKeys] objectAtIndex:indexPath.row];
	//NSLog(@"%@",mykey);
	NSString *keystring=[[NSString alloc] initWithFormat:@"%@",mykey];
	NSString *finalstring=[[NSString alloc] initWithFormat:@"%@",[sdic objectForKey:keystring]];
        NSLog(@"%@", finalstring);
	keystring=[[NSString alloc] initWithFormat:@"%@",[arabicdic objectForKey:keystring]];
	//if([keystring isEqualToString:@"name"])
	//	keystring=name;
	//NSLog(@"---%@",mykey);
    //cell.textLabel.text=keystring;
	//cell.detailTextLabel.text=finalstring;
     cell.textLabel.text=finalstring;
     cell.detailTextLabel.text=keystring;
     cell.textLabel.textAlignment = UITextAlignmentRight;
     cell.detailTextLabel.textAlignment = UITextAlignmentRight;
     cell.textLabel.numberOfLines=2;
        
	}else {
		//cell.textLabel.text=[sdic objectForKey:@"phone"];
		//cell.detailTextLabel.text=@"اضغط هنا لتتمكن من التصال به٫ا الرقم";
        CGRect rect = CGRectMake(10, 5, 80, 30);
		UIButton *button=[[UIButton alloc] initWithFrame:rect];
               [button setFrame:rect];
      //  button.alpha = 1;
        button.backgroundColor = [UIColor grayColor];
       // button.titleLabel.text = @"اتصل";
       // button.titleLabel.textColor = [UIColor blueColor];
            
        
        NSString *contactstring=[[NSString alloc] initWithFormat:@"%@",[sdic objectForKey:@"phone"]];
              button.tag = [contactstring intValue];
      //  NSLog(@"....tag....%d", button.tag);
      //  NSLog(@"%@",contactstring);
        
       [button addTarget:self action:@selector(buttonContact:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        [button release];

	}

	//}
	//NSLog(@"cccccccccc");
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
	//if(indexPath.row==4){
	//[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:0112382478"]];
	//}
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

-(IBAction)buttonContact:(id)sender{
    NSString *contactstring=[[NSString alloc] initWithFormat:@"%d",[sender tag]];
    NSLog(@"%@",contactstring);
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:contactstring]];
}


- (void)dealloc {
	[myid release];
	[plist release];
	[sdic release];
	[arabicdic release];
    [super dealloc];
}


@end

