//
//  CustomCell.m
//  CustomTable
//
//  Created by Cayden Liew on 11/12/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomCell.h"
#import "Global.h"
@implementation CustomCell
@synthesize leftLabel, imageView,selectimage;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        //left side
        selectimage=[[UIImageView alloc] initWithFrame:CGRectMake(0,5, tablesize.size.width, 40)];
        [self.contentView addSubview:selectimage];
        selectimage.tag=5;
        self.leftLabel = [[UILabel alloc] init];
        self.leftLabel.frame = CGRectMake(0,(50)/2-17.5, tablesize.size.width, 35);
        self.leftLabel.font = [UIFont boldSystemFontOfSize:14];
        self.leftLabel.backgroundColor = [UIColor clearColor];
        self.leftLabel.textColor = [UIColor whiteColor];
        self.leftLabel.textAlignment= NSTextAlignmentCenter;
        self.leftLabel.tag=3;
        [self.contentView addSubview:leftLabel];
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(tablesize.size.width-55, (50)/2-17.5, 50, 35)];
        self.imageView.tag=4;
        [self.contentView addSubview:imageView];
    }
    
    return self;
}



@end
