//
//  RootViewController.m
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "RootViewController.h"




@interface RootViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation UINavigationBar (CustomImage)

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed: @"gg.png"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}
@end


@implementation RootViewController
@synthesize mainarr;

@synthesize mainTable, backImg;
@synthesize fetchedResultsController=fetchedResultsController_, managedObjectContext=managedObjectContext_;


#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    
    UINavigationBar *navBar = [self.navigationController navigationBar];
    if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        // set globablly for all UINavBars
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"gg.png"] forBarMetrics:UIBarMetricsDefault];
        
        // could optionally set for just this navBar
        //[navBar setBackgroundImage:...
    }
    
    //self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
  UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"عودة" style:UIBarButtonItemStyleBordered                        target:nil  action:nil];
   
   
    //UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"backBtn.png"] style:UIBarButtonItemStylePlain target:nil action:nil];
     self.navigationItem.backBarButtonItem = backButton;
    
    
    [backButton release];
    
   	NSString *mypath=[[NSBundle mainBundle] pathForResource:@"mainmenue" ofType:@"plist"];
	mainarr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
        self.title=@"القائمة الرئيسية";
    
    
    CGFloat width = [UIScreen mainScreen].bounds.size.width ;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    backImg  = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    CGRect backFrame = CGRectMake(0,0, width, height-20);
    backImg.frame = backFrame;
    backImg.alpha =1;
    [self.view addSubview:backImg];
    
    mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0.0, 30.0, width, height-30)style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=height;
    mainTable.scrollEnabled = NO;
    mainTable.editing = NO;
   // mainTable.canCancelContentTouches =NO;
    
    mainTable.allowsSelection= NO;
    
  
    
    [self.view addSubview:mainTable];
    self.tableView.editing = NO;
    self.tableView.scrollEnabled =NO;
    [self.tableView removeFromSuperview];
    
    
    
    
/*self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 00, width, height
                                                               ) style:UITableViewStylePlain]autorelease];
    //mainTable.delegate = self;
    //mainTable.dataSource = self;
    //mainTable.autoresizesSubviews = YES;
    self.tableView.backgroundColor =[UIColor clearColor];
  //  mainTable.backgroundColor=[UIColor clearColor];
    self.tableView.alpha = 0.7;
    self.tableView.rowHeight=height;
    self.tableView.scrollEnabled = NO;*/
    
    //[self.view addSubview:mainTable];
    // Set up the edit and add buttons.
  //  self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
 //   UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject)];
 //   self.navigationItem.rightBarButtonItem = addButton;
   // [addButton release];
}


// Implement viewWillAppear: to do additional setup before the view is presented.
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}


- (BOOL) tableView: (UITableView *) tableView canEditRowAtIndexPath: (NSIndexPath *) indexPath {
   return NO;
} // canEditRowAtIndexPath

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
        return UITableViewCellEditingStyleNone;

}
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/

/*
 // Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
 */


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [[managedObject valueForKey:@"timeStamp"] description];
    
}


#pragma mark -
#pragma mark Add a new object

- (void)insertNewObject {
    
  //  // Create a new instance of the entity managed by the fetched results controller.
   // NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
  //  NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
  //  NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
  //  // If appropriate, configure the new managed object.
  //  [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
    
  //  // Save the context.
  //  NSError *error = nil;
  //  if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
    //    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    //    abort();
    //}
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {

    // Prevent new objects being added when in editing mode.
    [super setEditing:(BOOL)editing animated:(BOOL)animated];
    self.navigationItem.rightBarButtonItem.enabled = !editing;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  //  return [[self.fetchedResultsController sections] count];
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   // id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    //return [sectionInfo numberOfObjects];
	//return [mainarr count]-1;
    return 1;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *hlCellID = @"hlCellID";
    UITableViewCell *hlcell = [mainTable dequeueReusableCellWithIdentifier:hlCellID];
   
    

    
    if(hlcell == nil) {
        hlcell =  [[[UITableViewCell alloc] 
                    initWithStyle:UITableViewCellStyleDefault reuseIdentifier:hlCellID] autorelease];
        hlcell.accessoryType = UITableViewCellAccessoryNone;
        hlcell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //int section = indexPath.section;
    // NSMutableArray *sectionItems = [sections objectAtIndex:section];
    
    //int n = [sectionItems count];
    int n = [mainarr count]-1;
    int i=0,i1=0; 
    
    while(i<n){
        int yy = 10 +i1*84;
        int j=0;
    
        for(j=0; j<3;j++){
            
            if (i>=n) break;
                      
            NSMutableDictionary *dic=[mainarr objectAtIndex:i];
            
            CGRect rect = CGRectMake((100*j)+20, yy, 80, 65);
        //      CGRect rect = CGRectMake(xx, yy, 80, 55);
            UIButton *button=[[UIButton alloc] initWithFrame:rect];
            [button setFrame:rect];
            button.alpha = 1;
            UIImage *buttonImageNormal=[UIImage imageNamed:[dic objectForKey:@"img"]];
            [button setBackgroundImage:buttonImageNormal	forState:UIControlStateNormal];
            [button setContentMode:UIViewContentModeCenter];
            
            NSString *tagValue = [NSString stringWithFormat:@"%d", i];
            button.tag = [tagValue intValue];
            //NSLog(@"....tag....%d", button.tag);
            
            [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [hlcell.contentView addSubview:button];
            [button release];
            
            UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake((110*j), yy+69, 90, 15)] autorelease];
            label.text = [dic objectForKey:@"name"];
            label.textColor = [UIColor blackColor];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = UITextAlignmentCenter;
            //label.font = [UIFont fontWithName:@"ArialMT" size:12]; 
            label.font = [UIFont boldSystemFontOfSize:12];
            [hlcell.contentView addSubview:label];
            
            
            
            i++;
            
        }
        i1 = i1+1;
    }
    
    
    
    return hlcell;
    
    
/*    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    // Configure the cell.
	NSMutableDictionary *dic=[mainarr objectAtIndex:indexPath.row];
	
   // [self configureCell:cell atIndexPath:indexPath];
	cell.textLabel.text=[dic objectForKey:@"name"];
    cell.textLabel.textAlignment = UITextAlignmentRight;
	cell.imageView.image=[UIImage imageNamed:[dic objectForKey:@"img"]];
    
	NSString *imgname=[[NSString alloc] initWithFormat:@"n%d.png",indexPath.row+1];
	UIImageView *imgview=[[UIImageView alloc] initWithImage:[UIImage imageNamed:imgname]];
	
	cell.accessoryView=imgview;
	[imgview release];
    return cell;*/
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }   
}

-(IBAction)buttonPressed:(id)sender{
    int i = [sender tag];
     NSMutableDictionary *dic=[mainarr objectAtIndex:i];
    
     if([[dic objectForKey:@"targetpage"] intValue]==0){
     //secondmenue *detailViewController = [[secondmenue alloc] initWithNibName:@"secondmenue" bundle:nil];
         ListViewController *detailViewController = [[ListViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
     detailViewController.sdic=dic;
     detailViewController.plist=@"secondmenue";
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];	
     }
     else if([[dic objectForKey:@"targetpage"] intValue]==4){
     PrayTimeViewController *detailViewController = [[PrayTimeViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     
     
     }
    
   /*else if([[dic objectForKey:@"targetpage"] intValue]==9){
         LocViewController *detailViewController = [[LocViewController alloc] initWithNibName:@"LocViewController" bundle:nil];
         [self.navigationController pushViewController:detailViewController animated:YES];
         [detailViewController release];
         
         
     }*/
     else if(i==9){
         LocViewController *detailViewController = [[LocViewController alloc] initWithNibName:@"LocViewController" bundle:nil];
         [self.navigationController pushViewController:detailViewController animated:YES];
         [detailViewController release];
         
         
     }

    else
    {
        NSLog(@"%d", i);
    }
    
    
}
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // The table view should not be re-orderable.
    return NO;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here -- for example, create and push another view controller.
   /* NSMutableDictionary *dic=[mainarr objectAtIndex:indexPath.row];
	//NSLog(@"%@",[dic objectForKey:@"targetpage"]);
	if([[dic objectForKey:@"targetpage"] intValue]==0){
     secondmenue *detailViewController = [[secondmenue alloc] initWithNibName:@"secondmenue" bundle:nil];
		detailViewController.sdic=dic;
		NSLog(@"%d",[[dic objectForKey:@"targetpage"] intValue]);
		NSLog(@"%@",[dic objectForKey:@"name"] );
		NSLog(@"%d",[[dic objectForKey:@"id"] intValue]);
	//detailViewController.title=[dic objectForKey:@"name"];
	//detailViewController.name=[dic objectForKey:@"name"];
	//detailViewController.myid=[dic objectForKey:@"id"];
	detailViewController.plist=@"secondmenue";
	//NSManagedObject *selectedObject = [[self fetchedResultsController] objectAtIndexPath:indexPath];
	// ...
	// Pass the selected object to the new view controller.
	[self.navigationController pushViewController:detailViewController animated:YES];
	[detailViewController release];	
		//[dic release];
	}
    else if([[dic objectForKey:@"targetpage"] intValue]==4){
        PrayTimeViewController *detailViewController = [[PrayTimeViewController alloc] initWithNibName:@"PrayTimeViewController" bundle:nil];
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];

        
    }*/
	
}


#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (fetchedResultsController_ != nil) {
        return fetchedResultsController_;
    }
    
    /*
     Set up the fetched results controller.
    */
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    [aFetchedResultsController release];
    [fetchRequest release];
    [sortDescriptor release];
    [sortDescriptors release];
    
    NSError *error = nil;
    if (![fetchedResultsController_ performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return fetchedResultsController_;
}    


#pragma mark -
#pragma mark Fetched results controller delegate


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [self setBackImg:nil];
    [self setMainTable:nil];

    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


- (void)dealloc {
    [fetchedResultsController_ release];
    [managedObjectContext_ release];
	[mainarr release];
    [super dealloc];
}


@end

