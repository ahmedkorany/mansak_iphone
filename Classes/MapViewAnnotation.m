//
//  MapViewAnnotation.m
//  mutnav2
//
//  Created by walid nour on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MapViewAnnotation.h"

@implementation MapViewAnnotation

@synthesize title, coordinate;

- (id)initWithTitle:(NSString *)ttl andCoordinate:(CLLocationCoordinate2D)c2d {
    if (!(self = [super init])) return nil;
    title = ttl;
    coordinate = c2d;
    return self;
}


@end
