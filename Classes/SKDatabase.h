//
//  SKDatabase.h
//
//  Created by walid nour on 5/13/12.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//


/**
 *  this class for save and read from sqlite database
 */

#import <UIKit/UIKit.h>
#import <sqlite3.h>
@interface SKDatabase : NSObject {
    sqlite3 *dbh;
}

- (id)initWithFile:(NSString *)dbFile;
- (void) copyDatabaseIfNeeded:(NSString *)dbFile;
- (void)close;
- (sqlite3 *)dbh;
- (sqlite3_stmt *)prepare:(NSString *)sql;
- (id)lookupSingularSQL:(NSString *)sql forType:(NSString *)rettype;
- (id)lookupSingularSQL:(NSString *)sql;
- (NSMutableArray *)lookupMultiSQL:(NSString *)sql forType:(NSString *)rettype;
- (void) insertRecordIntoeTableNamed:(NSString *)sql;
- (int)lookupSingularINT:(NSString *)sql;


@end
