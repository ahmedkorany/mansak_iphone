//
//  menu_mainViewController.m
//  mutnav2
//  Created by rashad on 7/9/14.
//

#import "menu_mainViewController.h"
#import "Reachability.h"
#import "select_haj_type.h"
#import "Global.h"
#import "MyListViewController.h"
#import "Arrays.h"
@interface menu_mainViewController (){
    
    NewMainViewController *home_view;
    GridViewController *date_view;
    TatwefViewController *tawaf_ViewController;
    manasik *manasik_view;
    select_haj_type *haj_type_view;
    PrayTimeViewController *prayViewController;
    webviewcontroller *about_view;
    MyListViewController *list;
}

@end

@implementation menu_mainViewController
@synthesize main_table=_main_table,main_view=_main_view,SUBVIEW=_SUBVIEW,show_menu_button=_show_menu_button,hide_view_button=_hide_view_button,city_btn=_city_btn,arrow_btn=_arrow_btn,background_image=_background_image,navController,channlController,newsController;
;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

# pragma Mark - drawing & init the View.
- (void)viewDidLoad
{
    [super viewDidLoad];
    // this var used in animation .
    check_anim=YES;
    
    self.view.frame= [UIScreen mainScreen].bounds;
    
    // init target Views
    home_view = [[NewMainViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
    date_view= [[GridViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
    prayViewController = [[PrayTimeViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
    tawaf_ViewController =[[TatwefViewController alloc] init];
    about_view = [[webviewcontroller alloc]initWithNibName:@"webviewcontroller" bundle:nil];
    about_view.type=1;
    
    channl_view = [[HaramViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
    channl_view.type =3;
    news_View =[[NewsListViewController alloc]initWithNibName:@"MyListViewController" bundle:nil];
    
    
    // call "init_menu_view" that it design all views of menu.
    [self init_menu_view];
    
    // call "init_mainview" that it add menu on left.
    [self init_mainview];
    
    // get data of menu from "main_menu" plist .
    NSString *mypath=[[NSBundle mainBundle] pathForResource:@"main_menu" ofType:@"plist"];
    menu_arr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
    
    // set selected row backGround and select first index programtically.
    select_img=[UIImage imageNamed:@"on-lable.png"];
    select_img_v=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    select_img_v.image=select_img;
    select_img_back=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    select_img_back.image=[UIImage imageNamed:@"select.png"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [_main_table selectRowAtIndexPath:indexPath animated:YES scrollPosition:
     UITableViewScrollPositionNone];
    [_main_table.delegate tableView:_main_table didSelectRowAtIndexPath:indexPath];
 
  
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
   
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Home Screen"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

/**
 *  This method used to draw menu table on subView to add it on mainView later.
 */
-(void)init_menu_view{
    // init subView with frame size to add on it menu table and calender & arrow buttons .
    _SUBVIEW =[[UIView alloc] initWithFrame:self.view.frame];
    
    // init menu table View and set its data
    _main_table=[[UITableView alloc] initWithFrame:CGRectMake(_SUBVIEW.frame.size.width/4,_SUBVIEW.frame.size.height/4.3,_SUBVIEW.frame.size.width-(_SUBVIEW.frame.size.width/4),_SUBVIEW.frame.size.height-(_SUBVIEW.frame.size.height/4.3))];
    tablesize=_main_table.frame;
    _main_table.delegate = self;
    _main_table.dataSource = self;
    _main_table.backgroundColor=[UIColor clearColor];
    _main_table.rowHeight=50;
    _background_image=[[UIImageView alloc] initWithFrame:self.view.frame];
    [_background_image setImage:[UIImage imageNamed:@"menu-bg.jpg"]];
    
    [self.view addSubview:_SUBVIEW];
    [_SUBVIEW addSubview:_background_image];
    [_SUBVIEW addSubview:_main_table];
    
    NSLog(@"%d ",[GetHijriDate GetDayIndex]); 
    
    // init calender Button above menu table and set its data.
    UIImageView *city_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0, 70,70)];
    city_image.image=[[UIImage imageNamed:@"cal.png" ] stretchableImageWithLeftCapWidth:10  topCapHeight:0];
    _city_btn=[[UIButton alloc] initWithFrame:CGRectMake(_SUBVIEW.frame.size.width/2.05, _SUBVIEW.frame.size.width/6.8, 73, 73)];
    UILabel *city_selected=[[UILabel alloc] initWithFrame:CGRectMake(3, 28, _city_btn.frame.size.width-9, 35)];
    city_selected.text=[GetHijriDate GetHiriDate];
    city_selected.backgroundColor=[UIColor blackColor];
    city_selected.lineBreakMode = NSLineBreakByWordWrapping;
    city_selected.numberOfLines = 0;
    [city_selected setFont:[UIFont systemFontOfSize:10]];
    city_selected.textAlignment=NSTextAlignmentCenter;
    city_selected.backgroundColor=[UIColor clearColor];
    [_city_btn addSubview:city_image];
    [_city_btn addSubview:city_selected];
    [_city_btn addTarget:self action:@selector(city_select:) forControlEvents:UIControlEventTouchUpInside];
    [_SUBVIEW addSubview:_city_btn];
    
    // draw arrow button at the bottom of srceen.
    _arrow_btn=[[UIButton alloc] initWithFrame:CGRectMake((_SUBVIEW.frame.size.width-25)/2,_SUBVIEW.frame.size.height-25, 25, 25)];
    [_arrow_btn setImage:[UIImage imageNamed:@"menu_arrow.png"] forState:UIControlStateNormal];
    [_arrow_btn addTarget:self action:@selector(arrow_click:) forControlEvents:UIControlEventTouchUpInside];
    [_SUBVIEW addSubview:_arrow_btn];
    
    
}

/**
 *  This method used to add menuTable behind mainView and add values & buttons to show menuTable and transit between show or hide mainView and menuTable.
 */
-(void)init_mainview{
    // var used to transition between showen and hiden position to selected view.
    show_position=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    hide_position=CGRectMake(-220, 50, show_position.size.width,  show_position.size.height-100);
    // init mainView (that always hold selected View)
    _main_view =[[UIView alloc] initWithFrame:show_position];
    _main_view.backgroundColor=[UIColor grayColor];
    [_main_view.layer setShadowColor:[UIColor blackColor].CGColor];
    [_main_view.layer setShadowOpacity:0.8];
    [_main_view.layer setShadowRadius:3.0];
    [_main_view.layer setShadowOffset:CGSizeMake(4.0, 4.0)];
    
    //init menuBtn on right of MainView.
    _show_menu_button=[[UIButton alloc] initWithFrame:CGRectMake(_main_view.frame.size.width-35, 20, 30, 30)];
    [_show_menu_button addTarget:self action:@selector(menu_btn_selected:) forControlEvents:UIControlEventTouchUpInside];
    [_show_menu_button setBackgroundImage:[UIImage imageNamed:@"menu_btn.png"] forState:UIControlStateNormal];
    
    // init hide button that added on shown view to use it in transition between hide or show mainView.
    _hide_view_button=[[UIButton alloc] initWithFrame:CGRectMake(0, 0, hide_position.size.width, hide_position.size.height)];
    [_hide_view_button addTarget:self action:@selector(show_main_view) forControlEvents:UIControlEventTouchUpInside];
    _hide_view_button.hidden=YES;
    
    [_main_view insertSubview:_show_menu_button atIndex:1];
    [_main_view insertSubview:_hide_view_button atIndex:2];
    [self.view addSubview:_main_view];
    
    // add swipe Gusture to hide button to do the same action by swipeing.
    UISwipeGestureRecognizer * swipeRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeRight:)];
    swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
    [_hide_view_button addGestureRecognizer:swipeRight];
}

#pragma mark - Menutable delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // number of sections.
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // number of arrows.
    return [menu_arr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // draw cell according indexPath value of table Dic.
    NSString *CellIdentifier = [NSString stringWithFormat:@"MyReuseIdentifier %ld",(long)indexPath.row];
    
    CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    // add to cell its localization text and img.
    NSMutableDictionary *dic=[menu_arr objectAtIndex:[indexPath row]];
    cell.backgroundColor=[UIColor clearColor];
    cell.leftLabel.text=NSLocalizedString([dic objectForKey:@"name"], @"Menu");
    cell.imageView.image = [UIImage imageNamed:[dic objectForKey:@"img"]];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // it iterate on all rows and set image off to each row and image on & backGround to selected row.
     int page_view=0;
    for (int i=0; i<[menu_arr count]; i++) {
        // get all data of each cell.
        NSMutableDictionary *dic=[menu_arr objectAtIndex:i];
        NSIndexPath* cellPath = [NSIndexPath indexPathForRow:i inSection:0];
        UITableViewCell* cell = [tableView cellForRowAtIndexPath:cellPath];
        UILabel *label = (UILabel *)[cell viewWithTag:3];
        UIImageView *image = (UIImageView *)[cell viewWithTag:4];
        UIImageView *back_image = (UIImageView *)[cell viewWithTag:5];
        
        if (i==[indexPath row]) {
            // if it is selected cell get image on of this cell and change the backGround image.
            label.textColor=[UIColor colorWithRed:0.612 green:0.373 blue:0.024 alpha:1];
            image.image=[UIImage imageNamed:[dic objectForKey:@"img-on"]];
            back_image.image=[UIImage imageNamed:@"on-lable.png"];
            page_view = [[dic objectForKey:@"id"]intValue];
        }else{
            // if it isn't back to default img.
            label.textColor=[UIColor whiteColor];
            image.image=[UIImage imageNamed:[dic objectForKey:@"img"]];
            back_image.image=nil;
        }
    }
    // this 3 var used when the selected view is listView.
    NSArray *mylist;
    NSString *selectorName;
    SEL s;
    channlController =[[UINavigationController alloc]initWithRootViewController:channl_view];
    newsController =[[UINavigationController alloc]initWithRootViewController:news_View];
    /**
     *   switch in indexPath to add selected view to ManiView and send it to back.
     *   it add the newest view with tag 3 and remove the old one.
     */
    switch (page_view) {
        case 0:
            // Home View
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:home_view.view];
            [_main_view sendSubviewToBack:home_view.view];
            home_view.view.tag=3;
            break;
        case 1:
            // select hajj type view.
            haj_type_view=[[select_haj_type alloc] init];
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:haj_type_view.view];
            _main_view.backgroundColor=[UIColor whiteColor];
            [_main_view sendSubviewToBack:haj_type_view.view];
            haj_type_view.view.frame=CGRectMake(0, 0, show_position.size.width, show_position.size.height);
            haj_type_view.view.tag=3;
            break;
        case 2:
            // 3omara view.
            [[_main_view viewWithTag:3] removeFromSuperview];
            manasik_view =[[manasik alloc] init];
            [_main_view addSubview:manasik_view.view];
            [_main_view sendSubviewToBack:manasik_view.view];
            manasik_view.view.frame=CGRectMake(0, 0, show_position.size.width, show_position.size.height);
            manasik_view.view.tag=3;
            break;
        case 3:
            // tatwef View.
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:tawaf_ViewController.view];
            [_main_view sendSubviewToBack:tawaf_ViewController.view];
            tawaf_ViewController.view.frame=CGRectMake(0, 0, show_position.size.width, show_position.size.height);
            tawaf_ViewController.view.tag=3;
            break;
        case 4:
            // zayara view ..
            // it get its list array from "Arrays" class and set the view to navController and add the same steps of switch cases.
            list=[[MyListViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
            selectorName=@"zyara";
            s = NSSelectorFromString(selectorName);
            if ([Arrays performSelector:s]) {
                mylist= [Arrays performSelector:s];
                if ([mylist[0]isEqualToString:@"list"]) {
                    list.listArray=mylist;
                    list.mainTitle=@"zyara";
                }
            }
            
            navController = [[UINavigationController alloc]initWithRootViewController:list];
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:navController.view];
            [_main_view sendSubviewToBack:navController.view];
            navController.view.tag=3;
            break;
            
           case 5:
            // maps view ..
            // it get its list array from "Arrays" class and set the view to navController and add the same steps of switch cases.
             list=[[MyListViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
            selectorName=@"maps";
            s = NSSelectorFromString(selectorName);
            if ([Arrays performSelector:s]) {
                mylist= [Arrays performSelector:s];
                if ([mylist[0]isEqualToString:@"list"]) {
                    list.listArray=mylist;
                    list.mainTitle=@"maps";
                }
            }
           
            
            navController = [[UINavigationController alloc]initWithRootViewController:list];
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:navController.view];
            [_main_view sendSubviewToBack:navController.view];
            navController.view.tag=3;
            break;
        case 6:
            // mawqet salah view.
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:prayViewController.view];
            [_main_view sendSubviewToBack:prayViewController.view];
            prayViewController.view.tag=3;
            break;
            
            
        case 7:
            // se7a view ..
            // it get its list array from "Arrays" class and set the view to navController and add the same steps of switch cases.
            list=[[MyListViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
            selectorName=@"se7a";
            s = NSSelectorFromString(selectorName);
            if ([Arrays performSelector:s]) {
                mylist= [Arrays performSelector:s];
                if ([mylist[0]isEqualToString:@"list"]) {
                    list.listArray=mylist;
                    list.mainTitle=@"se7a";
                }
            }
            
            navController = [[UINavigationController alloc]initWithRootViewController:list];
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:navController.view];
            [_main_view sendSubviewToBack:navController.view];
            navController.view.tag=3;
            break;
        case 8:
            // gna2ez view ..
            // it get its list array from "Arrays" class and set the view to navController and add the same steps of switch cases.
            list=[[MyListViewController alloc] initWithNibName:@"MyListViewController" bundle:nil];
            selectorName=@"gna2ez";
            s = NSSelectorFromString(selectorName);
            if ([Arrays performSelector:s]) {
                mylist= [Arrays performSelector:s];
                if ([mylist[0]isEqualToString:@"list"]) {
                    list.listArray=mylist;
                    list.mainTitle=@"gna2ez";
                }
            }
            
            navController = [[UINavigationController alloc]initWithRootViewController:list];
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:navController.view];
            [_main_view sendSubviewToBack:navController.view];
            navController.view.tag=3;
            break;
        case 9:
            // about us
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:about_view.view];
            [_main_view sendSubviewToBack:about_view.view];
            about_view.view.tag=3;
            break;
        case 10:
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:channlController.view];
            [_main_view sendSubviewToBack:channlController.view];
            //    listController.view.frame=CGRectMake(0, 0, hide_position.size.width, hide_position.size.height);
            channlController.view.tag=3;
            break;

        case 12:
            [[_main_view viewWithTag:3] removeFromSuperview];
            [_main_view addSubview:newsController.view];
            [_main_view sendSubviewToBack:newsController.view];
            //    listController.view.frame=CGRectMake(0, 0, hide_position.size.width, hide_position.size.height);
            newsController.view.tag=3;
            
            break;
        default:
            break;
    }
    // after getting selected view .. call "show_main_view" to display the view animated.
    [self show_main_view];
}


#pragma Mark - Animations

/**
 *  This method made to handel transition from hide main view position to show it .. it make it by animation .
 */
-(void)show_main_view{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    _main_view.transform = CGAffineTransformIdentity;
    _main_view.frame = CGRectMake(show_position.origin.x, 0, show_position.size.width,show_position.size.height);
    ;
    _main_view.transform=CGAffineTransformMakeScale(1,1);
    [UIView commitAnimations];
    _hide_view_button.hidden=YES;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.4];
    _main_table.alpha=0.5;
    [UIView commitAnimations];
}

/**
 *  This method made to handel transition from show main view position to hide it .. it make it by animation .
 */
-(void)hide_main_view{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    _main_view.transform = CGAffineTransformIdentity;
    _main_view.frame = CGRectMake(hide_position.origin.x, 0, show_position.size.width,show_position.size.height);
    _main_view.transform=CGAffineTransformMakeScale(0.8,0.8);
    [UIView commitAnimations];
    _hide_view_button.hidden=NO;
    [UIView beginAnimations:@"fadeInNewView" context:NULL];
    [UIView setAnimationDuration:0.5];
    _main_table.alpha=1;
    [UIView commitAnimations];
}

/**
 *  This method used to rotate the gaven view to 180 degree animated with gaven duration and change flag of animatedView to check it later.
 *
 *  @param view     targetView.
 *  @param duration time of action.
 */
-(void)rotate180:(UIView*)view withDuration:(NSTimeInterval)duration {
    [UIView animateWithDuration:0.2 delay:0 options:0 animations: ^{
        view.transform = CGAffineTransformRotate(view.transform, -3.141593);
    } completion: ^(BOOL completed) {
        if (completed) {
            check_anim=NO;
        }
    }];
}

/**
 *  This method used to rotate the gaven view to 181 degree animated with gaven duration and change flag of animatedView to check it later.
 *  NOTE .. it return the target View to normal position.
 *  @param view     targetView.
 *  @param duration time of action.
 */
-(void)rotate181:(UIView*)view withDuration:(NSTimeInterval)duration {
    [UIView animateWithDuration:0.2 delay:0 options:0 animations: ^{
        view.transform = CGAffineTransformRotate(view.transform, +3.141593);
    } completion: ^(BOOL completed) {
        if (completed) {
            check_anim=YES;
        }
    }];
}

#pragma Mark - buttons methods action.

/**
 *  This method made to handel caleder button pressed .. it display calendar View as subView of view (to be the MainView).
 *
 *  @param sender calenderBtn.
 */
-(void)city_select:(id)sender{
    
    [self.view addSubview:date_view.view];
}

/**
 *  This method made to handel arrow menu pressed .. it can scroll the view or just added to let user know that there is another rows.
 *
 *  @param sender arrowBtn.
 */
-(void)arrow_click:(id)sender{
    // this method not implemented yet,it just added to let user know that there is another rows.
}

/**
 *  This method made to handel menuBtn pressed .. it call "hide_main_view" that display the table and hide mainView animated.
 *
 *  @param sender menuBtn.
 */
-(IBAction)menu_btn_selected:(id)sender{
    [self hide_main_view];
    
}

/**
 *  This method made to handel swiperight gesture .. it call "show_main_view" that display the mainView and hide menuTable animated.
 *
 *  @param gestureRecognizer swipeRight .
 */
-(void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [self show_main_view];
}

// when user scroll the view .. it check the status of arrow button and rotate it according the status from "check_anim" var.
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat height = _main_table.frame.size.height;
    
    CGFloat contentYoffset = _main_table.contentOffset.y;
    
    if((contentYoffset+height) > (_main_table.contentSize.height-20))
    {
        if (check_anim) {
            [self rotate180:_arrow_btn withDuration:0.5];
            check_anim=NO;
        }
    }else{
        if (!check_anim) {
            [self rotate181:_arrow_btn withDuration:0.5];
            check_anim=YES;
            
        }
    }
}


@end
