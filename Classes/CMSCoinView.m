//
//  CMSCoinView.m
//  FlipViewTest
//
//  Created by Rebekah Claypool on 10/1/13.
//  Copyright (c) 2013 Coffee Bean Studios. All rights reserved.
//

#import "CMSCoinView.h"
#import "manasik.h"
#import "Global_style.h"
#import "Global.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"


@interface CMSCoinView (){
    bool displayingPrimary;
    manasik *manasik_v;
}
@end

@implementation CMSCoinView

@synthesize view1=_view1, view2=_view2;

/**
 *  This method work when targetButton pressed(button in hajViews) .. it get the coinView(view that hold targetButton)and make anotherView behind .
 * draw webPage programmtically on this secondView & add swipe to new view.
 * get all needed data from sented plist and call the rest of init methods
 
 *  @param target     pressedButton used to return back to this view when finish webPage.
 *  @param plist_file hold all data (images , titles , html links)of all hajview manasik.
 *  @param mansak_id  used to get data of selected manask from manasikArray(that hold data from plist).
 *  @param mansak_day integer used to get index of day of this mansk.
 */
-(void)setUpView :(id)target plist_name:(NSString *)plist_file mansak_id:(NSInteger)mansak_id mansak_day:(NSInteger)mansak_day{
    
    // flag used to know what view showen now button or webPageView.
    check=0;

    // coinView(view that hold targetButton)
    self.view1 =(UIImageView *)[self viewWithTag:18];
    
    // secondView when buttonPressed
    self.view2 = [[UIImageView alloc]initWithFrame:CGRectMake(_view1.frame.origin.x, _view1.frame.origin.y, 320, 180)];
    [self addSubview:self.view2];
    [self sendSubviewToBack:self.view2];
    self.view2.hidden=YES;
    self.clipsToBounds=YES;
    self.view2.layer.cornerRadius=20;
    self.view1.layer.cornerRadius=20;
    
    // add swipe gesture to view to make transition between pages.
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeleft];
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swiperight];
    
    // get need data from plist
    mansak_plist_file=plist_file;
    NSString *mypath=[[NSBundle mainBundle] pathForResource:plist_file ofType:@"plist"];
    manasik_days=[[NSMutableArray alloc]initWithContentsOfFile:mypath];
    
    // get web data(plist details) from last index of plist file
    manasik_array=[[manasik_days objectAtIndex:[manasik_days count]-1]objectAtIndex:mansak_day];
    
    NSArray *arrayWithPlaces = [manasik_array valueForKey:@"id"];
    assert(manasik_array.count == arrayWithPlaces.count);
    NSUInteger index = [arrayWithPlaces indexOfObject:[NSString stringWithFormat:@"%ld",(long)mansak_id]];
    current_index=(int)index;
    current_day=(int)mansak_day;
    
    // init headerView of webPage and draw backButton
    _header_view_inner=[[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 60)];
    _header_view_inner.backgroundColor=[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [_header_view_inner addSubview:_back_btn];
    _header_view_inner.autoresizingMask=UIViewAutoresizingFlexibleWidth;
    _back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 25, 30, 30)];
    _back_btn.layer.cornerRadius=12.5;
    _back_btn.backgroundColor=[UIColor clearColor];
    [_back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [_back_btn addTarget:target action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];
    
     // init mainTitle(manasik Title)&second title (manasik day title) of webPage
    _title_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, _back_btn.frame.size.height+5)];
    _title_lbl.textAlignment=NSTextAlignmentCenter;
    _title_lbl.textColor=[UIColor whiteColor];
    _title_image_right=[[UIImageView alloc] initWithFrame:CGRectMake(_header_view_inner.frame.size.width-150,self.view2.frame.size.height-30,150,25)];
    _title_image_right.image=[UIImage imageNamed:@"inner_right.png"];
    _title_lbl_right=[[UILabel alloc] initWithFrame:CGRectMake(5, 3, _title_image_right.frame.size.width, _title_image_right.frame.size.height-5)];
    _title_lbl_right.textAlignment=NSTextAlignmentCenter;
    [_title_lbl_right setFont:[UIFont fontWithName:@"Arial-BoldMT" size:10]];
    _title_lbl_right.textColor=[UIColor whiteColor];
    [self.view2 addSubview:_title_image_right];
    [_title_image_right addSubview:_title_lbl_right];
    [_header_view_inner addSubview:_title_lbl];
    [_header_view_inner addSubview:_back_btn];
    [self.view2 addSubview:_header_view_inner];
    self.view2.userInteractionEnabled=YES;
    
    // call addWebView to draw webView
    [self addwebview];
    
    // call init data to load urls
    [self init_data];
    
    // init footerView from "Global_style"
    _footer_view=[[UIView alloc] init];
    _footer_view=[Global_style footer_view:_footer_view page_no:(int)[manasik_array count]];
    [self addSubview:_footer_view];
    
    // call initPageControl to add number of pages of this mansk.
    [self initPageControl];
}

/**
 *  draw custom PageControl with numberOfManasik and add it to footer.
 */
- (void)initPageControl
{
    // init custom_pagecontrol that different from normal in dot image
    _pageControl = [[custom_pagecontrol alloc] initWithFrame:CGRectMake(0,30, _footer_view.frame.size.width, 25)];
    _pageControl.userInteractionEnabled = NO;
    _pageControl.backgroundColor = [UIColor clearColor];
    //set number of points and set the current by selected mansk.
    _pageControl.numberOfPages = [manasik_array count];
    [_pageControl setCurrentPage:current_index];
    // add _pageControl to footerView
    [self.footer_view addSubview:_pageControl];
    
}

/**
 *  init data of webPage(titles,header image, html link) from plistData according manasik index and day.
 */
-(void) init_data{
    _web_content.delegate=self;
    // mansak_data hold all data of all manasik of selected day
    NSMutableDictionary *mansak_data=[manasik_days objectAtIndex:current_day];
    
    // mansak_details hold all data of selected mansk
    NSMutableDictionary *mansak_details=[manasik_array objectAtIndex:current_index];
    
    // init web page (_title_lbl title of mansk) &(_title_lbl_right title of day)&header image
    _title_lbl.text= NSLocalizedString([mansak_details objectForKey:@"title"],@"test");
    _title_lbl_right.text= NSLocalizedString([mansak_data objectForKey:@"hajj_type"],@"test");
    self.view2.image=[UIImage imageNamed:[mansak_details objectForKey:@"img"]];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@-%@ Screen/Iphone ",NSLocalizedString([mansak_details objectForKey:@"title"],@"test"),  NSLocalizedString([mansak_data objectForKey:@"hajj_type"],@"test")]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    // get target HTML file and load it to webView
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[mansak_details objectForKey:@"html"] ofType:@"html" inDirectory:@"www"]];
    [_web_content loadRequest:[NSURLRequest requestWithURL:url]];
    
}

//this method check if user pressed A+ or A- server return "ios" to tell me that user change size of text.-->  request has prefix ios.
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([[[request URL] absoluteString] hasPrefix:@"ios:"]) {
        
        // Call the given selector
        [self performSelector:@selector(webToNativeCall)];
        // Cancel the location change
        return NO;
    }
    
    return YES;
}

// init webView with global textSize(that user selected it in all textViews)
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [_web_content stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setsize(%d)",text_size]];
}

/**
 *  this method call when request of "shouldStartLoadWithRequest" has prefix "ios" .. that meant that user change size of text.
 *  it get size from javaScript method & save size on userDefault to used it later.
 */
- (void)webToNativeCall
{
    // get selected size fom "getsize" method.
    NSString *returnvalue =  [_web_content stringByEvaluatingJavaScriptFromString:@"getsize()"];
    
    //assign value to textSize var to ues it & save it in userDefault to use it later
    text_size=[returnvalue intValue];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%d",text_size] forKey:@"size_text"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer
                                                    *)otherGestureRecognizer {
    
    return YES;
}

/**
 *  This method rotate the coinView from button to webPage and vice versa.
 *
 *  @param animationTransitionType type of animation
 *  @param header_view             hederView
 *  @param sender_frame            button that will return to it when close webPageView.
 */
-(void)doTransitionWithType:(UIViewAnimationTransition)animationTransitionType header:(UIView *)header_view sender_frame:(CGRect )sender_frame{
    
    // assign senderButton to "sender_btn" to use it in close method.
    sender_btn=sender_frame;
    
    if (check==1) {
        // this webView flip to button
        [UIView transitionFromView:self.view2
                            toView:self.view1
                          duration:1
                           options:UIViewAnimationOptionTransitionFlipFromLeft+UIViewAnimationOptionCurveEaseInOut
                        completion:^(BOOL finished){
                            self.view2.hidden=YES;
                        }];
        
        //display button view &change the flag
        self.view1.hidden=NO;
        check=0;
    }
    else{
        // this button view flip to webView with animation
        [UIView transitionFromView:self.view1
                            toView:self.view2
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft+UIViewAnimationOptionCurveEaseInOut
                        completion:^(BOOL finished){
                            self.view1.hidden=NO;
                            [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
                                self.frame = CGRectMake(0,self.frame.origin.y, self.superview.frame.size.width, self.frame.size.height);
                            } completion: ^(BOOL completed) {
                                [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
                                    self.frame = CGRectMake(0,0, self.superview.frame.size.width, self.superview.frame.size.height);
                                    self.layer.cornerRadius=0;
                                } completion: ^(BOOL completed) {
                                    [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
                                        _web_content.frame = CGRectMake(0,_view2.frame.size.height,320,[[UIScreen mainScreen] bounds].size.height-_view2.frame.size.height);
                                    } completion: ^(BOOL completed) {
                                    }];
                                }];
                            }];
                        }];
        //display webPage view &change the flag
        self.view2.hidden=NO;
        check=1;
    }
}
/**
 *  this method flip to button with animation to orignal place.
 *  order of animation of back(in close method) is reversed from init in "doTransitionWithType"
 */
-(void)close{
    [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
        self.frame = CGRectMake(sender_btn.origin.x, sender_btn.origin.y, sender_btn.size.width, sender_btn.size.height);
        self.layer.cornerRadius=20;
    } completion: ^(BOOL completed) {
        [UIView transitionFromView:self.view2
                            toView:self.view1
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft+UIViewAnimationOptionCurveEaseInOut
                        completion:^(BOOL finished){
                            self.view2.hidden=YES;
                        }];
        self.view2.hidden=YES;
        self.view1.hidden=NO;
        check=0;
    }];
    // this block of code added at localization period used to change index of description textlabel to be infront of button not behind.
    for (UIView *subView in self.subviews) {
        if ([subView isKindOfClass:[UILabel class]]) {
            [self.view1 insertSubview:subView atIndex:1];
        }
    }
}

/**
 *  this method used to init webView & add GestureRecognizer to it
 */
-(void) addwebview{
    // init webView and setContentSize with size of mansik view size.
    _web_content=[[UIWebView alloc] initWithFrame:CGRectMake(0, 0,320,0)];
    [[_web_content scrollView] setContentSize:CGSizeMake(_web_content.scrollView.contentSize.width,_web_content.scrollView.contentSize.height+250)];
    
    //add GestureRecognizer
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [_web_content addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [_web_content addGestureRecognizer:swiperight];
    [self addSubview:_web_content];
}
/**
 *  This method handel SwipeGestureRecognizerleft .. it make transition to next webPage with animation and modify rest of view to this changes
 *
 *  @param sendr SwipeGestureRecognizer
 */
-(void)swipeleft:(id)sendr{
    
    if ([manasik_array count]!=current_index+1) {
        current_index++;
        
        //init data to new webPage
        [self init_data];
        // make transition animated
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromRight];
        [_view2.layer addAnimation:transition forKey:nil];
        [_web_content.layer addAnimation:transition forKey:nil];
        
        // chane currentPage of pageControl to new index
        self.pageControl.currentPage = current_index;
    }
}

/**
 *  This method handel SwipeGestureRecognizerRight .. it make transition to previous webPage with animation and modify rest of view to this changes
 *
 *  @param sendr SwipeGestureRecognizer
 */
-(void)swiperight:(id)sendr{
    if (current_index>0) {
        current_index--;
        
        //init data to new webPage
        [self init_data];
        // make transition animated
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        [transition setType:kCATransitionPush];
        [transition setSubtype:kCATransitionFromLeft];
        [_view2.layer addAnimation:transition forKey:nil];
        [_web_content.layer addAnimation:transition forKey:nil];
        
        // chane currentPage of pageControl to new index
        self.pageControl.currentPage = current_index;
    }
}
@end
