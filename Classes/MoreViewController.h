//
//  MoreViewController.h
//  mutnav2
//
//  Created by walid nour on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "oldImg.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MapKit/MapKit.h>
#import "ListViewController.h"
#import "PrayTimeViewController.h"
#import "CustomNavigationBar.h"
#import "NewEMViewController.h"
#import "EMutawefViewController.h"
#import "TownsViewController.h"
//#import <BugSense-iOS/BugSenseController.h>

@interface MoreViewController : UIViewController

@property (nonatomic, retain) NSMutableArray *mainarr;

@property (nonatomic, retain) IBOutlet UIImageView *backImg;
//@property (nonatomic, retain) IBOutlet AdsViewController *adsView;

//-(void)stopAds;


-(IBAction)buttonPressed:(id)sender;



@end
