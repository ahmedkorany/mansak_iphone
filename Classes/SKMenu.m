//
//  SKMenu.m
//  dbnav
//
//  Created by Shannon Appelcline on 9/11/08.
//  Copyright 2008 __MyCompanyName__. All rights reserved.
//

#import "SKMenu.h"

@implementation SKMenu

- (id)initWithFile:(NSString *)dbFile {
    
    self = [super init];
    
    myDB = [[SKDatabase alloc] initWithFile:dbFile];
    
    return self;
}

- (int)countForMenuWithParent:(int)parentid FromTable:(NSString *)table{
    
    int resultCount = 0;
    
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE adsID=%i",table, parentid];
    
    resultCount = (int)[myDB lookupSingularSQL:sql forType:@"integer"];
    
    return resultCount;
}

- (id)contentForMenuWithParent:(int)parentid Row:(int)row content:(NSString *)contenttype FromTable:(NSString *)table {
    
    NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE parentid=%i",contenttype,table, parentid];
    return [myDB lookupSingularSQL:sql forType:@"text"];
    
}

- (int)countFoTable:(NSString *)table{
    int resultCount = 0;
    
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@",table];
    
    resultCount = (int)[myDB lookupSingularSQL:sql forType:@"integer"];
    
    return resultCount;
}

- (int)countForTable:(NSString *)table{
    int resultCount = 0;
    
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@",table];
    
    resultCount = (int)[myDB lookupSingularINT:sql];
    
    return resultCount;
}

- (int)countFoTable:(NSString *)table withField:(NSString *)field withValue:(NSString *)value{
    int resultCount = 0;
    
    NSString *sql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM %@ WHERE %@='%@'",table,field,value];
    resultCount = (int)[myDB lookupSingularSQL:sql forType:@"integer"];
    return resultCount;
}

- (id)contentForMenuWithID:(int)row content:(NSString *)contenttype FromTable:(NSString *)table{
    NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE _id=%i",contenttype,table,row];
    return [myDB lookupSingularSQL:sql forType:@"text"];
}


- (id) getRow: (NSString *)sql{
    return [myDB lookupSingularSQL:sql];
}
- (NSMutableArray *)contentForMenuWithParent:(int)parentid content:(NSString *)contenttype FromTable:(NSString *)table{
    NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE parentid=%i",contenttype,table,parentid];
    return  [myDB lookupMultiSQL:sql forType:@"text"];
    
}
- (int)integerForMenuWithParent:(int)parentid Row:(int)row content:(NSString *)contenttype FromTable:(NSString *)table {
    
    NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE parentid=%i AND ordering=%i",contenttype,table,parentid,row];
    
    return (int)[myDB lookupSingularSQL:sql forType:@"integer"];
    
}

- (id)contentWithID:(int)row content:(NSString *)contenttype FromTable:(NSString *)table{
    NSString *sql = [NSString stringWithFormat:@"SELECT %@ FROM %@ WHERE _id=%i",contenttype,table,row];
    return [myDB lookupSingularSQL:sql];
}


- (void)insertRecordIntoTableNamed:(NSString *)tableName withField:(NSString *)field withValue:(NSString *)value{
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)", tableName, field,value ];
     NSLog(@"%@",sql);
    [myDB insertRecordIntoeTableNamed:sql];
}


- (void) deleteFromTable:(NSString *)tableName{
    NSString *sql = [NSString stringWithFormat:@"Delete from %@ ", tableName ];
    [myDB insertRecordIntoeTableNamed:sql];
}

- (void) deleteFromTable:(NSString *)tableName ByID:(int)adsID{
    NSString *sql = [NSString stringWithFormat:@"Delete from %@ WHERE adsID=%i ", tableName , adsID];
    [myDB insertRecordIntoeTableNamed:sql];
}

- (void) deleteFromTable:(NSString *)tableName withField:(NSString *)field ByID:(int)adsID{
    NSString *sql = [NSString stringWithFormat:@"Delete from %@ WHERE %@=%i ", tableName ,field, adsID];
    NSLog(@"%@",sql);
  [myDB insertRecordIntoeTableNamed:sql];
}
-(NSMutableArray*) getdata: (NSString*) myquery forType:(NSString *)rettype{
    return[myDB lookupMultiSQL:myquery forType:rettype];
}
- (int) getDataINT: (NSString *)sql {
    return [myDB lookupSingularINT:sql];
    
}
- (void)dealloc {
    [myDB close];
}
@end
