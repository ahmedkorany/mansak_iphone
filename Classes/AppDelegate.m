//
//  AppDelegate.m
//  mutnav2
//
//  Created by amr on 12/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "AppDelegate.h"
#import "menu_mainViewController.h"
#import "Global.h"

@implementation AppDelegate

@synthesize window;
@synthesize navigationController;
@synthesize myData,main_menu;
@synthesize netState,versions=_versions,version=_version,currentElement=_currentElement,ElementValue=_ElementValue;

#pragma mark -
#pragma mark Application lifecycle

- (void)awakeFromNib {    
    
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
   
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [[GAI sharedInstance].logger setLogLevel:kGAILogLevelInfo];
    [GAI sharedInstance].dispatchInterval = 20;
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-65205674-1"];
    // [GMSServices provideAPIKey:@"AIzaSyDJDvmzCFA2GtKlcd-Zvrxmff72aWU5W1U"];
    
    [NSThread sleepForTimeInterval:5.0];
    
    highf  =[[UIScreen mainScreen]bounds].size.height/480;
    screen_size=[[UIScreen mainScreen] bounds];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    main_menu = [[menu_mainViewController alloc] init];
    //[self.window setRootViewController:main_menu];
    
   [self.window addSubview:main_menu.view];
    [self.window makeKeyAndVisible];

   
    int cacheSizeMemory = 4*1024*1024; // 4MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    NSURLCache *sharedCache = [[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"] autorelease];
    [NSURLCache setSharedURLCache:sharedCache];
    
      [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self registerApp];
    });
    
    [self browseNotificationArticle:launchOptions];
   
    // Let the device know we want to receive push notifications
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    
    return YES;
}


- (void)checkNetworkStatus:(id)sender{

}
- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
   
}

// In case the app was sent into the background when there was no network connection, we will use
// the background data fetching mechanism to send any pending Google Analytics data.  Note that
// this app has turned on background data fetching in the capabilities section of the project.
-(void)application:(UIApplication *)application
performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
   
    completionHandler(UIBackgroundFetchResultNewData);
}

// We'll try to dispatch any hits queued for dispatch as the app goes into the background.
- (void)applicationDidEnterBackground:(UIApplication *)application
{
  
}




- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}




/**
 applicationWillTerminate: saves changes in the application's managed object context before the application terminates.
 */
- (void)applicationWillTerminate:(UIApplication *)application {
  //  [self saveContext];
}




#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


-(BOOL)parseDocumentWithURL:(NSURL *)url {
    if (url == nil)
        return NO;
    
    // this is the parsing machine
    NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    
    // this class will handle the events
    [xmlparser setDelegate:self];
    [xmlparser setShouldResolveExternalEntities:NO];
    
    // now parse the document
    BOOL ok = [xmlparser parse];
    if (ok == NO)
       NSLog(@"error");
    else
       NSLog(@"OK");
    
    [xmlparser release];
    return ok;
}

-(void)parserDidStartDocument:(NSXMLParser *)parser {
   // NSLog(@"didStartDocument");
}

-(void)parserDidEndDocument:(NSXMLParser *)parser {
  //  NSLog(@"didEndDocument");
}

-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"versions"]) {
        //Initialize the array.
        _versions = [[NSMutableArray alloc] init];
    }
    else if([elementName isEqualToString:@"version"]) {
        //Initialize the book.
        _version=[[NSMutableDictionary alloc] init];
        
        //Extract the attribute here.
        //   NSString *t=  [attributeDict objectForKey:@"content"] ;
        
        /// [_ad setObject:t forKey:@"url"];
        
    }
   // NSLog(@"Processing Element: ");
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if(!_ElementValue)
        _ElementValue = [[NSMutableString alloc] initWithString:string];
    else
        [_ElementValue appendString:string];
    
//    NSLog(@"Processing Value: %@", _ElementValue);
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if([elementName isEqualToString:@"versions"]){
        return;
    }
    
    if([elementName isEqualToString:@"version"]) {
        [_versions addObject:_version];
        _version = nil;
    }
    else{
        [_version setValue:_ElementValue forKey:elementName];
        // check allow for splash and reaya
        if([elementName isEqualToString:@"top_version"] ){
            
        }
        
    }
    _ElementValue = nil;
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
   
    
    
    if([title isEqualToString:@"نعم"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlvalue]];
    }
    else if([title isEqualToString:@"لا تظهر هذه الرسالة مجددًا"]){
        // [myData getRow:[NSString stringWithFormat:@"INSERT INTO AppVer (lastVar) values ('%@')",currentStringValue]];
        
        if (![myData countFoTable:@"AppVer"]==0){
            [myData getRow:[NSString stringWithFormat:@"UPdate AppVer SET lastVar ='%@' Where ID =(SELECT MAX(ID) from AppVer)",currentStringValue]];
        }
        else{
            [myData getRow:[NSString stringWithFormat:@"INSERT INTO AppVer (lastVar) values ('%@')",currentStringValue]];
        }
        
    }
}

// error handling
-(void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
   // NSLog(@"XMLParser error: %@", [parseError localizedDescription]);
}

-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError {
    //NSLog(@"XMLParser error: %@", [validationError localizedDescription]);
}

-(void)getVersion{
    
    myData = [[SKMenu alloc] initWithFile:@"mutawefdatabase.db"];
    
    
    int verID = [[[UIDevice currentDevice] systemVersion] intValue];
    
    NSString *urlString =[NSString stringWithFormat:@"http://api.madarsoft.com/mobile_programs_versions/v1/api/new_version.aspx?programid=28&devicetype=1&osversion=%i",verID] ;
 //   NSLog(@"urlString   %@",urlString);
    NSURL *url=[NSURL URLWithString:urlString];
    
    NSXMLParser *xmlparser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    [xmlparser setDelegate:self];
    [xmlparser setShouldResolveExternalEntities:NO];
    
    
    BOOL success = [xmlparser parse];
    
    if(success){
     //   NSLog(@"No Errors");
    }
    else{
   //     NSLog(@"Error Error Error!!!");
    }
    
    
    if([_versions count]>0){
        double preVersion;
       
        if ([[myData getRow:@"SELECT COUNT(*) FROM AppVer"]intValue]!=0) {
          preVersion = [[myData getRow:@"Select lastVar from AppVer Where ID =(SELECT MAX(ID) from AppVer)"]doubleValue ];
        }
        else{
            preVersion = 0;
        }
        NSDictionary *ver=[_versions objectAtIndex:0];
        currentStringValue =[ver valueForKey:@"top_version"];
        
        urlvalue =[ver valueForKey:@"url"];
        
        NSString*	version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        
        if([currentStringValue doubleValue ]> [version doubleValue] && [currentStringValue doubleValue ]> preVersion){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"تحديث"
                                                              message:@"يوجد اصدار أحدث لهذا البرنامج هل تريد التحديث ؟"
                                                             delegate:self
                                                    cancelButtonTitle:@"لا تظهر هذه الرسالة مجددًا"
                                                    otherButtonTitles:@"نعم",@"ذكرني لاحقًا", nil];
            [message performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            // [message show];
            return;
        }
        
        
        
    }

}


-(void)registerApp{
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"size_text"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"16" forKey:@"size_text"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"size_text"];
    text_size=[savedValue intValue];
    if (text_size<16) {
        text_size=16;
    }
    
    
    
    
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( ![userDefaults valueForKey:@"version"] )
    {
        // CALL your Function;
        
        // Adding version number to NSUserDefaults for first version:
        [userDefaults setFloat:[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue] forKey:@"version"];
    }
    else if ([[NSUserDefaults standardUserDefaults] floatForKey:@"version"] == [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue] )
    {
        /// Same Version so dont run the function
    }
    else
    {
        // Call Your Function;
        
        // Update version number to NSUserDefaults for other versions:
        [userDefaults setFloat:[[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue] forKey:@"version"];
    }

    
    NSString *deviceUDID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
	CTCarrier *carrier = [networkInfo subscriberCellularProvider];
	NSString *mnc = [carrier mobileNetworkCode];
    NSString *mcc = [carrier mobileCountryCode];
    
    if(mcc==Nil)mcc=@"0";
    if(mnc==Nil)mnc=@"0";
	
	NSString *urlAddress =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v2/api/add_client.aspx?mnc=%@&mcc=%@&platform=1&programid=28&deviceid=%@",mnc,mcc,deviceUDID] ;
    urlAddress = [urlAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
  	NSURL *url = [NSURL URLWithString:[urlAddress stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               // ...
                           }];

}


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
   
    
    NSLog(@"My token is: %@", deviceToken);
    NSString *deviceTokenstring = [NSString stringWithFormat:@"%@", deviceToken] ;
    deviceTokenstring=[deviceTokenstring stringByReplacingOccurrencesOfString:@" " withString:@""];
    deviceTokenstring=[deviceTokenstring stringByReplacingOccurrencesOfString:@">" withString:@""];
    deviceTokenstring=[deviceTokenstring stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *urlAddress =[NSString stringWithFormat:@"http://api.madarsoft.com/mobileads_NFcqDqu/v3/Register.aspx?devicetoken=%@&udid=0&programid=28&version=2&platform=1",deviceTokenstring] ;
    NSURL *url = [NSURL URLWithString:urlAddress];
     NSLog(@"My token is: %@", urlAddress);
    // Create the request.
   NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ([data length] >0 && error == nil)
                               {
                                   NSLog(@"IS DONE----");
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   NSLog(@"Nothing was downloaded.");
                               }
                               else if (error != nil){
                                   NSLog(@"Error = %@", error);
                               }
                               // ...
                           }];
    
    
    // Create the request.
   //   NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0];
  //  NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Create url connection and fire request
/*NSURLConnection *conn = [[NSURLConnection alloc] init];
     (void)[conn initWithRequest:request delegate:self];
  */
 /*
  UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Hello World!"
                                                           message:deviceTokenstring
                                                     delegate:nil
                                               cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
      [message show];*/
   
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
   // NSLog(@"Failed to get token, error: %@", error);
    
    
   /*
     UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Hello World!"
                                                 message:@"no provision profile."
                                                    delegate:nil
    cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
[message show];*/
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
  //  NSLog(@"app did received remote notification");
    id alert = [userInfo objectForKey:@"aps"];
    if ([alert isKindOfClass:[NSString class]]) {
       APN_message =alert ;
    } else if ([alert isKindOfClass:[NSDictionary class]]) {
       APN_message = [alert objectForKey:@"alert"];
   
        APN_message=[APN_message stringByReplacingOccurrencesOfString:@"/" withString:@""];
      //[_notifierArticle show:APN_message id:1];
       UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"أخبار المنسك"
                                                                               message:APN_message
                                                                               delegate:nil
                                                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                                 [message show];
        isAPN=NO;
        
    }
    
    isAPN=YES;
    
}

-(void)browseNotificationArticle:(NSDictionary *)launchOptions{
    // _isAPN=YES;
    NSDictionary *dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (dictionary != nil)
    {
        
       // NSLog(@"Launched from push notification: %@", dictionary);
        id alert = [dictionary objectForKey:@"aps"];
        if ([alert isKindOfClass:[NSString class]]) {
            APN_message = alert;
            
        } else if ([alert isKindOfClass:[NSDictionary class]]) {
            APN_message = [alert objectForKey:@"alert"];
            APN_message=[APN_message stringByReplacingOccurrencesOfString:@"/" withString:@""];
            
        }
        else{
          APN_message= [NSString stringWithFormat:@"test %@",[alert class]]   ;
            
        }
        isAPN=YES;
    }
    if (isAPN) {
        
        APN_message=[APN_message stringByReplacingOccurrencesOfString:@"/" withString:@""];
 
     //   [_notifierArticle show:APN_message id:1];
       _news_view=[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height)];
        _news_view.backgroundColor=[UIColor colorWithRed:0.153 green:0.718 blue:0.718 alpha:1];
        
        UILabel *header_lbl=[[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.window.frame.size.width,30)];
        header_lbl.text=@"تنبيهات";
        [header_lbl setFont:[UIFont fontWithName:@"Arial-BoldMT" size:16]];
        header_lbl.textAlignment=NSTextAlignmentCenter;
        header_lbl.textColor=[UIColor whiteColor];
        [_news_view addSubview:header_lbl];
        UIButton *back_btn=[[UIButton alloc] initWithFrame:CGRectMake(5, 20, 30, 30)];
        back_btn.layer.cornerRadius=12.5;
        back_btn.backgroundColor=[UIColor clearColor];
        [back_btn setImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
        [back_btn addTarget:self action:@selector(close_view:) forControlEvents:UIControlEventTouchUpInside];
        
        
     UIWebView *myWeb = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, self.window.frame.size.width, self.window.frame.size.height)];
        [_news_view setFrame:CGRectMake( -self.window.frame.size.width,  0,self.window.frame.size.width, self.window.frame.size.height)]; //notice this is OFF screen!
        [UIView beginAnimations:@"animateTableView" context:nil];
        [UIView setAnimationDuration:0.4];
        [self.window addSubview:_news_view];
        [_news_view setFrame:CGRectMake( 0, 0, self.window.frame.size.width, self.window.frame.size.height)]; //notice this is ON screen!
        [UIView commitAnimations];
        [_news_view addSubview:myWeb];
        [_news_view addSubview:back_btn];
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"news" ofType:@"html" inDirectory:@"www"]];
       NSString *body = [NSString stringWithFormat: @"arabicpartoftitle=%@",  APN_message];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL: url];
        [request setHTTPMethod: @"POST"];
        [request setHTTPBody: [body dataUsingEncoding: NSUTF8StringEncoding]];
       
        [myWeb loadRequest:request];
        [_news_view addSubview:myWeb];
        isAPN=NO;
       
        
   }
}

-(void)close_view:(id)sender{
    [UIView animateWithDuration:0.4 delay:0  options:0 animations: ^{
        [_news_view setFrame:CGRectMake( -self.window.frame.size.width, 0, self.window.frame.size.width, self.window.frame.size.height)]; //notice this is ON screen!
    } completion: ^(BOOL completed) {
        [_news_view removeFromSuperview];
    }];
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    if(self.videoIsInFullscreen == YES)
    {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    else
    {
        return UIInterfaceOrientationMaskPortrait;
    }
}




- (void)dealloc {
    
       [window release];
    [super dealloc];
}


@end

