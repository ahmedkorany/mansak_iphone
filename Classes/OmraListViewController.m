//
//  OmraListViewController.m
//  mutnav2
//
//  Created by walid nour on 3/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "oldImg.h"
#import "OmraListViewController.h"
#import "OmraTextViewController.h"
/*
@implementation UINavigationBar (CustomImage)

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed: @"gg.png"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}
@end
*/
@implementation OmraListViewController

@synthesize bgImage, mainTable;
@synthesize myid,menuearr,plist,sdic;
@synthesize mainarr;

//@synthesize adsView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0,0, 320,430*highf);

    
        mainTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 45*highf, 320, 350*highf) style:UITableViewStylePlain];
    mainTable.delegate = self;
    mainTable.dataSource = self;
    mainTable.autoresizesSubviews = YES;
    mainTable.backgroundColor=[UIColor clearColor];
    mainTable.alpha = 1;
    mainTable.rowHeight=50.0*highf;
   // mainTable.separatorColor = [UIColor blackColor];
    mainTable.hidden = NO;
    //mainTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    
    [self.view addSubview:bgImage];
    [self.view addSubview:mainTable];
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor]; 
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =@"عمرة";
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    [self.view addSubview:titleLabel];
    

    NSString *mypath=[[NSBundle mainBundle] pathForResource:@"mainmenue" ofType:@"plist"];
	mainarr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
     sdic=[mainarr objectAtIndex:0];
    plist=[[NSString alloc] initWithFormat:@"%@15",@"secondmenue"];
 
    NSString *path=[[NSBundle mainBundle] pathForResource:plist ofType:@"plist"];
	menuearr=[[NSMutableArray alloc]initWithContentsOfFile:path ];
    
  /*  if (flag==0) {
        
        adsView = [[AdsViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        adsView.adsID = 2;
        [adsView getURLS];
        if (adsView.statusID==0) {
            [self.view addSubview:adsView.view];
            [self performSelector: @selector(stopAds) withObject: nil afterDelay:10];
        }
        flag=1;
    }*/
    // Do any additional setup after loading the view from its nib.
}


-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
   /* UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];*/
  
}
/*
-(void)stopAds{
    flag=0;
    [adsView.view removeFromSuperview];
    //[adsView release];
}
*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuearr count]-1;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    if (indexPath.row%2==0) {
         [bgView setImage:[UIImage imageNamed:@"i1.png"]];
    }
    else{
    [bgView setImage:[UIImage imageNamed:@"i2.png"]];
    }
    [cell.contentView addSubview:bgView];
    [cell setBackgroundView:bgView];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [bgView release];
    // Configure the cell...
	NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
	cell.textLabel.text=[dic objectForKey:@"name"];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.lineBreakMode = NSLineBreakByCharWrapping;
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    cell.textLabel.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    lbl.textColor = [UIColor colorWithRed:0.039 green:0.49 blue:0.576 alpha:1.0];
    lbl.text = [NSString stringWithFormat:@"%i",indexPath.row +1];
    lbl.backgroundColor = [UIColor clearColor];
    cell.accessoryView = lbl;
    [lbl release];
    
  /*  UIImageView *bggView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    [bggView setImage:[UIImage imageNamed:@"cellbgg.png"]];
    [cell setSelectedBackgroundView:bggView];
    [bggView release];*/
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   NSMutableDictionary *dic=[menuearr objectAtIndex:indexPath.row];
	NSMutableDictionary *lastdic=[menuearr objectAtIndex:[menuearr count]-1];
	int targetpage=[[dic objectForKey:@"targetpage"] intValue];
	NSLog(@"%d",targetpage);
	NSLog(@"%@",[lastdic objectForKey:@"plist"] );
		OmraTextViewController *detailViewController2 = [[OmraTextViewController alloc] initWithNibName:@"ListViewController" bundle:nil];
        detailViewController2.sdic=dic;
	    detailViewController2.plist=[lastdic objectForKey:@"plist"];
        [self.navigationController pushViewController:detailViewController2 animated:YES];
		[detailViewController2 release];

}
- (void)viewDidUnload
{
    [self setBgImage:nil];
    [self setMainTable:nil];
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
	[super dealloc];
}

@end
