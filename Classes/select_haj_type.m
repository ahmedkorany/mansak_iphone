//
//  select_haj_type.m
//  mutnav2
//
//  Created by rashad on 8/13/14.
//
//

#import "select_haj_type.h"
#import "haj_mofrad.h"
#import "Global_style.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"
@interface select_haj_type ()

@end

@implementation select_haj_type
@synthesize haj_mofrad_view;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    /**
     *  get the views (headerView , title labels ) from "Global_style" & get footer view and hajjButtons from xib.
     */
    
    //  pass header view to "Global_style" to draw header view and return it.
    _haj_header_view=[Global_style header_view:_haj_header_view view_frame:self.view.frame];
    
    //draw footerView
    _footer_view.autoresizingMask= UIViewAutoresizingFlexibleBottomMargin;
    _footer_view.frame=CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-59, self.view.frame.size.width, _footer_view.frame.size.height);
    
    //get Title labels from retured headerView and set Title to them.
    _title_lable =(UILabel *)[_haj_header_view viewWithTag:4];
    _title_lable.text=NSLocalizedString(@"manasikEl7ag", @"test");
    _title_lable =(UILabel *)[_haj_header_view viewWithTag:5];
    _title_lable.text=NSLocalizedString(@"anwa3El7ag", @"test");
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Screen/Iphone ",NSLocalizedString(@"anwa3El7ag", @"test")]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    
    //get hajButtons and set Titles to them.
    for (UIView *myView in [self.view subviews]) {
        UIButton *myBtn;
        if (myView.tag==200) {
            myBtn=(UIButton*)myView;
            [myBtn setTitle:NSLocalizedString(@"hajMofrad", @"test") forState:UIControlStateNormal];
        }else if(myView.tag==201){
            myBtn=(UIButton*)myView;
            [myBtn setTitle:NSLocalizedString(@"hajTmt3", @"test") forState:UIControlStateNormal];
        }else if(myView.tag==202){
            myBtn=(UIButton*)myView;
            [myBtn setTitle:NSLocalizedString(@"hajQran", @"test") forState:UIControlStateNormal];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// send message to "initHajjViewWithTitle" with buttonTitle and its plist file.
- (IBAction)haj_mofrad:(id)sender {
    [self initHajjViewWithTitle:NSLocalizedString(@"hajMofrad", @"test") forData:@"hajj_mufrad"];
}

// send message to "initHajjViewWithTitle" with buttonTitle and its plist file.
- (IBAction)hajj_tamt3:(id)sender {
    [self initHajjViewWithTitle:NSLocalizedString(@"hajTmt3", @"test") forData:@"hajj_tmt3"];
}

// send message to "initHajjViewWithTitle" with buttonTitle and its plist file.
- (IBAction)hajj_qeran_action:(id)sender {
    [self initHajjViewWithTitle:NSLocalizedString(@"hajQran", @"test") forData:@"hajj_qeran"];
}

/**
 *  this method init hajView according selected type then add hajView to this view as subview with animation.
 *
 *  @param title     selected hajTitle
 *  @param plistName plist hold all data about selected hajType
 */
-(void)initHajjViewWithTitle:(NSString*)title forData:(NSString*)plistName{
    //init hajView
    haj_mofrad_view=[[haj_mofrad alloc] init];
    haj_mofrad_view.hajj_plist=plistName;
    haj_mofrad_view.title_lable_text=title;
  
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Screen/Iphone ",title]];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    // give minus value to width to prepare to animation
    haj_mofrad_view.view.frame=CGRectMake(-self.view.frame.size.width,haj_mofrad_view.view.frame.origin.y,haj_mofrad_view.view.frame.size.width, haj_mofrad_view.view.frame.size.height);
    
    [self.view addSubview:haj_mofrad_view.view];
    
    // transition(increase of width) to hajView with animation
    [UIView animateWithDuration:0.5 delay:0  options:0 animations: ^{
        haj_mofrad_view.view.frame=CGRectMake(0,haj_mofrad_view.view.frame.origin.y,haj_mofrad_view.view.frame.size.width, haj_mofrad_view.view.frame.size.height);
    } completion: ^(BOOL completed) {
        
    }];
}
@end
