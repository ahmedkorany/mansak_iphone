//
//  TextCellView.m
//  Almosaly
//
//  Created by Sara Ali on 2/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TextCellView.h"


@implementation TextCellView
@synthesize textLabel,drawCellRect;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
        [self initCell];
        self.backgroundColor =[UIColor clearColor];
        self.drawCellRect=YES;
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
    if (self.drawCellRect) {
        CGContextRef context = UIGraphicsGetCurrentContext();
        // Drawing with a white stroke color
        CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.7);
        // Draw them with a 2.0 stroke width so they are a bit more visible.
        CGContextSetLineWidth(context, 0.5);
        CGSize mySize = self.bounds.size;
        // Add Rect to the current path, then stroke it
        CGContextAddRect(context, CGRectMake(0, 0, mySize.width ,mySize.height));
        CGContextStrokePath(context);
    }
    
    
}

- (void)setText:(NSString*)text{
    
	   txt = text;
	   self.textLabel.text = text;
}

- (void)initCell{
    
    CGRect textFrame;
    UIFont* txtFont= [UIFont boldSystemFontOfSize:12];
    textFrame = self.frame;
    textLabel =  [[UILabel alloc] initWithFrame:textFrame];
    textLabel.font = txtFont;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.textColor=[UIColor whiteColor];
    [self addSubview:textLabel];
    self.opaque = NO;
}

// called when the frame property is set, this is overriding
-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.textLabel.frame = self.bounds;
}


@end
