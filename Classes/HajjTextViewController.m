//
//  HajjTextViewController.m
//  mutnav2
//
//  Created by waleed on 2/12/13.
//
//

#import "HajjTextViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "mutnav2AppDelegate.h"
#import "Global.h"
@interface HajjTextViewController ()

@end

@implementation HajjTextViewController

@synthesize bgImage;
@synthesize mytext,segments,titleName;
@synthesize arrdoaa,arrmanasek;
@synthesize sdic,hajjType,dayIndex;;
@synthesize sizeindex,txtType,manPath,errorPath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView
 {
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame=CGRectMake(0, 0,320, 430*highf);
    sizeindex = text_size;
    bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"insidebg.png"]];
    bgImage.frame = CGRectMake(0, 0, 320,430*highf);
    mytext = [[UITextView alloc] initWithFrame:CGRectMake(20, 90*highf, 280,295*highf)];
    mytext.delegate =self;
    mytext.autoresizesSubviews = YES;
    mytext.backgroundColor = [UIColor clearColor];
    mytext.alpha = 1;
    //mytext.font = [UIFont boldSystemFontOfSize:globalsize];
    mytext.editable = NO;
    mytext.textAlignment = NSTextAlignmentRight;
    [mytext.layer setBorderColor: [[UIColor colorWithRed:0.314 green:0.243 blue:0.227 alpha:1.0] CGColor]];
    [mytext.layer setBorderWidth: 3.0];
    [mytext.layer setCornerRadius:15.0f];
    [mytext.layer setMasksToBounds:YES];
    mytext.layer.shouldRasterize = YES;
    // [txtdetails.layer setShadowRadius:2.0f];
    mytext.layer.shadowColor = [[UIColor blackColor] CGColor];
    // txtdetails.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    //txtdetails.layer.shadowOpacity = 1.0f;
    mytext.layer.shadowRadius = 1.0f;
    mytext.font = [UIFont boldSystemFontOfSize:sizeindex];

    segments = [[UISegmentedControl alloc] initWithFrame:CGRectMake(8, 45*highf, 296, 40*highf)];
    segments.backgroundColor = [UIColor clearColor];
    segments.hidden = NO;
    segments.tintColor = [UIColor clearColor];
       [segments setBackgroundImage:[UIImage imageNamed:@"blacksea.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segments setBackgroundImage:[UIImage imageNamed:@"greensea.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [segments setBackgroundImage:[UIImage imageNamed:@"greensea.png"] forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
    [segments setDividerImage:[UIImage imageNamed:@"hHead2.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segments setDividerImage:[UIImage imageNamed:@"hHead.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
     [segments setDividerImage:[UIImage imageNamed:@"hHead2.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    [segments setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                          [UIFont boldSystemFontOfSize:16.0],UITextAttributeFont,
                                          [UIColor whiteColor], UITextAttributeTextColor,
                                          [UIColor clearColor], UITextAttributeTextShadowColor,
                                          [NSValue valueWithUIOffset:UIOffsetMake(0, 1)], UITextAttributeTextShadowOffset,
                                          nil] forState:UIControlStateNormal];
    
    [segments setSelectedSegmentIndex:2];
    [segments addTarget:self action:@selector(changefile:) forControlEvents:UIControlEventValueChanged];

    
   // self.title=[sdic objectForKey:@"name"];
    if ([[sdic objectForKey:@"Type"]isEqual:@"Y"]) {
        txtType=1;
    }
    else{
        txtType=0;
    }
    manPath = [sdic objectForKey:@"Men"];
    errorPath = [sdic objectForKey:@"Error"];
              
	if(txtType==1)
	{
        [segments insertSegmentWithTitle:@"الدعاء" atIndex:1 animated:YES];
        [segments insertSegmentWithTitle:@"أخطاء شائعة" atIndex:0 animated:YES];
        [segments insertSegmentWithTitle:@"المناسك" atIndex:2 animated:YES];
        [segments setSelectedSegmentIndex:2];
    }
    else if(txtType==0) {
        [segments insertSegmentWithTitle:@"المناسك" atIndex:1 animated:YES];
        [segments insertSegmentWithTitle:@"أخطاء شائعة" atIndex:0 animated:YES];
        [segments setSelectedSegmentIndex:1];
	}
    [self addfiletotextview:manPath];
    [self.view addSubview:bgImage];
    
    UIImageView *headImg =[[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gg.png"]]autorelease];
    headImg.frame=CGRectMake(0, 0, 320, 50*highf);
    [self.view addSubview:headImg];
    
    UILabel *titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(50, 0, 220, 45*highf)]autorelease];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    titleLabel.text =titleName;
    titleLabel.font = [UIFont boldSystemFontOfSize:20];
    titleLabel.lineBreakMode=NSLineBreakByCharWrapping;
    titleLabel.numberOfLines=0;
    //[titleLabel sizeToFit];
    [self.view addSubview:titleLabel];
    
    [self.view addSubview:mytext];
    [self.view addSubview:segments];
    
    CGRect increct = CGRectMake(130, 370*highf, 30,30*highf);
    UIButton *incbutton=[[UIButton alloc] initWithFrame:increct];
    [incbutton setFrame:increct];
    incbutton.alpha = 1;
    incbutton.tag = 1;
    UIImage *buttonImageNormal=[UIImage imageNamed:@"incBtn.png"];
    [incbutton setBackgroundImage:buttonImageNormal	forState:UIControlStateNormal];
    [incbutton setContentMode:UIViewContentModeCenter];
    [incbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect decrect = CGRectMake(160, 370*highf,30,30*highf);
    UIButton *decbutton=[[UIButton alloc] initWithFrame:decrect];
    [decbutton setFrame:decrect];
    decbutton.alpha = 1;
    decbutton.tag =2;
    UIImage *decImageNormal=[UIImage imageNamed:@"decBtn.png"];
    [decbutton setBackgroundImage:decImageNormal	forState:UIControlStateNormal];
    [decbutton setContentMode:UIViewContentModeCenter];
    [decbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:incbutton];
    [self.view addSubview:decbutton];
    [decbutton release];
    [incbutton release];
}

-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [self.mytext resignFirstResponder];
    return NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden=YES;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 5, 50, 30);
    [backButton setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:TRUE];
    [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:backButton];
}

-(IBAction)changeSize:(id)sender{
    int i = [sender tag];
    if (i==1) {
        sizeindex++;
    }
    else if(i==2)
    {
        sizeindex--;
    }
    if (sizeindex<14) {
        sizeindex=14;
    }
    mytext.font = [UIFont boldSystemFontOfSize:sizeindex];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"size"
                                                     ofType:@"txt"];
    NSString *size=[[NSString alloc] init];
    size=[NSString stringWithFormat:@"%d",sizeindex];
    [size writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
    text_size=sizeindex;
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    //[self setFrom:nil];
    [self setBgImage:nil];
    [self setMytext:nil];
    [self setSdic:nil];
    [self setSegments:nil];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)changefile:(id) sender{
	
    	if(txtType==1)
	{
        if([sender selectedSegmentIndex]==1){
            NSInteger randomint=arc4random()%14+1;
            //   NSString *randomfile=[[NSString alloc] initWithFormat:@"doaa%d",randomint];
            NSString *randomfile=[NSString stringWithFormat:@"doaa%d",randomint];
            [self addfiletotextview:randomfile];
            NSLog(@"%d",[sender selectedSegmentIndex]);
        }
        else if([sender selectedSegmentIndex]==0){
            
            [self addfiletotextview:errorPath];
            NSLog(@"%@",errorPath);
        }
        else if([sender selectedSegmentIndex]==2){
            
            [self addfiletotextview:manPath];
            NSLog(@"%d",[sender selectedSegmentIndex]);
        }
    }
	else if(txtType==0) {
        if([sender selectedSegmentIndex]==0){
            
            [self addfiletotextview:errorPath];
            NSLog(@"%@",errorPath);
        }
        else if([sender selectedSegmentIndex]==1){
            
             [self addfiletotextview:manPath];
        }
        
	}
    
}

-(void)addfiletotextview:(NSString*)filename{
	NSFileManager *filemgr;
	 NSLog(@"%@",filename);
	filemgr = [NSFileManager defaultManager];
	//plist=[[NSString alloc] initWithFormat:filepath,plist];
	
	NSString *path=[[NSBundle mainBundle] pathForResource:filename ofType:@"dat"];
    NSLog(@"%@",path);
	if ([filemgr fileExistsAtPath: path ] == YES)
	{
		
		//menuearr=[[NSMutableArray alloc]initWithContentsOfFile:mypath ];
		
		//NSData *databuffer;
        //	databuffer = [filemgr contentsAtPath: path ];
        
        NSError *error;
        
        //	mytext.text=[[NSString alloc] initWithData:databuffer encoding:NSUTF8StringEncoding];
        mytext.text=[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
		//NSString* aStr = [[NSString alloc] initWithData:databuffer encoding:NSASCIIStringEncoding];
		
	}
}

@end
