//
//  electronicmutawef.h
//  mutnav2
//
//  Created by amr on 1/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface electronicmutawef : UIViewController {
	UIImageView *elecmutawef;
	UITextView *mytext;
	NSMutableArray *arrdoaa;
	NSMutableArray *arrmanasek;
	int imgindex;
	UISegmentedControl *segments;
	UILabel *lbltitle;
	NSMutableDictionary *sdic;
	NSString *from;
	
}
@property(nonatomic,retain)IBOutlet UIImageView *elecmutawef;
@property(nonatomic,retain)IBOutlet UITextView *mytext;
@property(nonatomic,retain)IBOutlet NSMutableArray *arrdoaa;
@property(nonatomic,retain)IBOutlet NSMutableArray *arrmanasek;
@property(nonatomic,retain)IBOutlet UISegmentedControl *segments;
@property(nonatomic,retain)IBOutlet UILabel *lbltitle;
@property(nonatomic,retain)NSMutableDictionary *sdic;
@property(nonatomic,retain)NSString *from;
@property (retain, nonatomic) IBOutlet UIImageView *bgImage;
@property int imgindex;
-(IBAction) next_tawaf:(id)sender;
-(IBAction) previous_tawaf:(id)sender;
-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;
@end
