#ifndef PRAYERTIMECALCULATOR_H
#define PRAYERTIMECALCULATOR_H

#ifdef __cplusplus

#import "date.h"
#import <vector>
#import <map>
#import <string>
using std::vector;
using std::string;
using std::map;
using std::stringstream;

class PrayerTimeCalculator;

//This is the Objective-C wrapper
@interface PrayersCaculator : NSObject
{
    PrayerTimeCalculator* calc;
}

-initWithDay:(NSInteger)day Month:(NSInteger)mnth Year:(NSInteger)year Lat:(double)lat Long:(double)lng Zone:(int)zone
      Mazhab:(NSInteger)mzhb Way:(NSInteger)way DLS:(NSInteger)dls;

//get array of NSString* representing the 6 prayer times ( including shrooq)
-(NSArray*) getPrayersStrings;
//get array of 5 NSNumber* representing double format for the 5 prayer times
-(NSArray*) getPrayersDoubles;
//get array of NSArray of NSString, each array representing prayer times for the 7 days of the week. 0 for Saturday
-(NSArray*) getWeeklyPrayers:(Date)date;
//getShrooqTime
-(double) getShrooqTime;

@end



//standard c++
class PrayerTimeCalculator
{
public:
    PrayerTimeCalculator();
    PrayerTimeCalculator(int day, int mnth, int yer, double lt, double lg, double tzone, int mz, int way,int DLSaving);
    
    vector<string> calculateDailyPrayers_String() ;
    vector<double> calculateDailyPrayers();
    map<int , vector<string> > calculateWeeklyPrayers(Date SomeDay);
    
    double calculateJulianDate(int day, int month, int year) ;
    
    void setSummerTZone(int V) {SumTimeZone = V;}
    int  isSummerTZone() {return SumTimeZone; }
    void setDate(Date dt);
    
    
private:
    double calculateEquationOfTime(double JD, double TimePortion);
    double calculateSunDeclination(double JD, double TimePortion) ;
    
    
    vector<double> calculatePrayersOneIteration();
    double calculateDhuhrTime() ;
    double calculateMidDay(double t) ;
    double calculateSunset() ;
    double calculateSunrise() ;
    double calculateT(double a, double TimePortion);
    void setTimePortions(vector<double> Prayers) ;
    vector<double> adjustPrayers(vector<double> prayers) ;
    double calculateAsrTime() ;
    vector<double> calculateFajrAndIshaTimes() ;
    double calculateAofK(double G, double t) ;
    string adjustToString(double pt) ;
    
private: double Fajr;
private: double Sunrise;
private: double Dhuhr;
private: double Asr;
private: double Sunset;
private: double Maghreb;
private: double Isha;
private: double Lng;
private: double Lat;
private: double Tzone;
    
private: int Mazhab;
private: int Way;
private: int Day;
private: int Month;
private: int Year;
    
private: int  SumTimeZone;  // Summer time zone
private: double Eqt;
private: double Dcl;
private: double JDate;
private: vector<double> TimePortions;
    
    
};
#endif   
#endif // PRAYERTIMECALCULATOR_H
