//
//  HajjTextViewController.h
//  mutnav2
//
//  Created by waleed on 2/12/13.
//
//
#import "oldImg.h"
#import <UIKit/UIKit.h>
//#import <BugSense-iOS/BugSenseController.h>
@interface HajjTextViewController : UIViewController
<UITextViewDelegate >

@property (retain, nonatomic) IBOutlet UIImageView *bgImage;

@property (nonatomic,retain) IBOutlet UITextView *mytext;
@property (nonatomic,retain) IBOutlet NSMutableArray *arrdoaa;
@property (nonatomic,retain) IBOutlet NSMutableArray *arrmanasek;
@property (nonatomic,retain) IBOutlet UISegmentedControl *segments;

@property (nonatomic,retain) NSMutableDictionary *sdic;
@property (nonatomic,retain) NSString *titleName;
@property (nonatomic,retain) NSString *manPath;
@property (nonatomic,retain) NSString *errorPath;
@property int sizeindex;
@property int hajjType;
@property int dayIndex;
@property int txtType;


-(void)addfiletotextview:(NSString*)filename;
-(IBAction)changefile:(id) sender;
-(IBAction)changeSize:(id)sender;
@end
