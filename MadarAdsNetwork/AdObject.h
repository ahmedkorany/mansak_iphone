//
//  AdObject.h
//  testadsiphone
//
//  Created by Shaima Magdi on 12/22/14.
//  Copyright (c) 2014 hossam fathy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdObject : NSObject
@property int adDisplaying;

@property bool splashCheckedApperenceTime;
// if ad is madarsoft
@property(nonatomic,strong) NSString * adURL;
// banner or splash
@property(nonatomic,strong) NSString * adType;
// id on the server
@property int adId;
// 0 or more
@property int durationInSecond;
// bool to display or not.
@property int allowAd,test;
// type of ad admob or other
@property int contentType;
// 1 by default incremnent by 1 one offline
@property int offLineApperenceCount;
// duration Between Two Apperence
@property int durationBetweenTwoApperence;

@property int positionNumber;

@property(nonatomic,strong) NSString * lastApperenceTime;

@property int timeBeforeFirstDisplaying;  // time in seconds

@property(nonatomic,strong) NSDictionary * oldData;

-(BOOL)shouldBeDisplay;




@end
