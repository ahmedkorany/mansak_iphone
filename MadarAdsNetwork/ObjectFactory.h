//
//  ObjectFactory.h
//  testadsiphone
//
//  Created by Shaima Magdi on 12/17/14.
//  Copyright (c) 2014 hossam fathy. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CoreGraphics/CGGeometry.h>

#import "IframeAdsContents.h"
#import "AdObject.h"


@interface ObjectFactory : NSObject{
    
}
@property (nonatomic ,strong) NSString *admobBannerUnitId;
@property (nonatomic ,strong) NSString *admobSplashUnitId;
@property (nonatomic ,strong) NSString *googleTrackingId;
@property (nonatomic) CGRect bannerFrame;
@property(nonatomic)CGRect splashFrame;



-(id <IframeAdsContents>)creatAdContentObjForTag:(AdObject*)adData andType:(NSString*)type toRoot:(UIViewController*)root;
@end
