//
//  ViewController.m
//  twitternew
//
//  Created by mohamed rashad on 7/8/13.
//  Copyright (c) 2013 mohamed rashad. All rights reserved.
//

#import "ViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
typedef NS_ENUM(NSUInteger, UYLTwitterSearchState)
{
    UYLTwitterSearchStateLoading,
    UYLTwitterSearchStateNotFound,
    UYLTwitterSearchStateRefused,
    UYLTwitterSearchStateFailed
};

@interface ViewController ()
@property (nonatomic,strong) NSURLConnection *connection;
@property (nonatomic,strong) NSMutableData *buffer;
@property (nonatomic,strong) NSMutableArray *results;
@property (nonatomic,strong) ACAccountStore *accountStore;
@property (nonatomic,assign) UYLTwitterSearchState searchState;
@end

@implementation ViewController

- (ACAccountStore *)accountStore
{
    if (_accountStore == nil)
    {
        _accountStore = [[ACAccountStore alloc] init];
    }
    return _accountStore;
}

- (NSString *)searchMessageForState:(UYLTwitterSearchState)state
{
    switch (state)
    {
        case UYLTwitterSearchStateLoading:
            return @"Loading...";
            break;
        case UYLTwitterSearchStateNotFound:
            return @"No results found";
            break;
        case UYLTwitterSearchStateRefused:
            return @"Twitter Access Refused";
            break;
        default:
            return @"Not Available";
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = [self.results count];
    return count > 0 ? count : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    if ([self.results count] > 0) {
        
        
        NSDictionary *tweet = (self.results)[indexPath.row];
        NSDictionary *som=tweet[@"user"];
        NSString *name=(NSString *)som[@"profile_image_url"];
        NSURL *url=[NSURL URLWithString:name];
        NSData *data=[NSData dataWithContentsOfURL:url];
        //UIImageView *image=[image]
        cell.textLabel.font=[UIFont fontWithName:@"Helvetica" size:14.0];
        cell.textLabel.numberOfLines=3;
        cell.textLabel.text = tweet[@"text"];
        cell.imageView.image=[UIImage imageWithData:data];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row & 1)
    {
        cell.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#define RESULTS_PERPAGE @"100"
-(BOOL)checkaccount{
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    NSArray *accounts1 = [self.accountStore accountsWithAccountType:accountType];
    
    if (accounts1 ==0) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Twitter Account" message:@"No Twitter Account Configured ,Please Go To Setting And Add Account" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alert show];
        return NO;
    }else{
        return YES;
    }

}
- (void)loadQuery
{
    self.searchState = UYLTwitterSearchStateLoading;
    if ([self checkaccount]) {
    NSString *encodedQuery = [searchtext stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    ACAccountType *accountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    
        [self.accountStore requestAccessToAccountsWithType:accountType
                                                   options:NULL
                                                completion:^(BOOL granted, NSError *error)
         {
             if (granted)
             {
                 NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/search/tweets.json"];
                 NSDictionary *parameters = @{@"count" : RESULTS_PERPAGE,
                                              @"q" : encodedQuery};
                 
                 SLRequest *slRequest = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                           requestMethod:SLRequestMethodGET
                                                                     URL:url
                                                              parameters:parameters];
                 
                 NSArray *accounts = [self.accountStore accountsWithAccountType:accountType];
                 slRequest.account = [accounts lastObject];
                 NSURLRequest *request = [slRequest preparedURLRequest];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                 });
             }
             else
             {
                 self.searchState = UYLTwitterSearchStateRefused;
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.searchtable reloadData];
                 });
             }
         }];
    }
    
    
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.buffer = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [self.buffer appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.connection = nil;
    
    NSError *jsonParsingError = nil;
    NSDictionary *jsonResults = [NSJSONSerialization JSONObjectWithData:self.buffer options:0 error:&jsonParsingError];
    
    self.results = jsonResults[@"statuses"];
    if ([self.results count] == 0)
    {
        NSArray *errors = jsonResults[@"errors"];
        if ([errors count])
        {
            self.searchState = UYLTwitterSearchStateFailed;
        }
        else
        {
            self.searchState = UYLTwitterSearchStateNotFound;
        }
    }
    
    self.buffer = nil;
    [self.searchtable reloadData];
    [self.searchtable flashScrollIndicators];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.connection = nil;
    self.buffer = nil;
    self.searchState = UYLTwitterSearchStateFailed;
    
    [self handleError:error];
    [self.searchtable reloadData];
}

- (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Connection Error"
                                                        message:errorMessage
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addtwitt)];
    self.title=@"نتائج البحث";
    [self checkaccount];
    [self getplist];
    
}
-(void) addtwitt{
    if ([self checkaccount]) {
    if([TWTweetComposeViewController canSendTweet]){
        TWTweetComposeViewController *twitter=[[TWTweetComposeViewController alloc] init];
        [self presentViewController:twitter animated:YES completion:nil];
    }
    }
}
-(void)getplist{
    NSString *plistpath=[[NSBundle mainBundle]pathForResource:@"search" ofType:@"plist"];
    searchlist=[NSArray arrayWithContentsOfFile:plistpath];
}

- (IBAction)mwaqf:(id)sender {

    searchtext=[searchlist objectAtIndex:0];
    [self loadQuery];
    [self.searchtable reloadData];
}

- (IBAction)memories:(id)sender {
    searchtext=[searchlist objectAtIndex:1];
    [self loadQuery];
    [self.searchtable reloadData];
}

- (IBAction)help:(id)sender {
    searchtext=[searchlist objectAtIndex:2];
    [self loadQuery];
    [self.searchtable reloadData];
}
@end
