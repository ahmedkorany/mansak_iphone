//
//  ViewController.h
//  twitternew
//
//  Created by mohamed rashad on 7/8/13.
//  Copyright (c) 2013 mohamed rashad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    NSString *searchtext;
    NSArray *searchlist;
}

- (IBAction)mwaqf:(id)sender;
- (IBAction)memories:(id)sender;
- (IBAction)help:(id)sender;
-(void)getplist;
-(BOOL) checkaccount;
@property (weak, nonatomic) IBOutlet UITableView *searchtable;

@end
