//
//  ArabicConverter.h
//  mutnav2
//
//  Created by Shaima Magdi on 10/22/14.
//
//

/**
 *  This class used to use CUSTOM FONT in your code
 *
 *  if you want to use it you must use original font name when you call it -->
    to get original font name you must get array of all fonts from -->> [UIFont familyNames]
    then loop for this array to get each name [UIFont fontNamesForFamilyName: family]
    couse some fonts their name it is different in code from their name in resouces.
 *
 *  if want to use custom font font=[UIFont fontWithName:@"Hor" size:8]
 */
#import <Foundation/Foundation.h>

@interface ArabicConverter : NSObject


-(NSString*)convertArabic:(NSString*)normal;
-(NSString*)convertArabicBack:(NSString*)apfb;

@end
