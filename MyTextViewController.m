//
//  MyTextViewController.m
//  mutnav2
//
//  Created by Shaima Magdi on 11/16/14.
//
//

#import "MyTextViewController.h"
#import "Global.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAITracker.h"
#import "GAIDictionaryBuilder.h"

@interface MyTextViewController ()<UITextViewDelegate>{
    int maxPages;
    int pages;
}

@end

@implementation MyTextViewController
@synthesize mainTitle,secondTitle;
@synthesize sizeIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:[NSString stringWithFormat:@"%@ Screen/Iphone",mainTitle  ] ];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


- (void)viewDidLoad
{
      [super viewDidLoad];
    
    /**
     * getTextSize from user default if it is the first time or non editing before set default by 14
     */
    text_size=[[[NSUserDefaults standardUserDefaults] objectForKey:@"size_text"]intValue ];
    if (text_size>14) {
        sizeIndex = text_size;
    }else{
        sizeIndex = 14;
    }
    
    /**
     *  page use as index in arrayOfStings .. began with 1 instead of 0 as zero not hold actual data.
     */
    
    pages = 1;
    
    /**
     *  label show number of all texts in view to indicate which text user is reading now.
     *
     *  @param int count of gaven array -1.
     *
     *  @return number of actual data.
     */
    
    maxPages = (int)[_textArray count]-1;
    
    /*!
     @discussion it draw the footer & header view programmatically and set title and second title according property that was set before & adding 4 buttons for (increment,decrement,next,previous) and set pagesLabel.
     */
    
    CGRect screensize=[[UIScreen mainScreen] bounds];
    self.view.frame=CGRectMake(0, 0, screensize.size.width,screensize.size.height);
    self.view.backgroundColor = [UIColor whiteColor];
    // draw headerView
    _header_view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,130)];
    UIImageView *header_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _header_view.frame.size.width,  _header_view.frame.size.height)];
    header_image.image=[UIImage imageNamed:@"inner screen header.png"];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake((_header_view.frame.size.width-120)/2,50,120, 25)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.tag=4;
    titleLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    titleLabel.textColor=[UIColor colorWithRed:0.369 green:0.376 blue:0.373 alpha:1] /*#5e605f*/;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    if([self.navigationController.viewControllers objectAtIndex:0] != self){
        titleLabel.text =NSLocalizedString(mainTitle, @"") ;
    }
    else{
        titleLabel.text= NSLocalizedString(mainTitle, @"");
    }
    UIImageView *second_image=[[UIImageView alloc] initWithFrame:CGRectMake(40, _header_view.frame.size.height-40, self.view.frame.size.width-80, 45)];
    [second_image setImage:[UIImage imageNamed:@"title.png"]];
    UILabel *first_title=[[UILabel alloc] initWithFrame:CGRectMake(second_image.frame.origin.x+20, _header_view.frame.size.height-25, second_image.frame.size.width-40, 25)];
    first_title.backgroundColor=[UIColor clearColor];
    first_title.tag=5;
    first_title.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];
    
    first_title.textColor=[UIColor whiteColor];
    first_title.textAlignment=NSTextAlignmentCenter;
    first_title.text=NSLocalizedString(secondTitle, @"");
    [_header_view addSubview:header_image];
    [ _header_view  addSubview:titleLabel];
    [_header_view addSubview:second_image];
    [_header_view addSubview:first_title];
    [self.view addSubview:_header_view];
    if (highf==1) {
        _txtdetails = [[UITextView alloc] initWithFrame:CGRectMake(20, 175, 280,220*highf)];
    }
    else{
        _txtdetails = [[UITextView alloc] initWithFrame:CGRectMake(20, 175, 280,245*highf)];
    }
    // draw textView & set it's data
    _txtdetails.delegate =self;
    _txtdetails.autoresizesSubviews = YES;
    _txtdetails.backgroundColor = [UIColor whiteColor];
    _txtdetails.alpha = 1;
    _txtdetails.font = [UIFont fontWithName:@"Arial" size:sizeIndex];
    _txtdetails.editable = NO;
    _txtdetails.textAlignment = NSTextAlignmentCenter;
    _txtdetails.textColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:1];
    _txtdetails.text=NSLocalizedString(_textArray[1], @"") ;
    
   // draw pagesLabel
    _pagesLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 140, 100, 30)];
    _pagesLabel.backgroundColor = [UIColor whiteColor];
    [_pagesLabel.layer setCornerRadius:10.0f];
    _pagesLabel.hidden= YES;
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, 135, 280, 40)];
    bgView.backgroundColor = [UIColor colorWithRed:0.149 green:0.71 blue:0.702 alpha:1] /*#26b5b3*/;
    
    [bgView.layer setCornerRadius:5.0f];
    [self.view addSubview:bgView];
    
    NSString *segtitle=[NSString stringWithFormat:@" 1/%d ",maxPages];
    _pagesLabel.text = segtitle;
    _pagesLabel.textAlignment = NSTextAlignmentCenter;
    _pagesLabel.autoresizesSubviews = YES;
    _pagesLabel.textColor = [UIColor blackColor];
    _pagesLabel.hidden= NO;
    _pagesLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:18];;
    
    // draw nextBtn
    UIImageView *nextImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"next.png"]];
    nextImg.frame = CGRectMake(50, 0, 30, 30);
    UILabel *nextLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,50,30)];
    nextLabel.text =NSLocalizedString(@"next", @"");
    nextLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    nextLabel.textColor=[UIColor whiteColor] ;
    nextLabel.textAlignment=NSTextAlignmentRight;
    
    
    UIButton *nextBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [nextBtn setFrame:CGRectMake(215, 140, 80, 30)];
    nextBtn.alpha = 1;
    nextBtn.tag = 1;
    [nextBtn addTarget:self action:@selector(handlePage:) forControlEvents:UIControlEventTouchUpInside];
    [nextBtn addSubview:nextImg];
    [nextBtn addSubview:nextLabel];
    
     // draw prevBtn
    UIImageView *preImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"previous.png"]];
    preImg.frame = CGRectMake(0, 0, 30, 30);
    UILabel *preLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0,50,30)];
    preLabel.text =NSLocalizedString(@"prev", @"");
    preLabel.font = [UIFont fontWithName:@"Arial-BoldMT" size:20];
    preLabel.textColor=[UIColor whiteColor] ;
    preLabel.textAlignment=NSTextAlignmentLeft;
    
    UIButton *prevBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [prevBtn setFrame:CGRectMake(25, 140, 80, 30)];
    prevBtn.alpha = 1;
    prevBtn.tag =2;
    [prevBtn addTarget:self action:@selector(handlePage:) forControlEvents:UIControlEventTouchUpInside];
    [prevBtn addSubview:preImg];
    [prevBtn addSubview:preLabel];
    [self.view addSubview:nextBtn];
    [self.view addSubview:prevBtn];
    [self.view addSubview:_pagesLabel];
    [self.view addSubview:_txtdetails];
    
    // draw incrementBtn
    CGRect increct = CGRectMake(115, self.view.frame.size.height-80, 40, 40);
    UIButton *incbutton=[[UIButton alloc] initWithFrame:increct];
    [incbutton setFrame:increct];
    incbutton.alpha = 1;
    incbutton.tag = 1;
    [incbutton setTitle:@"A+" forState:UIControlStateNormal];
    [incbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    incbutton.backgroundColor = [UIColor colorWithRed:0.965 green:0.706 blue:0.275 alpha:1] /*#f6b446*/;
    [incbutton setContentMode:UIViewContentModeCenter];
    [incbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    incbutton.layer.cornerRadius = 5.0;
    
    // draw DecrementBtn
    CGRect decrect = CGRectMake(165, self.view.frame.size.height-80, 40, 40);
    UIButton *decbutton=[UIButton buttonWithType:UIButtonTypeCustom];
    [decbutton setFrame:decrect];
    decbutton.alpha = 1;
    decbutton.tag =2;
    [decbutton setTitle:@"A-" forState:UIControlStateNormal];
    [decbutton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    decbutton.layer.cornerRadius = 5.0;
    decbutton.backgroundColor = [UIColor colorWithRed:0.149 green:0.71 blue:0.702 alpha:1] /*#26b5b3*/;
    [decbutton addTarget:self action:@selector(changeSize:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:incbutton];
    [self.view addSubview:decbutton];
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    
    // Setting the swipe direction.
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    // Adding the swipe gesture on image view
    [self.view addGestureRecognizer:swipeLeft];
    [self.view addGestureRecognizer:swipeRight];
    
    
    _footer_view = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height-49, self.view.frame.size.width,49)];
    UIImageView *footer_image=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,  _footer_view.frame.size.width,  _footer_view.frame.size.height)];
    footer_image.image=[UIImage imageNamed:@"footer_44.png"];
    [ _footer_view  addSubview:footer_image];
    
    [self.view addSubview:_footer_view];
}

/// pop the view from navigation to show the previous one.
-(void)popViewControllerWithAnimation {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.hidden = YES;
    if([self.navigationController.viewControllers objectAtIndex:0] != self)
    {
        // draw back button
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame =CGRectMake(10, 20, 30, 30);
        [backButton setImage:[UIImage imageNamed:@"backBtn.png"] forState:UIControlStateNormal];
        [backButton setShowsTouchWhenHighlighted:TRUE];
        [backButton addTarget:self action:@selector(popViewControllerWithAnimation) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:backButton];
    }
}

/**
 *  this method used to set String to textView (with last size user selected) and set pagesLabel.
 *
 *  @param textName is NSString that will show in textView.
 *  @warning this method used localization string from gaven string.
 */
-(void)addTextToTextView:(NSString*)textName{
    NSString *segtitle=[NSString stringWithFormat:@"%d/%d ",pages,maxPages];
    [_pagesLabel setText:segtitle];
    _txtdetails.text=NSLocalizedString(textName, @"") ;
    _txtdetails.font = [UIFont fontWithName:@"Arial" size:sizeIndex];
}

/**
 *  this method used to change size of text and save this size to userDefault to use it later.
 *
 *  @param sender the increment & decrement button and check their tag.
 */
-(IBAction)changeSize:(id)sender{
    
    int i = (int)[sender tag];
    // increment btn
    if (i==1) {
        sizeIndex++;
    }
    // decrement btn
    else if(i==2)
    {
        if (sizeIndex>14) {
            
            sizeIndex--;
        }
        
    }
    _txtdetails.font = [UIFont fontWithName:@"Arial" size:sizeIndex];
    // save in userDefault
    NSString *size=[[NSString alloc] init];
    size=[NSString stringWithFormat:@"%d",sizeIndex];
    [[NSUserDefaults standardUserDefaults] setObject:size forKey:@"size_text"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    text_size=sizeIndex;
}
// disable interaction of textView with user
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    [self.txtdetails resignFirstResponder];
    return NO;
}

/**
 *  method handel swipe action in transition between Texts.
 *  if description is right it transition to the previous index else "next" then send message to "addTextToTextView" with index.
 *
 *  @param swipe swipe description
 */
- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if(swipe.direction == UISwipeGestureRecognizerDirectionRight)
    {
        pages--;
        
        if (pages<1) {
            pages=1;
        }
    }
    else
    {
        pages++;
        
        if (pages>=maxPages) {
            pages=maxPages;
            
        }
        
    }
    [self addTextToTextView:_textArray[pages]];
}

/**
 *  the same action of "handleSwipe" but this used when press next and previous buttons.
 *
 *  @param sender next and previous buttons and check their tags.
 */

-(IBAction)handlePage:(id)sender{
    
    if([sender tag]==2)
    {
        pages--;
        
        if (pages<1) {
            pages=1;
        }
    }
    else
    {
        pages++;
        
        if (pages>=maxPages) {
            pages=maxPages;
            
        }
    }
    [self addTextToTextView:_textArray[pages]];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return Portrait interface
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
