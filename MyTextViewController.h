//
//  MyTextViewController.h
//  mutnav2
//
//  Created by Shaima Magdi on 11/16/14.
//
//

/*!
 @discussion This class display items as a Texts (TextView) according to an array of Strings was set to it.
 the same logic used here .. index 0 hold "text" word .. then the rest of array are the actual data must be set to text view .
 
 
 use this class :-
 1- You just need to import it to your view.
 2- set array of this class with type of target view at index zero.
 3- text size will be equal to the last size that user modified it.
 4-set the other items (mainTitle,secondTitle) before push this view.
 
 */
#import <UIKit/UIKit.h>

@interface MyTextViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *header_view;
@property (strong, nonatomic) IBOutlet UIView *footer_view;
@property (nonatomic, strong) NSString *mainTitle;
@property (nonatomic, strong) NSString *secondTitle;
@property(nonatomic,strong)IBOutlet UITextView *txtdetails;
/*!
 * @brief TextViewController label object hold number of all texts of this view .
 */
@property(nonatomic,strong)IBOutlet UILabel *pagesLabel;
/*!
 * @brief TextViewController arrays object hold all texts of this view .
 */
@property (nonatomic ,strong)NSArray *textArray;
/*!
 * @brief TextViewController int that control in size of text .
 */
@property int sizeIndex;


@end
